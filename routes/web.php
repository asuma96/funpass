<?php

Route::get('/', function () {
    return view('pages.home');
});
Auth::routes();
Route::post('uploadavatar', 'UserController@uploadAvatar');
Route::get('/list', function () {
    return view('pages.products');
});
Route::get('/ticket-bundles', function () {
    return view('pages.ticket-bundles');
});
Route::get('/login', function () {
    return view('pages.login');
});
Route::get('/register', function () {
    return view('pages.register');
});
Route::get('/shoppingCart', function () {
    return view('pages.shoppingCart');
});
Route::get('/home', function () {
    return view('pages.home');
});

Route::get('sitemap', 'PageController@sitemap');

Route::get('wishlist', 'UserController@getWishlists');
Route::get('wishlist/attach/{idProduct}', 'UserController@attachProductToWishlist');
Route::get('wishlist/detach/{idProduct}', 'UserController@detachProductFromWishlist');
Route::get('myaccount', 'UserController@myAccount');
Route::post('myaccount', 'UserController@update');

Route::get('shopping', 'OrderController@history');
Route::post('buy', 'OrderController@store');

Route::get('single-product/{product}', 'ProductController@show');
Route::get('product-category/{category_id}', 'ProductController@listByCategory');
//Route::get('search', 'ProductController@search');
Route::post('get-products-in', 'ProductController@getProductsIn');

Route::get('orders/{hash}', 'OrderController@orderHash');


Route::get('best-sellers', 'ProductController@best');
Route::get('orders', 'OrderController@history');
Route::get('/special-offers', 'ProductController@specialOffers');
Route::get('page/{url}', 'PageController@page');
Route::post('users/login-as-user/{id}', 'UserController@loginAsUser');


Route::get('/search', function () {
    return view('pages.search');
});

Route::get('/myaccount', function () {
    return view('pages.myaccount');
});

Route::get('/faq', function () {
    return view('pages.faq');
});
Route::get('/contact', function () {
    return view('pages.contactUs');
});

Route::get('/shopping-cart', function () {
    return view('pages.shoppingCart');
});

Route::get('/checkout', function () {
    return view('pages.checkout');
});
Route::get('/modalWindow', function () {
    return view('pages.modalWindow');
});
