<?php

Route::group(['prefix' => 'v1'], function () {
    Route::resource('categories', 'API\CategoryController');
    Route::post('categories/upload-cover', 'API\CategoryController@upload');

    Route::resource('products', 'API\ProductController');
    Route::get('products/category/{idCategory}', 'API\ProductController@searchCategory');

    Route::get('best-sellers', 'API\ProductController@bestSellers');

    Route::post('login', 'API\Auth\LoginController@login');
    Route::post('admin/login', 'API\Auth\LoginController@adminLogin');

    Route::post('register', 'API\Auth\RegisterController@register');
});

Route::group(['prefix' => 'v1', 'middleware' => 'auth:api'], function () {
    Route::resource('users', 'API\UserController');
    Route::get('myaccount', 'API\UserController@myAccount');
    Route::post('myaccount', 'API\UserController@updateSelf');
    Route::post('password', 'API\UserController@changePassword');
    Route::get('wishlist/attach/{product_id}', 'API\UserController@attachToWishlist');
    Route::get('wishlist/detach/{product_id}', 'API\UserController@detachFromWishlist');
    Route::get('wishlist', 'API\UserController@getWishlists');
    Route::post('products/{id}/send-received', 'API\UserController@receiversUserSendEmail');
    Route::post('newsletter', 'API\UserController@newsletter');
    Route::post('upload-avatar/{id}', 'API\UserController@upload');
    Route::post('add-address', 'API\UserController@addAddress');
    Route::post('remove-address/{id}', 'API\UserController@removeAddress');

    Route::post('products/{product}/{relation}/{action}', 'API\ProductController@sync');
    Route::resource('orders', 'API\OrderController');
    Route::post('orders/pay-as-user/{token}', 'API\OrderController@payAsUser');
    Route::get('history', 'API\OrderController@history');
    Route::post('buy', 'API\OrderController@store');

    Route::resource('keywords', 'API\KeywordController');
    Route::resource('bans', 'API\BanController');
    Route::resource('images', 'API\ImageController');
    Route::resource('settings', 'API\SettingController');
    Route::resource('pages', 'API\PageController');
    Route::resource('blocks', 'API\BlockController');
    Route::resource('files', 'API\FileController');
    Route::resource('badges', 'API\BadgeController');
    Route::resource('agents', 'API\AgentController');
    Route::resource('tickets', 'API\TicketController');
});
