    <!-- start latest post area -->
    <section class="latest_post_area">
        <div class="container">
            <div class="row">
                <div class="latest_post">
                    <div class="section_title">
                        <h4>Seasonal Events</h4>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="single_latest_post">
                                <div class="grid">
                                    <figure class="effect-honey">
                                        <a href="#"><img src="dist/img/starwars.png" alt=""></a>
                                        <figcaption>
                                            <h6><span>Starts<br /></span>1 Dec 2016</h6>
                                        </figcaption>
                                    </figure>
                                    <!--  <div class="product-description">
                                        <p>
                                            <a href="#"><span>By</span> Hastech</a>
                                            <a href="#">12 <span>Comments</span></a>
                                            <a href="#">25 <span>Likes</span></a></p>
                                        <a href="#">Eddie Chacon & Sissy Sainte</a>
                                        <p>
                                            Seed at risus sed tortor dapibus varius sits amet eu saps ein. Pellentesque st amet gra vida quam. [...]
                                        </p>
                                    </div><product-description -->
                                </div><!-- grid -->
                            </div><!-- .single_latest_post -->
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="single_latest_post">
                                <div class="grid">
                                    <figure class="effect-honey">
                                        <a href="#"><img src="dist/img/mardi.png" alt=""></a>
                                        <figcaption>
                                            <h6><span>Starts<br /></span>8 Feb 2016</h6>
                                        </figcaption>
                                    </figure>

                                </div><!-- grid -->
                            </div><!-- .single_latest_post -->
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="single_latest_post">
                                <div class="grid">
                                    <figure class="effect-honey">
                                        <a href="#"><img src="dist/img/epcotFlower.png" alt=""></a>
                                        <figcaption>
                                            <h6><span>Starts<br /></span>2 Mar 2016</h6>
                                        </figcaption>
                                    </figure>
                                </div><!-- grid -->
                            </div><!-- .single_latest_post -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end latest post area -->