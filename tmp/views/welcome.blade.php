
@extends('layouts.app')

@section('content')
{{--    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Welcome</div>

                    <div class="panel-body">
                        Your Application's Landing Page.
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
        <!-- start header -->
@include('header')
        <!-- end header -->

<!-- start home slider -->
<!-- Stretch Gallery containter -->
@include('sections.stretchgallery')
        <!-- End of Stretch Gallery containter -->
<!-- end home slider -->

<!-- start supports area -->
@include('sections.supportsarea')
        <!-- end supports area -->

<!-- start product area -->
@include('sections.productsarea')
        <!-- end product area -->

<!-- start latest post area -->
@include('sections.latestpostarea')
        <!-- end latest post area -->
@endsection

@section('jscontent')

    <script>
        $(".m_nav").click(function () {
            $("#cssmenu").slideToggle("slow", function () {
            });
        });


    </script>
@endsection