@extends('layouts.app')

@section('content')
    <!-- start header -->
    @include('header')
    <!-- end header -->
    <div class="container p-l-none p-r-none">
        <div class="">
            <div class="">
                @include('sections.myaccount')
            </div>
        </div>
    </div>
@endsection
