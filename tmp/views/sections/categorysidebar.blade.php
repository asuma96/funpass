<div class="category_sidebar">
    <aside class="sidebar_widget">
        <div class="widget_title">
            <h4 class="special_border_right">CATEGORIES</h4>
        </div>
        <div class="sub_widget badge_widget">
            <ul>
                @foreach(cache('topCategories') as $category)
                    <li><a href="/product-category/{{$category->id}}">{{$category->title}}</a></li>
                @endforeach
            </ul>
        </div>
    </aside>


    <aside class="sidebar_widget"/>
</div>