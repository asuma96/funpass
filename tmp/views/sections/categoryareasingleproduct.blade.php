<!-- start category area -->
<div class="category_area">
    <div class="container">
        <div class="row">
            <div class="category">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-12">
                        @include('sections.categorysidebar')
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                        <div class="category_products">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="single_product_page fix">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-5 no-padding-right">
                                                    <div class="single_product_image">
                                                        <div class="fotorama" data-nav="thumbs" data-fit="cover" data-width="100%" data-loop="true" data-autoplay="true">
                                                            <img alt='image1'/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-7 no-padding-left">
                                                    <div class="single_product_details">
                                                        <div class="single_product_description">
                                                            <h6>Ticket information</h6>

                                                            <div class="product_detail top_border_with_pad">
                                                                <p>
                                                                </p>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="color_size top_border_with_pad row">
                                                                        <div class="">
                                                                            <div class="product_color">
                                                                                <span class="col-sm-4 m-t-sm">Adult</span>
                                                                                <div class="col-sm-4" >
                                                                                    <span></span>
                                                                                    <span></span>
                                                                                    <span>
                                                                                        <br>(<span></span>
                                                                                        <span></span>)
                                                                                    </span>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <label>
                                                                                        <input type="number" min="1" max="100" step="1" class="form-control" value="0">
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="">
                                                                            <div class="product_size">
                                                                                <span class="col-sm-4 m-t-sm">Child</span>
                                                                                <div class="col-sm-4" >
                                                                                    <span></span>
                                                                                    <span class=""></span>

                                                                                    <span>
                                                                                        <br>(<span></span>
                                                                                        <span class=""></span>)
                                                                                    </span>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <label>
                                                                                        <input type="number" min="0" max="100" step="1" class="form-control" value="0">
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="">
                                                                            <div class="product_size">
                                                                                <span class="col-sm-4 m-t-sm">Infant</span>
                                                                                <div class="col-sm-4" >
                                                                                    <span></span>
                                                                                    <span class=""></span>

                                                                                    <span>
                                                                                        <br>(<span ></span>
                                                                                        <span class=""></span>)
                                                                                    </span>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <label>
                                                                                        <input type="number" min="0" max="100" step="1" class="form-control" value="0">
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="appCtrl.product_detail top_border_with_pad">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <p>Arrive Date:</p>
                                                                        <p style="color:red">
                                                                            Select correct date!
                                                                        </p>
                                                                        <datepicker date-format="MM/dd/yyyy" date-min-limit="@{{appCtrl.getArriveDate()}}" date-max-limit="@{{appCtrl.getArriveDate(appCtrl.product.EndSellDate.split(' ')[0])}}" date-year-title="selected title">
                                                                            <input placeholder="Enter date" class="form-control"/>
                                                                        </datepicker>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <p><strong>Min Age: </strong><span></span></p>
                                                                <p>
                                                                    <strong>Start Sale Date: </strong><span></span>
                                                                </p>

                                                                <p>
                                                                    <strong>End Sale Date: </strong><span ng-bind="appCtrl.product.EndSellDate.split(' ')[0]"></span>
                                                                </p>
                                                                <br>
{{--                                                                <p ng-show="appCtrl.product.StartSellDate > appCtrl.today()">
                                                                    <b>Start Sell Date:</b> <span ng-bind="appCtrl.product.StartSellDate.split(' ')[0]"></span>
                                                                </p>--}}
                                                                <p>
                                                                    <b>Out of stock</b>
                                                                </p>
                                                               <p>
                                                                    <b>Sold out</b>
                                                                </p>
                                                            </div>
                                                            <div class="product_detail top_border_with_pad">
                                                                <p>
                                                                    <strong>Keywords: </strong><span><a href="#"></a><b>, </b></span>
                                                                </p>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="quanlity_cart_fav border_with_pad fix">
                                                                        <div class="no-padding-left no-padding-right">
                                                                            <div class="subtotal-grandtotal">
                                                                                <div class="clearfix p-b-sm">
                                                                                    <div class="sub-total">
                                                                                        {{--<span class="check-subtot">Subtotal</span><br>--}}
                                                                                        <span class="check-grandtot">Grand Total</span>
                                                                                    </div>
                                                                                    <div class="grand-total">
{{--                                                                                        <span data-ng-bind="dollarIcon"></span><span class="Subtotal" ng-bind="(appCtrl.getItemTotal(appCtrl.product)*1).toFixed(2)"></span>

                                                                                        <span ng-if="currentCurrency != 'USD'">
                                                                                            (<span data-ng-bind="currentIcon"></span><span class="Subtotal" ng-bind="(appCtrl.getItemTotal(appCtrl.product)*currentIndex).toFixed(2)"></span>)
                                                                                        </span>
                                                                                        <br>--}}
                                                                                        <span></span><span class="check-grandtot GrandTotal"></span>

                                                                                        <span>
                                                                                            (<span></span><span class="check-grandtot GrandTotal"></span>)
                                                                                        </span>
                                                                                        <br>
                                                                                    </div>
                                                                                </div>
                                                                                {{--<a data-ng-click="block_confirm_order_btn? '' : appCtrl.confirmCheckout()"--}}
                                                                                {{--ng-class="{'disabled': appCtrl.cart<=0}"--}}
                                                                                {{--class="btn btn-default btn_reverse confirm_order_btn m-t-sm has-spinner">--}}
                                                                                {{--<span class="spinner"><i class="fa fa-spinner fa-spin fa-fw"></i></span>--}}
                                                                                {{--Confirm Order--}}
                                                                                {{--</a> --}}
                                                                                <span>
                                                                                    <a class="btn btn-default btn_reverse confirm_order_btn m-t-sm has-spinner" href="{{ URL::to('/shopping-cart')}}">In cart</a>
                                                                                </span>
                                                                                <span>
                                                                                <a href="#"
                                                                                   class="btn m-r-sm btn-default btn_reverse confirm_order_btn m-t-sm has-spinner">Buy Now</a>
                                                                                {{--<a ng-show="!appCtrl.Arrive_Date_Validate(appCtrl.product.Arrive_Date, appCtrl.product.StartSellDate.split(' ')[0], appCtrl.product.EndSellDate.split(' ')[0], 'product') && (appCtrl.product.Tickets_Left != 0) && (appCtrl.product.dateIsValid)" href="#" ng-show="true" ng-click="appCtrl.addToCart(appCtrl.product)"--}}
                                                                                <a href="#"
                                                                                   class="btn btn-default btn_reverse confirm_order_btn m-t-sm has-spinner">Add to cart</a>
                                                                                <a class="btn btn-default btn_reverse confirm_order_btn m-t-sm has-spinner" href="#">Add to wishlist</a>
                                                                                </span>
                                                                            </div>
                                                                            <div class="cart_fav fix">
                                                                                <ul class="social_icons pull-right">
                                                                                    <li></li>
                                                                                    <li></li>
{{--                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-refresh"></i></a>
                                                                                    </li>
                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-heart"></i></a>
                                                                                    </li>
                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-search"></i></a>
                                                                                    </li>--}}
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="product_item_details no_border_with_50_pad">
                                                    <div role="tabpanel">

                                                        <!-- Nav tabs -->
                                                        <ul class="nav nav-tabs" role="tablist">
                                                            <li role="presentation" class="active"><a
                                                                        href="#description" aria-controls="description"
                                                                        role="tab" data-toggle="tab">description</a>
                                                            </li>
                                                            <li role="presentation"><a href="#review"
                                                                                       aria-controls="review" role="tab"
                                                                                       data-toggle="tab">terms</a></li>
                                                        </ul>

                                                        <!-- Tab panes -->
                                                        <div class="tab-content">
                                                            <div role="tabpanel" class="tab-pane active"
                                                                 id="description">
                                                                <div></div>

                                                            </div>
                                                            <div role="tabpanel" class="tab-pane"
                                                                 id="review">
                                                                <div></div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            {{--appCtrl.relatedproducts.length > 0--}}
                                            <div class="col-md-12">
                                                <div class="upsell_products">
                                                    <div class="section_title">
                                                        <a href="#" class="btn btn_reverse">Other Tickets!</a>
                                                    </div>
                                                    <div style="overflow: hidden;" class="upsell_products_carousel">
                                                        <div class="item" style="overflow: hidden;" >
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <img src="#" alt="">
                                                                    <figcaption>
                                                                        <p class="add_to_cart">Add to cart</p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a>Et harum
                                                                        quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price"></span> <span
                                                                                    class="new_price"></span>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>