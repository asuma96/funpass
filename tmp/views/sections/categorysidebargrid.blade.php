<div class="category_sidebar">
    <aside class="sidebar_widget">
        <div class="widget_title">
            <h6 class="special_border_right">Category</h6>
        </div>

        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse"
                           data-parent="#accordion" href="#collapseOne">
                            Women
                            <span><i class="fa fa-plus-square-o"></i></span>
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul>
                            <li><a href="#">Sharee</a></li>
                            <li><a href="#">T-shirt</a></li>
                            <li><a href="#">Shoes</a></li>
                            <li><a href="#">Salowar</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse"
                           data-parent="#accordion" href="#collapseTwo">
                            Men
                            <span><i class="fa fa-plus-square-o"></i></span>
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul>
                            <li><a href="#">Shirt</a></li>
                            <li><a href="#">T-shirt</a></li>
                            <li><a href="#">Shoes</a></li>
                            <li><a href="#">Pent</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse"
                           data-parent="#accordion" href="#collapseThree">
                            Child
                            <span><i class="fa fa-plus-square-o"></i></span>
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul>
                            <li><a href="#">Women</a></li>
                            <li><a href="#">Men</a></li>
                            <li><a href="#">Child</a></li>
                            <li><a href="#">Jewellery</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse"
                           data-parent="#accordion" href="#collapseFour">
                            Jwellery
                            <span><i class="fa fa-plus-square-o"></i></span>
                        </a>
                    </h4>
                </div>
                <div id="collapseFour" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul>
                            <li><a href="#">Women</a></li>
                            <li><a href="#">Men</a></li>
                            <li><a href="#">Child</a></li>
                            <li><a href="#">Jewellery</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse"
                           data-parent="#accordion" href="#collapseFive">
                            Accessories
                            <span><i class="fa fa-plus-square-o"></i></span>
                        </a>
                    </h4>
                </div>
                <div id="collapseFive" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul>
                            <li><a href="#">Women</a></li>
                            <li><a href="#">Men</a></li>
                            <li><a href="#">Child</a></li>
                            <li><a href="#">Jewellery</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </aside>
    <aside class="sidebar_widget">
        <div class="widget_title">
            <h4 class="special_border_right">shop by</h4>
        </div>
        <div class="sub_widget badge_widget">
            <div class="widget_title">
                <h5>Manufacture</h5>
            </div>
            <ul>
                <li><a href="#">Hermes<span class="badge">(20)</span></a></li>
                <li><a href="#">Delce & Gabbana<span class="badge">(16)</span></a></li>
                <li><a href="#">Louis Voitton<span class="badge">(18)</span></a></li>
                <li><a href="#">Versace<span class="badge">(16)</span></a></li>
                <li><a href="#">Versace<span class="badge">(22)</span></a></li>
            </ul>
        </div>
        <div class="sub_widget badge_widget">
            <div class="widget_title">
                <h5>Color</h5>
            </div>
            <ul>
                <li><a href="#">White<span class="badge">(20)</span></a></li>
                <li><a href="#">Red<span class="badge">(16)</span></a></li>
                <li><a href="#">Black<span class="badge">(18)</span></a></li>
                <li><a href="#">Blue<span class="badge">(16)</span></a></li>
            </ul>
        </div>
        <div class="sub_widget badge_widget">
            <div class="widget_title">
                <h5>Size</h5>
            </div>
            <ul>
                <li><a href="#">X - Small<span class="badge">(20)</span></a></li>
                <li><a href="#">Small<span class="badge">(16)</span></a></li>
                <li><a href="#">Medium<span class="badge">(18)</span></a></li>
                <li><a href="#">Large<span class="badge">(16)</span></a></li>
            </ul>
        </div>
    </aside>
    <aside class="sidebar_widget">
        <div class="widget_title">
            <h4 class="special_border_right">Tags</h4>
        </div>
        <div class="tags_widget">
            <a href="#" class="tag">Jacket</a>
            <a href="#" class="tag">Men Store</a>
            <a href="#" class="tag">Shop</a>
            <a href="#" class="tag">Dress</a>
            <a href="#" class="tag">Fashion</a>
            <a href="#" class="tag">Women</a>
            <a href="#" class="tag">Jeen</a>
            <a href="#" class="tag">Footwear</a>
        </div>
    </aside>
    <aside class="sidebar_widget">
        <div class="widget_title">
            <h4 class="special_border_right">Gallery</h4>
        </div>
        <div class="sidebar_image_gallery">
            <p>You have no item to compare</p>
            <a href="#"><img src="dist/img/compare-one.png" alt=""></a>
        </div>
    </aside>
</div>