    <!-- Stretch Gallery containter -->
    <div page="Home" class="page_class" id="stretch_gallery1">
        <div class="sg">

            <!-- Slide begining -->
            <div class="sg_slide" style="background: url('dist/img/1a.jpg')">
                <a href="{{ URL::to('list')}}">
                    <div class="sg_dummy">

                                <span class="sg_text_block">
                                    <div class="sg_caption">Legoland</div>
                                    <div class="sg_description">Let the kids take control for a little while</div>
                                </span>

                    </div>
                </a>
            </div>
            <!-- End of slide -->
            <!-- Slide begining -->
            <div class="sg_slide" style=" background: url('dist/img/2a.jpg')">
                <a href="{{ URL::to('list')}}">
                    <div class="sg_dummy">

                            <span class="sg_text_block">
                                <div class="sg_caption">Universal Studios & Islands of Adventure</div>
                                <div class="sg_description">Explore the magic at the wizarding world of Harry Potter</div>
                            </span>

                    </div>
                </a>
            </div>
            <!-- End of slide -->
            <!-- Slide begining -->
            <div class="sg_slide" style=" background: url('dist/img/3a.jpg')">
                <a href="{{ URL::to('list')}}">
                    <div class="sg_dummy">

                        <span class="sg_text_block">
                            <div class="sg_caption">Disney World</div>
                            <div class="sg_description">Experience the happiest place in the world</div>
                        </span>

                    </div>
                </a>
            </div>
            <!-- End of slide -->
            <!-- Slide begining -->
            <div class="sg_slide" style=" background: url('dist/img/4a.jpg')">
                <a href="{{ URL::to('list')}}">
                    <div class="sg_dummy">

                    <span class="sg_text_block">
                        <div class="sg_caption">Sea World</div>
                        <div class="sg_description">Fish are friends, not food!</div>
                    </span>

                    </div>
                </a>
            </div>
            <!-- End of slide -->
            <!-- Slide begining -->
            <div class="sg_slide" style=" background: url('dist/img/5a.jpg')">
                <a href="{{ URL::to('list')}}">
                    <div class="sg_dummy">

                    <span class="sg_text_block">
                        <div class="sg_caption">Orlando 360</div>
                        <div class="sg_description">Get ready for some serious fun!</div>
                    </span>

                    </div>
                </a>
            </div>
            <!-- End of slide -->
        </div>
    </div>
    <!-- End of Stretch Gallery containter -->