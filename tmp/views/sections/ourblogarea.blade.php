<!-- Start Our Blog -->
<section class="our_blog_area">
    <div class="container">
        <div class="row">
            <div class="our_blog">
                <div class="row">
                    <div class="col-md-9">
                        <div class="blog_content">
                            <div class="single_post">
                                <div class="post_content">
                                    <img src="dist/img/blog_post_large_img.png" alt="">
                                    <h4 class="post_title">30 Beautiful and Creative Ecommerce Website</h4>

                                    <div class="post_info">
                                        <span><a href="#">By Admin</a></span>
                                        -
                                        <span><a href="#">Dec 23,2014</a></span>
                                    </div>
                                    <p>
                                        Cras condimentum venenatis enim a facilisis. Fusce in mauris velit. Ut ipsum
                                        lectus, laoreet ac condimentum nec, rhoncus ac augue. Maecenas sollicitudin
                                        euismod congue. Nunc in vestibulum arcu. Donec vitae enim est. Phasellus
                                        fermentum odio quam. Donec aliquam lacinia nisi, idrhs oncus sapien pharetra ac.
                                        Quisque sed pharetra eros, at molestie erat
                                    </p>
                                    <img class="floatright" src="dist/img/blog_post_small_img.png" alt="">

                                    <p class="floatleft">
                                        Cras condimentum venenatis enim a facilisis. Fusce in mauris velit. Utsd ipsum
                                        lectus, laoreet ac condimentum nec, rhoncus ac augue. Maec enas sollicitudin
                                        euismod congue. Nunc in vestibulum arcu. Donec vita enims est. Phasellus
                                        fermentum odio quam. Donec aliquam lacini a nisi idrhses oncus sapien pharetra
                                        ac. Quisque sed pharetra eros, at molestie eratsd. Maecenas quis risus
                                        tristique, vehicula sem. Donec vitae enim est. Phas ellus fermentum odio quam.
                                        Donec vitae enim est. Donec vitae enim. Pha ellus fermentum odio quam.
                                    </p>

                                    <p>
                                        Cras condimentum venenatis enim a facilisis. Fusce in mauris velit. Ut ipsum
                                        lectus, laoreet ac condimentum nec, rhoncus ac augue. Maecenas sollicitudin
                                        euismod congue. Nunc in vestibulum arcu. Donec vitae enim est. Phasellus
                                        fermentum odio quam. Donec aliquam lacinia nisi, idrhs oncus sapien pharetra ac.
                                        Quisque sed pharetra eros, at molestie erat. Maecenas quis risus tristique,
                                        vehicula sem. Donec vitae enim est. Phas ellus fermentum odio quam. Donec
                                        aliquam lacinia nisi, idrhs oncus sapien pharetra ac.
                                    </p>
                                </div>
                                <div class="row">
                                    <div class="blog_tag_social fix">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding-left">
                                            <div class="tag_share">
                                                <p>
                                                    <span>Tags:</span>
                                                    <a href="#">Rokan, </a>
                                                    <a href="#">blog, </a>
                                                    <a href="#">PSD, </a>
                                                    <a href="#">Wordpress</a>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding-right">
                                            <ul class="social_share">
                                                <li>Share:</li>
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="comment_section">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <h4 class="section_title">Comments <a href="#"><span
                                                            class="badge">(04)</span></a></h4>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                            <div class="row">
                                                <div class="commentator_img">
                                                    <img src="dist/img/commentator-img-large.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-10 col-md-10 no-padding-left col-sm-10 col-xs-10">
                                            <div class="section_title">
                                                <h5>Doe Jone</h5>
                                            </div>
                                            <ol class="breadcrumb">
                                                <li class="active"><a href="#">03 mins ago</a></li>
                                                <li><a href="#">Repost</a></li>
                                                <li><a href="#">Reply</a></li>
                                            </ol>
                                            <p>
                                                Aliquam tristique bibendum velit vel pellentesque. Morbi eget semper
                                                ipsum. Maecenas cursus perdiet leo, egestas ullamcorper mauris mattis
                                                et. Aenean posuere feugiat fermentum serts. Maecenas mentum sollicitudin
                                                congue.
                                            </p>

                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="double_comment">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                            <div class="row">
                                                                <div class="commentator_img">
                                                                    <img src="dist/img/commentator-img-large.png" alt="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no-padding-left">
                                                            <div class="section_title">
                                                                <h5>Doe Jone</h5>
                                                            </div>
                                                            <ol class="breadcrumb">
                                                                <li class="active"><a href="#">03 mins ago</a></li>
                                                                <li><a href="#">Repost</a></li>
                                                                <li><a href="#">Reply</a></li>
                                                            </ol>
                                                            <p>
                                                                Aliquam tristique bibendum velit vel pellentesque. Morbi
                                                                eget semper ipsum. Maecenas cursus perdiet leo, egestas
                                                                ullamcorper mauris mattis et. Aenean posuere feugiat
                                                                fermentum serts. Maecenas mentum sollicitudin congue.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="double_comment">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                            <div class="row">
                                                                <div class="commentator_img">
                                                                    <img src="dist/img/commentator-img-large.png" alt="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no-padding-left">
                                                            <div class="section_title">
                                                                <h5>Doe Jone</h5>
                                                            </div>
                                                            <ol class="breadcrumb">
                                                                <li class="active"><a href="#">03 mins ago</a></li>
                                                                <li><a href="#">Repost</a></li>
                                                                <li><a href="#">Reply</a></li>
                                                            </ol>
                                                            <p>
                                                                Aliquam tristique bibendum velit vel pellentesque. Morbi
                                                                eget semper ipsum. Maecenas cursus perdiet leo, egestas
                                                                ullamcorper mauris mattis et. Aenean posuere feugiat
                                                                fermentum serts. Maecenas mentum sollicitudin congue.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="comment_section">
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                            <div class="row">
                                                <div class="commentator_img">
                                                    <img src="dist/img/commentator-img-large.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-10 col-md-10 no-padding-left col-sm-10 col-xs-10">
                                            <div class="section_title">
                                                <h5>Rokan Theme</h5>
                                            </div>
                                            <ol class="breadcrumb">
                                                <li class="active"><a href="#">03 mins ago</a></li>
                                                <li><a href="#">Repost</a></li>
                                                <li><a href="#">Reply</a></li>
                                            </ol>
                                            <p>
                                                Aliquam tristique bibendum velit vel pellentesque. Morbi eget semper
                                                ipsum. Maecenas cursus perdiet leo, egestas ullamcorper mauris mattis
                                                et. Aenean posuere feugiat fermentum serts. Maecenas mentum sollicitudin
                                                congue.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="leave_comment">
                                        <div class="col-md-12">
                                            <h6 class="section_title">Leave a Comment</h6>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="comment_form">
                                                <input type="text" placeholder="Your name">
                                                <input type="email" placeholder="Your email">
                                                <input type="text" placeholder="Your website">
                                                <textarea name="" cols="30" rows="10"
                                                          placeholder="Your comment"></textarea>
                                                <a href="#" class="btn btn_reverse">Send Message</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="blog_sidebar">
                            <aside class="sidebar_widget">
                                <div class="widget_title">
                                    <h4>Category</h4>
                                </div>
                                <ul>
                                    <li><a href="#">Web Design</a></li>
                                    <li><a href="#">PSD Template</a></li>
                                    <li><a href="#">WordPress</a></li>
                                    <li><a href="#">E-Commerce</a></li>
                                    <li><a href="#">Magento</a></li>
                                    <li><a href="#">Prestashop</a></li>
                                </ul>
                            </aside>
                            <aside class="sidebar_widget">
                                <div class="widget_title">
                                    <h4>Category</h4>
                                </div>
                                <div class="side_post">
                                    <ul>
                                        <li>
                                            <a href="#">sep 29, 2014</a>

                                            <p>Quisque faucibus enim non tempus gravis da Morbi non sem sagittis,
                                                venenatis neqd ue. consequat justo sem <a href="#">[ more ]</a></p>
                                        </li>
                                        <li>
                                            <a href="#">sep 29, 2014</a>

                                            <p>Quisque faucibus enim non tempus gravis da Morbi non sem sagittis,
                                                venenatis neqd ue. consequat justo sem <a href="#">[ more ]</a></p>
                                        </li>
                                        <li>
                                            <a href="#">sep 29, 2014</a>

                                            <p>Quisque faucibus enim non tempus gravis da Morbi non sem sagittis,
                                                venenatis neqd ue. consequat justo sem <a href="#">[ more ]</a></p>
                                        </li>
                                    </ul>

                                </div>
                            </aside>
                            <aside class="sidebar_widget">
                                <div class="widget_title">
                                    <h4>Archive</h4>
                                </div>
                                <ul>
                                    <li><a href="#">September 2014</a></li>
                                    <li><a href="#">August 2014</a></li>
                                    <li><a href="#">July 2014</a></li>
                                    <li><a href="#">October 2013</a></li>
                                    <li><a href="#">January 2013</a></li>
                                    <li><a href="#">December 2012</a></li>
                                </ul>
                            </aside>
                            <aside class="sidebar_widget">
                                <div class="widget_title">
                                    <h4>Tags</h4>
                                </div>
                                <div class="tags_widget">
                                    <a href="#" class="tag">Fashion</a>
                                    <a href="#" class="tag">Women’s</a>
                                    <a href="#" class="tag">Child</a>
                                    <a href="#" class="tag">Lookbook</a>
                                    <a href="#" class="tag">Accessories</a>
                                    <a href="#" class="tag">Dress</a>
                                    <a href="#" class="tag">Sale Off</a>
                                    <a href="#" class="tag">Accessories</a>
                                    <a href="#" class="tag">Clothing</a>
                                    <a href="#" class="tag">Men's</a>
                                    <a href="#" class="tag">Carry bag</a>
                                    <a href="#" class="tag">Clothing</a>
                                </div>
                            </aside>
                            <aside class="sidebar_widget">
                                <div class="widget_title">
                                    <h4>Gallery</h4>
                                </div>
                                <div class="sidebar_image_gallery">
                                    <a href="#"><img src="dist/img/sidebar_image_one.png" alt=""></a>
                                    <a href="#"><img src="dist/img/sidebar_image_two.png" alt=""></a>
                                    <a href="#"><img src="dist/img/sidebar_image_three.png" alt=""></a>
                                    <a href="#"><img src="dist/img/sidebar_image_four.png" alt=""></a>
                                    <a href="#"><img src="dist/img/sidebar_image_five.png" alt=""></a>
                                    <a href="#"><img src="dist/img/sidebar_image_six.png" alt=""></a>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Our Our Blog -->