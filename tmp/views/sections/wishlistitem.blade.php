@if(count($wishlist) == 0)
<tr>
    <td class="text-center" colspan="5"><h4 class="p-md">Your wishlist are empty for now!</h4></td>
</tr>
@else
    @foreach($wishlist as $item)
    <tr class="table-product-info">
        <td class="prod-del"><a class="trash-table" href="/product/{{ $item->id }}"><i class="fa fa-trash"></i></a></td>
        <td class="prod-img-center">
            <div class="prod-table-img-c"><img class="wishlistimg" alt="product-name" src="http://placehold.it/96x123" >
            </div>
        </td>
        <td class="prod-name">
            <p><a><b>{{ $item->title }}</b></a></p>
            <p>Start Sell Date: <span>{{ $item->start_sell_date }}</span></p>
            <p>End Sell Date: <span>{{ $item->end_sell_date }}</span></p>
            <p>Min Age: <span>{{ $item->min_age }}</span></p>
        </td>
        <td class="prod-tot">
            {{--        <b data-ng-bind="item.Receive_Email"></b>--}}
            <input type="checkbox"
                   parse-int=""
                   class="">
        </td>
        <td class="prod-tot">
            {{--<span>No actions available for now</span>--}}
            <p class="btn btn-block">Add to cart</p>
            <p class="btn btn-block">Buy now</p>
        </td>
    </tr>
    @endforeach
@endif
