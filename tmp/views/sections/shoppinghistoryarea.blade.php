<!-- Start Shopping Cart -->
<section class="shopping-cart-area">
    <div class="container">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tbody>
                    <tr class="table-title">
                        <td class="cart-img">Image</td>
                        <td class="cart-name">Product Name</td>
                        <td class="cart-qty">Quantity</td>
                        <td class="cart-price">Grandtotal</td>
                        <td class="cart-price">Shipping method</td>
                    </tr>
                    @include('sections.orderitem')
                    <tr>
                        <td colspan="7">
                            <a style="margin: 10px;" href="{{URL::to('list')}}" class="left_table_btn btn btn_reverse floatleft">continue shopping</a>
                            <a style="margin: 10px;" href="{{URL::to('shopping-cart')}}" class="right_table_btn btn btn_reverse floatright">View shopping cart</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!-- End Shopping Cart -->