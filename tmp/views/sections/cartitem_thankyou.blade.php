<tr class="table-product-info">
    <td class="prod-img-center">
        <div class="prod-table-img-c"><img alt="product-name" src="http://placehold.it/96x123">
        </div>
    </td>
    <td class="prod-name"><span> Title</span></td>
    <td class="prod-price">
        <span>Min age:<span></span></span>
        <span>Adult<br><input disabled type="number" min="1"  max="1000" required><br></span>
        <span>Child<br><input disabled type="number" min="0"  max="1000" required><br></span>
        <span>Infant<br><input disabled type="number" min="0"  max="1000" required></span>
    </td>
{{--    <td class="prod-qty">
        <span data-ng-bind="dollarIcon"></span><span ng-bind="(appCtrl.getItemSubtotal(item) * 1).toFixed(2)"></span>
        <span ng-if="currentCurrency != 'USD'">
            <br>(<span data-ng-bind="currentIcon"></span><span ng-bind="(appCtrl.getItemSubtotal(item) * currentIndex).toFixed(2)"></span>)
        </span>
    </td>--}}
    <td class="prod-tot">
        <span></span><span></span>
        <span>
            <br>(<span></span><span></span>)
        </span>
    </td>
    <td class="prod-tot">
        <div class="btn-group" data-toggle="buttons">
            <label id="lb1" class="btn_shippingmethod btn btn-primary">
                Electronic Ticket
            </label>
            <label id="lb2" class="btn_shippingmethod btn btn-primary">
                Ship to Home
            </label>
            <label id="lb3" class="btn_shippingmethod btn btn-primary">
                Pick Up at Ticket Center
            </label>
        </div>
    </td>
</tr>