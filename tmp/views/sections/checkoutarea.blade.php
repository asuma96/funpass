<!-- start checkout area -->
<section class="checkout_area">
    <div class="container">
        <div class="row">
            <div class="checkout fix p-none">
                <div class="">
                    <div class="">
                        <div class="checkout_form">
                            <div class="panel-group" id="accordion_two">
                                <div id="panel1" class="panel panel-default checkout_panel">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-parent="#accordion_two"
                                               href="#collapse_check_One" aria-expanded="true"
                                               aria-controls="collapse_check_One">
                                                <span class="btn">1</span> checkout method
                                                <span class="floatright"><i class="fa fa-minus"></i></span>
                                                <span class="floatright"><i class="fa fa-plus"></i></span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_check_One" class="panel1 panelG panel-collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    @if(!isset($user))
                                                        <div class="register_part">
                                                            <div class="section_title">
                                                                <h6>@{{'Checkout as guest or register an account!'}}</h6>
                                                            </div>
                                                            <div class=" p-t-sm">
                                                                <a href="/registercheckout"
                                                                   class="btn btn_reverse">@{{'Checkout as guest'}}</a>
                                                                <a href="/register" class="btn btn_reverse">Register</a>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    @if(isset($user))
                                                        <div class="register_part">
                                                            <div class="section_title">
                                                                <h6>@{{'Checkout as ' + appCtrl.getUsername() + ' or register an account!'}}</h6>
                                                            </div>
                                                            <div class=" p-t-sm">
                                                                <a
                                                                   class="btn btn_reverse">@{{'Checkout as ' + appCtrl.getUsername() }}</a>
                                                                <a href="/register" class="btn btn_reverse">Register</a>
                                                            </div>
                                                        </div>
                                                    @endif


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="panel2" class="panel panel-default checkout_panel">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-parent="#accordion_two"
                                               href="#collapse_check_Two" aria-expanded="false"
                                               aria-controls="collapse_check_Two">
                                                <span class="btn">2</span> Cart Summary
                                                <span class="floatright"><i class="fa fa-minus"></i></span>
                                                <span class="floatright"><i class="fa fa-plus"></i></span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_check_Two" class="panel2 panelG panel-collapse">
                                        <div class="panel-body body_list_item">
                                            <div class="row">
                                                <div class="table-responsive" style="overflow-x: inherit!important;">
                                                    <table class="table table-bordered table-bordered_kok">
                                                        <tbody>
                                                        <tr class="table-title">
                                                            <td class="cart-del">Remove</td>
                                                            <td class="cart-img">Image</td>
                                                            <td class="cart-name">Product Name</td>
                                                            <td class="cart-qty">Quantity</td>
{{--                                                            <td class="cart-subtotal">Subtotal</td>--}}
                                                            <td class="cart-price">Grandtotal</td>
                                                            <td class="cart-price">Arrive date</td>
                                                            <td class="cart-price">Shipping method</td>
                                                        </tr>
                                                        @include('sections.cartitem')
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 cart_checkout cart_checkout_l">

                                                        <div>
                                                            <div class="subtotal-grandtotal">
                                                                <div class="sub-total">
{{--                                                                    <span class="check-subtot">Subtotal</span><br>--}}
                                                                    <span class="check-grandtot">Grand Total</span>

                                                                </div>
                                                                <div class="grand-total">
{{--                                                                    <span data-ng-bind="dollarIcon"></span><span class="Subtotal" ng-bind="(appCtrl.getSubtotal()*1).toFixed(2)"></span>
                                                                    <span ng-if="currentCurrency != 'USD'">
                                                                        (<span data-ng-bind="currentIcon"></span><span class="Subtotal" ng-bind="(appCtrl.getSubtotal()*currentIndex).toFixed(2)"></span>)
                                                                    </span>
                                                                    <br>--}}

                                                                    <span class="check-grandtot"></span>
                                                                    <span>
                                                                        (<span class="check-grandtot" ></span>)
                                                                    </span>
                                                                    <br>

                                                                </div>
                                                            </div>
                                                            <a class="btn btn_reverse">Continue</a>
                                                            <a class="btn btn_reverse">Continue</a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="panel3" class="panel panel-default checkout_panel">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-parent="#accordion_two"
                                               href="#collapse_check_Three" aria-expanded="false"
                                               aria-controls="collapse_check_Three">
                                                <span class="btn btn_status">3</span> Credit Card Information
                                                <span class="floatright"><i class="fa fa-minus"></i></span>
                                                <span class="floatright"><i class="fa fa-plus"></i></span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_check_Three" class="panel3 panelG panel-collapse" >
                                        <div class="panel-body">
                                            <h6>Credit Card Information</h6>

                                            <div class="btn-group" data-toggle="buttons" style="width: 50%">
                                                <label id="lb1" class="btn_shippingmethod btn btn-primary" style="width: 30%; margin-right:10px" >
                                                    USE PREVIOUS CARD
                                                </label>
                                                <label id="lb2" class="btn_shippingmethod btn btn-primary" style="width: 30%; margin-right:10px">
                                                    EDIT CARD INFO
                                                </label>
                                                <label id="lb3" class="btn_shippingmethod btn btn-primary" style="width: 30%;" >
                                                    DELETE CARD INFO
                                                </label>
                                            </div>

                                            <div>
                                                <a class="btn btn_reverse">Continue</a>
                                                <a class="btn btn_reverse">Continue</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="panel3" class="panel panel-default checkout_panel">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-parent="#accordion_two"
                                               href="#collapse_check_Three" aria-expanded="false"
                                               aria-controls="collapse_check_Three">
                                                <span class="btn btn_status">4</span> Shipping Information
                                                <span class="floatright"><i class="fa fa-minus"></i></span>
                                                <span class="floatright"><i class="fa fa-plus"></i></span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_check_Three" class="panel3 panelG panel-collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="register_part">
                                                        <div class="section_title">
                                                            <h6>Enter your shipping information!</h6>
                                                        </div>
                                                        <div class="login_body register_with_check">
                                                            <form action="#" id="shippingForm">
                                                                <ul>
                                                                    <li class="form-group">
                                                                        <label for="adr">Address <i
                                                                                    class="fa fa-star"></i></label>
                                                                        <input class="form-control" id="address"
                                                                               type="text" name="address">
                                                                    </li>
                                                                    <li class="form-group">
                                                                        <label for="city">City <i
                                                                                    class="fa fa-star"></i></label>
                                                                        <input class="form-control" id="city"
                                                                               type="text" value="" name="city">
                                                                    </li>
                                                                    <li class="form-group">
                                                                        <label for="state">State <i
                                                                                    class="fa fa-star"></i></label>
                                                                        <input class="form-control" id="state"
                                                                               type="text" value="" name="state">
                                                                    </li>
                                                                    <li class="form-group">
                                                                        <label for="country">Country <i
                                                                                    class="fa fa-star"></i></label>
                                                                        <input class="form-control" id="country"
                                                                               type="text" value="" name="country">
                                                                    </li>
                                                                    <li class="form-group">
                                                                        <label for="zip">Zip <i class="fa fa-star"></i></label>
                                                                        <input class="form-control" id="zip" type="text"
                                                                               value="" name="zip">
                                                                    </li>
                                                                </ul>
                                                            </form>
                                                        </div>
                                                        <a class="btn btn_reverse">Continue</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="panel5" class="panel panel-default checkout_panel">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-parent="#accordion_two"
                                               href="#collapse_check_Five" aria-expanded="false"
                                               aria-controls="collapse_check_Five">
                                                <span class="btn btn_status">5</span> Payment Information
                                                <span class="floatright"><i class="fa fa-minus"></i></span>
                                                <span class="floatright"><i class="fa fa-plus"></i></span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_check_Five" class="panel5 panelG panel-collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="register_part">
                                                        <div class="section_title">
                                                            <h6>Enter your payment card info!</h6>
                                                        </div>
                                                        <div class="login_body register_with_check">
                                                            <form action="#" id="cardForm">
                                                                <ul>
                                                                    <li class="form-group">
                                                                        <label for="cardnumber">Card number <i
                                                                                    class="fa fa-star"></i></label>
                                                                        <input class="form-control" id="cardnumber"
                                                                               type="text" name="ccNumber"
                                                                               data-stripe="number"">
                                                                    </li>
                                                                    <li class="form-group">
                                                                        <label for="nameoncard">Name on card <i
                                                                                    class="fa fa-star"></i></label>
                                                                        <input class="form-control" id="nameoncard"
                                                                               type="text" value="" name="fullName">
                                                                    </li>
                                                                    <li class="form-group">
                                                                        <label for="month">Expire month <i
                                                                                    class="fa fa-star"></i></label>
                                                                        <input class="form-control payment_month"
                                                                               id="month" type="text" value=""
                                                                               data-stripe="exp-month" name="expMonth">
                                                                    </li>
                                                                    <li class="form-group">
                                                                        <label for="year">Expire year <i
                                                                                    class="fa fa-star"></i></label>
                                                                        <input class="form-control payment_year"
                                                                               id="year" type="text" value=""
                                                                               data-stripe="exp-year" name="expYear">
                                                                    </li>
                                                                    <li class="form-group">
                                                                        <label for="CCV">CCV <i class="fa fa-star"></i></label>
                                                                        <input class="form-control" id="CCV" type="text"
                                                                               value="" data-stripe="cvc"
                                                                               name="cvvNumber">
                                                                    </li>
                                                                </ul>
                                                            </form>
                                                        </div>
                                                        <a class="btn btn_reverse">Continue</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="panel6" class="panel panel-default checkout_panel">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-parent="#accordion_two"
                                               href="#collapse_check_Six" aria-expanded="false"
                                               aria-controls="collapse_check_Six">
                                                <span class="btn">6</span> Order Review
                                                <span class="floatright"><i class="fa fa-minus"></i></span>
                                                <span class="floatright"><i class="fa fa-plus"></i></span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_check_Six" class="panel6 panelG panel-collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="register_part">
                                                        <div class="section_title">
                                                            <h6 class="section_title_l">Check your information and
                                                                proceed to pay</h6>
                                                        </div>
                                                        <br/>

                                                        <div class="row">
                                                            <h6 class="section_title_r">@{{'Guest name: ' + appCtrl.getUsername()}}</h6>
                                                        </div>

                                                        <div class="row">
                                                            <div class="table-responsiv body_list_item_l">
                                                                <table class="table table-bordered">
                                                                    <tbody>
                                                                    <tr class="table-title">
                                                                        <td class="cart-del">Remove</td>
                                                                        <td class="cart-img">Image</td>
                                                                        <td class="cart-name">Product Name</td>
                                                                        <td class="cart-qty">Quantity</td>
{{--                                                                        <td class="cart-subtotal">Subtotal</td>--}}
                                                                        <td class="cart-price">Grandtotal</td>
                                                                        <td class="cart-price">Arrive date</td>
                                                                        <td class="cart-price">Shipping method</td>
                                                                    </tr>
                                                                    @include('sections.cartitem')
                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                        </div>
                                                        <div class="clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-4 cart_checkout subtotal-grandtotal_l">

                                                                <div>
                                                                    <div class="subtotal-grandtotal">
                                                                        <div class="clearfix p-b-sm">
                                                                            <div class="sub-total">
{{--                                                                                <span class="check-subtot">Subtotal</span><br>--}}
                                                                                <span class="check-grandtot">Grand Total</span>

                                                                            </div>
                                                                            <div class="grand-total">
   {{--                                                                             <span data-ng-bind="dollarIcon"></span><span class="Subtotal" ng-bind="(appCtrl.getSubtotal()*1).toFixed(2)"></span>
                                                                                <span ng-if="currentCurrency != 'USD'">
                                                                                    (<span data-ng-bind="currentIcon"></span><span class="Subtotal" ng-bind="(appCtrl.getSubtotal()*currentIndex).toFixed(2)"></span>)
                                                                                </span>
                                                                                <br>--}}

                                                                                <span></span><span class="check-grandtot GrandTotal"></span>
                                                                                <span>
                                                                                    (<span></span><span class="check-grandtot GrandTotal"></span>)
                                                                                </span>
                                                                                <br>

                                                                            </div>
                                                                        </div>
                                                                        {{--<a data-ng-click="block_confirm_order_btn? '' : appCtrl.confirmCheckout()"--}}
                                                                           {{--ng-class="{'disabled': appCtrl.cart<=0}"--}}
                                                                           {{--class="btn btn-default btn_reverse confirm_order_btn m-t-sm has-spinner">--}}
                                                                            {{--<span class="spinner"><i class="fa fa-spinner fa-spin fa-fw"></i></span>--}}
                                                                            {{--Confirm Order--}}
                                                                        {{--</a> --}}
                                                                        <a class="btn btn-default btn_reverse confirm_order_btn m-t-sm has-spinner">
                                                                            <span class="spinner"><i class="fa fa-spinner fa-spin fa-fw"></i></span>
                                                                            Confirm Order
                                                                        </a>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="subtotal-grandtotal_r">
                                                                <div class="padding_b">
                                                                    <h6>Shipping address:</h6>
                                                                    <strong class="line-hh"></strong>
                                                                </div>
                                                                {{--<br/>--}}
                                                                {{--                                                                <div class="padding_b" data-ng-if="appCtrl.payment_method">
                                                                                                                                    <h6>Shipping method:</h6>
                                                                                                                                    <strong class="line-hh">Method @{{ appCtrl.payment_method }}</strong>
                                                                                                                                </div>--}}
                                                                {{--<br/>--}}
                                                                <div class="padding_b">
                                                                    <h6>Payment information:</h6>

                                                                    <p class="line-hh">
                                                                        Card number: <strong></strong><br/>
                                                                        Cardholder name: <strong></strong><br/>
                                                                        Expiration date: <strong></strong>/<strong></strong><br/>
                                                                        CCV: <strong></strong><br/>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End checkout area -->

