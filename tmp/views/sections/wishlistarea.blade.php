<!-- Start Shopping Cart -->
<section class="shopping-cart-area">
    <div class="container">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tbody>
                    <tr class="table-title">
                        <td class="cart-img">Remove</td>
                        <td class="cart-img">Image</td>
                        <td class="cart-name">Product information</td>
                        <td class="cart-price">Receive news by email</td>
                        <td class="cart-price">Actions</td>
                    </tr>
                    @include('sections.wishlistitem')
                    <tr>
                        <td colspan="7">
                            <a style="margin: 10px;" href="{{URL::to('list')}}" class="left_table_btn btn btn_reverse floatleft">continue shopping</a>
                            <a style="margin: 10px;" href="{{URL::to('shopping-cart')}}" class="right_table_btn btn btn_reverse floatright">View shopping cart</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!-- End Shopping Cart -->