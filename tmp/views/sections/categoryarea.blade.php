<!-- start category area -->
<div class="category_area">
    <div class="container">
        <div class="row">
            <div class="category">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-12">
                        @include('sections.categorysidebar')
                    </div>
                    <div id="park-info" class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Epcot®</h2>
                                <h4>Disney World® - Orlando</h4>

                                <div class="product_item_details no_border_with_50_pad search-pages">
                                    <div role="tabpanel">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#images"
                                                                                      aria-controls="review" role="tab"
                                                                                      data-toggle="tab">images</a></li>
                                            <li role="presentation"><a href="#description" aria-controls="description"
                                                                       role="tab" data-toggle="tab">description</a></li>
                                            <li role="presentation"><a href="#videos" aria-controls="tags" role="tab"
                                                                       data-toggle="tab">videos</a></li>
                                            <li role="presentation"><a href="#map" aria-controls="tags" role="tab"
                                                                       data-toggle="tab">map</a></li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane" id="description">
                                                <p>
                                                    High tech fun and Disney imagination combine with the wonder of
                                                    diverse cultures in two distinct realms: Future World (witness the
                                                    wonders of tomorrow, today) and World Showcase (visit 11 countries
                                                    in one day). Both promise thrills and excitement in over 30 amazing
                                                    attractions – these are the ones not to miss.
                                                </p>

                                                <p><h5>Fun for little ones:</h5>
                                                <ul class="disneyBullet">
                                                    <li>The Seas with Nemo & Friends (inspired by Disney·Pixar’s
                                                        ‘Finding Nemo’): board a ‘clamobile’ to see what happened to
                                                        Nemo after the movie
                                                    </li>
                                                    <li>Epcot Character Spot: grab your autograph book and camera – your
                                                        favourite Characters are waiting to meet you!
                                                    </li>
                                                    <li>Turtle Talk with Crush (inspired by Disney·Pixar’s ‘Finding
                                                        Nemo’): hang out with the totally tubular turtle in this
                                                        interactive show
                                                    </li>
                                                </ul>
                                                </p>
                                                <p><h5>Big thrills:</h5>
                                                <ul class="disneyBullet">
                                                    <li>Mission: SPACE: blast into the most thrilling ride ever –
                                                        whether the full force original or an exciting but milder
                                                        journey, both finish with a landing on Mars
                                                    </li>
                                                    <li>Test Track: buckle up to discover just how fast, fast really is
                                                        on this high-octane thrill ride
                                                    </li>
                                                    <li>Soarin’™: fly over spectacular California in this simulated
                                                        hang-glider journey – just like a bird in flight
                                                    </li>
                                                </ul>
                                                </p>
                                                <p><h5>Family Fun:</h5>
                                                <ul class="disneyBullet">
                                                    <li>Spaceship Earth: journey 40,000 years from the dawn of recorded
                                                        time to modern day, before seeing how we might live in the
                                                        future
                                                    </li>
                                                    <li>Disney Phineas and Ferb: Agent P's World Showcase Adventure:
                                                        become a secret agent and go undercover on this interactive
                                                        quest around World Showcase
                                                    </li>
                                                    <li>IllumiNations: Reflections of Earth: finish with this nightly
                                                        spectacular of fireworks, lasers and high-energy soundtrack
                                                    </li>
                                                </ul>
                                                </p>


                                            </div>
                                            <div role="tabpanel" class="tab-pane active" id="images">
                                                <div id="slider" class="flexslider">
                                                    <ul class="slides">
                                                        <li>
                                                            <img src="dist/img/epcot/e1.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e2.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e4.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e5.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e6.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e7.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e8.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e9.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e10.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e11.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e12.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e13.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e14.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e15.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e16.jpg"/>
                                                        </li>
                                                        <!-- items mirrored twice, total of 12 -->
                                                    </ul>
                                                </div>
                                                <div id="carousel" class="flexslider">
                                                    <ul class="slides">
                                                        <li>
                                                            <img src="dist/img/epcot/e1.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e2.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e4.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e5.jpg"/>
                                                        </li>
                                                        <li>

                                                            <img src="dist/img/epcot/e6.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e7.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e8.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e9.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e10.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e11.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e12.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e13.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e14.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e15.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="dist/img/epcot/e16.jpg"/>
                                                        </li>
                                                        <!-- items mirrored twice, total of 12 -->
                                                    </ul>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="videos">
                                                <p>
                                                    Quisque eget elit purus. Vivamus dictum rutrum ipsum, quist ornare
                                                    lorem iaculis nec. Sed volutpat tincdunt justo eget lob ortis.
                                                    Phasellus mos lestie rutrum lorem sit. Quisque eget elit purus.
                                                    Vivamus dictum rutrum ipsumi. Phasellus molestie rutrum lorem sit.
                                                    Quisque eget elit purus be Vivamus dictum rutrum ipsumi. Quisque
                                                    eget elit purus. Vivamus dictum rutrum ipsum, quist ornare lorem
                                                    iaculis nec. Sed volutpat tincdunt jus to eget lob ortis. Phasellus
                                                    molestie rutrum lorem sit. Quisque eget elit purus. Vivamus dictum
                                                    rutrum ipsumi.
                                                </p>

                                                <p>
                                                    Vivamus dictum rutrum ipsum, quist ornare lorem iaculis nec. Sed
                                                    volutpat tincdunt justo eget lob ortis. Phasellus mos lestie rutrum
                                                    lorem sit. Quisque eget elit purus. Vivamus dictum rutrum ipsumi.
                                                    Phasellus molestie rutrum lorem sit. Quisque eget elit purus be
                                                    Vivamus dictum rutru m ipsumi. Quisque eget elit purus. Vivamus
                                                    dictum rutrum ipsum, quist ornare lorem iaculis nec. Quisque eget
                                                    elit purus be Vivamus dictum rutru m ipsumi. Quisque eget elit
                                                    purus. Vivamus dictum rutrum ipsum, quist ornare lorem iaculis nec.
                                                </p>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="map">
                                                <img src="dist/img/epcot/epcot-map.jpg"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="category_products">
                            <div class="row">
                                <div class="list_view">
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <!--  <div class="sorting">
                  <label>
                      <select>
                          <option selected> Default Sorting </option>
                          <option>Grid View</option>
                          <option>List View</option>
                      </select>
                  </label>
              </div>-->
                                    </div>
                                    <div>

                                    </div>
                                </div>
                            </div>
                            <!--Start Row View-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="product_item_details no_border_with_50_pad ticketChooser">
                                        <div role="tabpanel">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#tickets"
                                                                                          aria-controls="description"
                                                                                          role="tab" data-toggle="tab">tickets</a>
                                                </li>
                                                <li role="presentation"><a href="#packages" aria-controls="review"
                                                                           role="tab" data-toggle="tab">packages</a>
                                                </li>
                                                <li role="presentation"><a href="#offers" aria-controls="tags"
                                                                           role="tab" data-toggle="tab">offers</a></li>
                                                <div class="sorting_view floatright">
                                                    <i id="theGrid" class="fa fa-th deactive"></i>
                                                    <i id="theRow" class="fa fa-list"></i>
                                                </div>
                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="tickets">
                                                    <div id="rowView">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="single_list_product">
                                                                    <div class="col-md-4 no-padding-left">
                                                                        <div class="single_list_image">
                                                                            <div class="single_tab_content">
                                                                                <figure class="effect-lexi">
                                                                                    <a href="#"><img alt=""
                                                                                                     src="dist/img/tab-image-two-without-overlay.png"></a>
                                                                                    <figcaption>
                                                                                        <p class="add_to_cart">30%
                                                                                            off</p>
                                                                                    </figcaption>
                                                                                </figure>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-8  no-padding-right">
                                                                        <div class="single_list_details">
                                                                            <div class="product_description">
                                                                                <a href="#">Et harum quidem rerum
                                                                                    dress</a>

                                                                                <div class="star">
                                                                                    <a href="#">
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star-o"></i>
                                                                                        <i class="fa fa-star-o"></i>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="price">
                                                                                    <p>
                                                                                        <span class="old_price">$409.00</span>
                                                                                        <span class="new_price">$369.00</span>
                                                                                    </p>
                                                                                </div>
                                                                                <div class="product_detail">
                                                                                    <p>
                                                                                        Quisque eget elit purus. Vivamus
                                                                                        dictum rutrum ipsum, quist
                                                                                        ornare lorem iaculis nec. Sed
                                                                                        volutpat tincdunt justo eget lob
                                                                                        ortis. Phasellus molestie rutrum
                                                                                        lorem sit. Quisque eget elit
                                                                                        purus. Vivamus dictum rutrum
                                                                                        ipsumi. Phasellus molestie
                                                                                        rutrum lorem sit. Quisque eget
                                                                                        elit purus. Vivamus dictum
                                                                                        rutrum ipsumi.
                                                                                    </p>
                                                                                </div>
                                                                                <ul class="social_icons">
                                                                                    <li><a class="add_cart_btn"
                                                                                           href="#">Add to cart</a></li>
                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-refresh"></i></a>
                                                                                    </li>
                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-heart"></i></a>
                                                                                    </li>
                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-search"></i></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="single_list_product">
                                                                    <div class="col-md-4 no-padding-left">
                                                                        <div class="single_list_image">
                                                                            <div class="single_tab_content">
                                                                                <figure class="effect-lexi">
                                                                                    <a href="#"><img alt=""
                                                                                                     src="dist/img/tab-image-four.png"></a>
                                                                                    <figcaption>
                                                                                        <p class="add_to_cart">20%
                                                                                            off</p>
                                                                                    </figcaption>
                                                                                </figure>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-8  no-padding-right">
                                                                        <div class="single_list_details">
                                                                            <div class="product_description">
                                                                                <a href="#">Et harum quidem rerum
                                                                                    dress</a>

                                                                                <div class="star">
                                                                                    <a href="#">
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star-o"></i>
                                                                                        <i class="fa fa-star-o"></i>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="price">
                                                                                    <p>
                                                                                        <span class="old_price">$409.00</span>
                                                                                        <span class="new_price">$369.00</span>
                                                                                    </p>
                                                                                </div>
                                                                                <div class="product_detail">
                                                                                    <p>
                                                                                        Quisque eget elit purus. Vivamus
                                                                                        dictum rutrum ipsum, quist
                                                                                        ornare lorem iaculis nec. Sed
                                                                                        volutpat tincdunt justo eget lob
                                                                                        ortis. Phasellus molestie rutrum
                                                                                        lorem sit. Quisque eget elit
                                                                                        purus. Vivamus dictum rutrum
                                                                                        ipsumi. Phasellus molestie
                                                                                        rutrum lorem sit. Quisque eget
                                                                                        elit purus. Vivamus dictum
                                                                                        rutrum ipsumi.
                                                                                    </p>
                                                                                </div>
                                                                                <ul class="social_icons">
                                                                                    <li><a class="add_cart_btn"
                                                                                           href="#">Add to cart</a></li>
                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-refresh"></i></a>
                                                                                    </li>
                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-heart"></i></a>
                                                                                    </li>
                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-search"></i></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="single_list_product">
                                                                    <div class="col-md-4 no-padding-left">
                                                                        <div class="single_list_image">
                                                                            <div class="single_tab_content">
                                                                                <figure class="effect-lexi">
                                                                                    <a href="#"><img alt=""
                                                                                                     src="dist/img/tab-image-three.png"></a>
                                                                                    <figcaption>
                                                                                        <p class="add_to_cart">Hot</p>
                                                                                    </figcaption>
                                                                                </figure>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-8  no-padding-right">
                                                                        <div class="single_list_details">
                                                                            <div class="product_description">
                                                                                <a href="#">Et harum quidem rerum
                                                                                    dress</a>

                                                                                <div class="star">
                                                                                    <a href="#">
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star-o"></i>
                                                                                        <i class="fa fa-star-o"></i>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="price">
                                                                                    <p>
                                                                                        <span class="old_price">$409.00</span>
                                                                                        <span class="new_price">$369.00</span>
                                                                                    </p>
                                                                                </div>
                                                                                <div class="product_detail">
                                                                                    <p>
                                                                                        Quisque eget elit purus. Vivamus
                                                                                        dictum rutrum ipsum, quist
                                                                                        ornare lorem iaculis nec. Sed
                                                                                        volutpat tincdunt justo eget lob
                                                                                        ortis. Phasellus molestie rutrum
                                                                                        lorem sit. Quisque eget elit
                                                                                        purus. Vivamus dictum rutrum
                                                                                        ipsumi. Phasellus molestie
                                                                                        rutrum lorem sit. Quisque eget
                                                                                        elit purus. Vivamus dictum
                                                                                        rutrum ipsumi.
                                                                                    </p>
                                                                                </div>
                                                                                <ul class="social_icons">
                                                                                    <li><a class="add_cart_btn"
                                                                                           href="#">Add to cart</a></li>
                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-refresh"></i></a>
                                                                                    </li>
                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-heart"></i></a>
                                                                                    </li>
                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-search"></i></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="single_list_product">
                                                                    <div class="col-md-4 no-padding-left">
                                                                        <div class="single_list_image">
                                                                            <div class="single_tab_content">
                                                                                <figure class="effect-lexi">
                                                                                    <a href="#"><img alt=""
                                                                                                     src="dist/img/tab-image-two.png"></a>
                                                                                    <figcaption>
                                                                                        <p class="add_to_cart">40%
                                                                                            off</p>
                                                                                    </figcaption>
                                                                                </figure>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-8  no-padding-right">
                                                                        <div class="single_list_details">
                                                                            <div class="product_description">
                                                                                <a href="#">Et harum quidem rerum
                                                                                    dress</a>

                                                                                <div class="star">
                                                                                    <a href="#">
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star-o"></i>
                                                                                        <i class="fa fa-star-o"></i>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="price">
                                                                                    <p>
                                                                                        <span class="old_price">$409.00</span>
                                                                                        <span class="new_price">$369.00</span>
                                                                                    </p>
                                                                                </div>
                                                                                <div class="product_detail">
                                                                                    <p>
                                                                                        Quisque eget elit purus. Vivamus
                                                                                        dictum rutrum ipsum, quist
                                                                                        ornare lorem iaculis nec. Sed
                                                                                        volutpat tincdunt justo eget lob
                                                                                        ortis. Phasellus molestie rutrum
                                                                                        lorem sit. Quisque eget elit
                                                                                        purus. Vivamus dictum rutrum
                                                                                        ipsumi. Phasellus molestie
                                                                                        rutrum lorem sit. Quisque eget
                                                                                        elit purus. Vivamus dictum
                                                                                        rutrum ipsumi.
                                                                                    </p>
                                                                                </div>
                                                                                <ul class="social_icons">
                                                                                    <li><a class="add_cart_btn"
                                                                                           href="#">Add to cart</a></li>
                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-refresh"></i></a>
                                                                                    </li>
                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-heart"></i></a>
                                                                                    </li>
                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-search"></i></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="single_list_product">
                                                                    <div class="col-md-4 no-padding-left">
                                                                        <div class="single_list_image">
                                                                            <div class="single_tab_content">
                                                                                <figure class="effect-lexi">
                                                                                    <a href="#"><img alt=""
                                                                                                     src="dist/img/tab-image-one.png"></a>
                                                                                    <figcaption>
                                                                                        <p class="add_to_cart">Hot</p>
                                                                                    </figcaption>
                                                                                </figure>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-8  no-padding-right">
                                                                        <div class="single_list_details">
                                                                            <div class="product_description">
                                                                                <a href="#">Et harum quidem rerum
                                                                                    dress</a>

                                                                                <div class="star">
                                                                                    <a href="#">
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star-o"></i>
                                                                                        <i class="fa fa-star-o"></i>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="price">
                                                                                    <p>
                                                                                        <span class="old_price">$409.00</span>
                                                                                        <span class="new_price">$369.00</span>
                                                                                    </p>
                                                                                </div>
                                                                                <div class="product_detail">
                                                                                    <p>
                                                                                        Quisque eget elit purus. Vivamus
                                                                                        dictum rutrum ipsum, quist
                                                                                        ornare lorem iaculis nec. Sed
                                                                                        volutpat tincdunt justo eget lob
                                                                                        ortis. Phasellus molestie rutrum
                                                                                        lorem sit. Quisque eget elit
                                                                                        purus. Vivamus dictum rutrum
                                                                                        ipsumi. Phasellus molestie
                                                                                        rutrum lorem sit. Quisque eget
                                                                                        elit purus. Vivamus dictum
                                                                                        rutrum ipsumi.
                                                                                    </p>
                                                                                </div>
                                                                                <ul class="social_icons">
                                                                                    <li><a class="add_cart_btn"
                                                                                           href="#">Add to cart</a></li>
                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-refresh"></i></a>
                                                                                    </li>
                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-heart"></i></a>
                                                                                    </li>
                                                                                    <li><a href="#"><i
                                                                                                    class="fa fa-search"></i></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!--end row view-->
                                                    <!--Start grid view-->
                                                    <div id="gridView" class="row">
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-one.png" alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount">sale</p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-three.png"
                                                                                     alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-four.png"
                                                                                     alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-ten.png" alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-six.png" alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-seven.png"
                                                                                     alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-eight.png"
                                                                                     alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-nine.png"
                                                                                     alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-ten.png" alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-four.png"
                                                                                     alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-one.png" alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-ten.png" alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end grid view-->
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="packages">

                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="offers">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End category area -->