<!-- start category area -->
<div class="category_area">
    <div class="container">
        <div class="row">
            <div class="category">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-12">
                        @include('sections.categorysidebar')
                    </div>
                    <div id="park-info" class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Walt Disney World Resort</h2>
                                <h4>Orlando</h4>

                                <div class="product_item_details no_border_with_50_pad search-pages">
                                    <div role="tabpanel">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#images"
                                                                                      aria-controls="review" role="tab"
                                                                                      data-toggle="tab">images</a></li>
                                            <li role="presentation"><a href="#description" aria-controls="description"
                                                                       role="tab" data-toggle="tab">description</a></li>
                                            <li role="presentation"><a href="#videos" aria-controls="tags" role="tab"
                                                                       data-toggle="tab">videos</a></li>
                                            <li role="presentation"><a href="#map" aria-controls="tags" role="tab"
                                                                       data-toggle="tab">map</a></li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane" id="description">
                                                <p>
                                                    It’s the place where dreams come true. Imagine six fantastic Disney
                                                    Parks, two exciting night-time entertainment districts, and a
                                                    state-of-the-art sports complex. Now add in over 20 themed Resort
                                                    Hotels, plus countless opportunities for dining, shopping and
                                                    recreation, and it’s easy to see why Walt Disney World Resort is the
                                                    number one holiday destination in the world.<br/>
                                                    Whether you’re looking for beloved Disney Characters, thrilling
                                                    attractions, splashing good Water Parks or even a relaxing beach or
                                                    two, Walt Disney World has it all. Meet Mickey. Ride a magic carpet
                                                    through Agrabah. Get right in the middle of your favourite toons
                                                    with outrageous, in-your-face 3D. Plunge down a waterfall. Travel
                                                    through time, space and under the sea. Golf from dawn ‘til dusk on
                                                    pro courses. Water ski on private, pristine lakes. Shop ‘til you
                                                    drop. Be dazzled by spectacular, world-class entertainment. Savour
                                                    delectable dining at award-winning restaurants. The possibilities
                                                    are virtually endless.<br/>
                                                    And with so much to explore, there’s only one ticket that lets you
                                                    play Disney your way: Disney’s Ultimate Ticket. Exclusively designed
                                                    for UK guests visiting Walt Disney World Resort in Florida, this
                                                    ticket means you can come and go as you please to all six Parks and
                                                    more at one amazing price. It’s the only ticket you need.<br/>
                                                    With something for everyone, it’s no wonder Walt Disney World is the
                                                    holiday of your dreams. And come true they do – just like magic.
                                                    <br/>Terms and conditions apply to Disney's Ultimate Ticket.
                                                </p>
                                            </div>
                                            <div role="tabpanel" class="tab-pane active" id="images">
                                                <div id="slider" class="flexslider">
                                                    <ul class="slides">
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney1.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney2.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney3.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney4.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney5.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney6.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney7.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney8.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney9.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney10.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney11.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney12.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney13.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney14.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney15.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney16.jpg"/>
                                                        </li>
                                                        <!-- items mirrored twice, total of 12 -->
                                                    </ul>
                                                </div>
                                                <div id="carousel" class="flexslider">
                                                    <ul class="slides">
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney1.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney2.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney3.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney4.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney5.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney6.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney7.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney8.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney9.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney10.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney11.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney12.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney13.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney14.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney15.jpg"/>
                                                        </li>
                                                        <li>
                                                            <img src="/dist/img/walt-disney/disney16.jpg"/>
                                                        </li>
                                                        <!-- items mirrored twice, total of 12 -->
                                                    </ul>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="videos">
                                                <p>
                                                    Quisque eget elit purus. Vivamus dictum rutrum ipsum, quist ornare
                                                    lorem iaculis nec. Sed volutpat tincdunt justo eget lob ortis.
                                                    Phasellus mos lestie rutrum lorem sit. Quisque eget elit purus.
                                                    Vivamus dictum rutrum ipsumi. Phasellus molestie rutrum lorem sit.
                                                    Quisque eget elit purus be Vivamus dictum rutrum ipsumi. Quisque
                                                    eget elit purus. Vivamus dictum rutrum ipsum, quist ornare lorem
                                                    iaculis nec. Sed volutpat tincdunt jus to eget lob ortis. Phasellus
                                                    molestie rutrum lorem sit. Quisque eget elit purus. Vivamus dictum
                                                    rutrum ipsumi.
                                                </p>

                                                <p>
                                                    Vivamus dictum rutrum ipsum, quist ornare lorem iaculis nec. Sed
                                                    volutpat tincdunt justo eget lob ortis. Phasellus mos lestie rutrum
                                                    lorem sit. Quisque eget elit purus. Vivamus dictum rutrum ipsumi.
                                                    Phasellus molestie rutrum lorem sit. Quisque eget elit purus be
                                                    Vivamus dictum rutru m ipsumi. Quisque eget elit purus. Vivamus
                                                    dictum rutrum ipsum, quist ornare lorem iaculis nec. Quisque eget
                                                    elit purus be Vivamus dictum rutru m ipsumi. Quisque eget elit
                                                    purus. Vivamus dictum rutrum ipsum, quist ornare lorem iaculis nec.
                                                </p>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="map">
                                                <img src="/dist/img/walt-disney/WDW_map.jpg"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="category_products">
                            <div class="row">
                                <div class="list_view">
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <!--  <div class="sorting">
                  <label>
                      <select>
                          <option selected> Default Sorting </option>
                          <option>Grid View</option>
                          <option>List View</option>
                      </select>
                  </label>
              </div>-->
                                    </div>
                                    <div>

                                    </div>
                                </div>
                            </div>
                            <!--Start Row View-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="product_item_details no_border_with_50_pad ticketChooser">
                                        <div role="tabpanel">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#tickets"
                                                                                          aria-controls="description"
                                                                                          role="tab" data-toggle="tab">tickets</a>
                                                </li>
                                                <li>
                                                    <select class="form-control" name="" style="height: 42px;" id="">
                                                        <option value="date_up">Date up</option>
                                                        <option value="date_down">Date down</option>
                                                        <option value="price_up">Price up</option>
                                                        <option value="price_down">Price down</option>
                                                        <option value="title_up">Title up</option>
                                                        <option value="title_down">Title down</option>
                                                    </select>
                                                </li>
{{--                                                <li role="presentation"><a href="#packages" aria-controls="review"
                                                                           role="tab" data-toggle="tab">packages</a>
                                                </li>
                                                <li role="presentation"><a href="#offers" aria-controls="tags"
                                                                           role="tab" data-toggle="tab">offers</a></li>--}}
                                                <div class="sorting_view alignright"> <!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-->
                                                <li><a class="btn" href="{{Url::to('/advancedsearch')}}">Advanced search</a></li>
                                                </div>
{{--                                                <div class="sorting_view floatright">


                                                    <i id="theGrid" class="fa fa-th deactive"></i>
                                                    <i id="theRow" class="fa fa-list"></i>
                                                </div>--}}
                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="tickets">
                                                    @include('sections.listproduct')

                                                    <!--end row view-->
                                                    <!--Start grid view-->
                                                    <div id="gridView" class="row">
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-one.png" alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount">sale</p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-three.png"
                                                                                     alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-four.png"
                                                                                     alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-ten.png" alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-six.png" alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-seven.png"
                                                                                     alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-eight.png"
                                                                                     alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-nine.png"
                                                                                     alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-ten.png" alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-four.png"
                                                                                     alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-one.png" alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="single_tab_content">
                                                                <figure class="effect-lexi">
                                                                    <a href="#"><img src="/dist/img/tab-image-ten.png" alt=""></a>
                                                                    <figcaption>
                                                                        <a href="#" class="add_to_cart">Add to cart</a>

                                                                        <p class="discount"></p>

                                                                        <p class="favourite">
                                                                            <a href="#"><i
                                                                                        class="fa fa-refresh"></i></a>
                                                                            <a href="#"><i class="fa fa-heart"></i></a>
                                                                            <a href="#"><i class="fa fa-search"></i></a>
                                                                        </p>
                                                                    </figcaption>
                                                                </figure>
                                                                <div class="product_description">
                                                                    <a href="#">Et harum quidem rerum dress</a>

                                                                    <div class="star">
                                                                        <a href="#">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                            <i class="fa fa-star-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="price">
                                                                        <p><span class="old_price">$409.00</span> <span
                                                                                    class="new_price">$369.00</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end grid view-->
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="packages">

                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="offers">

                                                </div>

                                                {{--<a class="btn" href="{{Url::to('/advancedsearch')}}">Advanced search</a>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End category area -->