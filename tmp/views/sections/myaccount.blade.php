<div class="container p-l-none p-r-none myAccount">

    <div class="">
        <div class="">
            <div class="panel panel-default">
                <div class="panel-heading">My Account</div>

                <div class="panel-body">

                    <div class="form-group">
                    </div>

                    <div class="container">
                        <div class="">
                            <!-- left column -->
                            <div class="col-md-3">
                                <div class="text-center">
                                    <div class="avatar_account"><div style="background-image: 'URL::to('rest/v1/image/' . $user->avatar)'"></div></div>
                                    {{--<img  style="height:150px; width:150px" src="{{$user->avatar == '' ? '/dist/img/1.png' : URL::to('rest/v1/image/' . $user->avatar)}}" class="avatar_account img-circle" alt="avatar">--}}
                                    <form enctype="multipart/form-data" action="/rest/v1/uploadavatar/?" method="POST">

                                        {{ csrf_field() }}
                                        <div class="clearfix p-t-sm">
                                            <div class="p-sm m-t-xs">
                                                <p class="select_file_btn"><strong>File not selected</strong></p>
                                            </div>
                                            <div class="clearfix">
                                                <input type="hidden" name="ID" value='' type="text"/>
                                                <input type="hidden" name="MAX_FILE_SIZE" value="900000"/>
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                                <label class="file_upload pull-left">
                                                    <span class="button btn btn-default btn-sm">Select file</span>
                                                    <input name="file" type="file">
                                                </label>
                                                <input type="submit" class="btn btn-default btn-sm" value=" Set avatar "/>
                                            </div>
                                            {{--Select image: <input name="file" type="file"/>--}}
                                        </div>

                                        @if ($errors->has('Avatar'))
                                            <div class="col-md-12">
                                                <div class="form-group p-l-md">
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('Avatar') }}</strong>
                                                    </span>
                                                </div>
                                            </div>
                                        @endif
                                    </form>
                                </div>
                            </div>

                            <!-- edit form column -->
                            <div class="col-md-9 personal-info">
                                {{--<div class="alert alert-info alert-dismissable">--}}
                                    {{--<a class="panel-close close" data-dismiss="alert">×</a>--}}
                                    {{--<i class="fa fa-coffee"></i>--}}
                                    {{--This is an <strong>.alert</strong>. Use this to show important messages to the user.--}}
                                {{--</div>--}}
                                <form class="form-horizontal myaccountform" role="form" method="post" action="/myaccount">
                                {{--<input type="hidden" name="_method" value="PATCH">--}}
                                    {{ csrf_field() }}
                                    <h4>Personal info</h4>
                                    <div class="clearfix">
                                        <div class="col-md-6">
                                            <div class="form-group m-n">
                                                <label class="control-label">Username</label>
                                                <div class="p-t-xs">
                                                    <input type="text" placeholder="Username"
                                                           class="form-control" name="name"
                                                           value="{{ Auth::user()->name }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group m-n">
                                                <label class="control-label">Telephone</label>
                                                <div class="p-t-xs">
                                                    <input type="text" placeholder="Telephone"
                                                           class="form-control" name="phone"
                                                           value="{{ Auth::user()->phone }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-b-sm clearfix">
                                        <div class="col-md-6">
                                            <div class="form-group m-n">
                                                <label class="control-label">First Name</label>
                                                <div class="p-t-xs">
                                                    <input type="text" placeholder="First Name"
                                                           class="form-control" name="first_name"
                                                           value="{{ Auth::user()->first_name }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group m-n">
                                                <label class="control-label">Last Name</label>
                                                <div class="p-t-xs">
                                                    <input type="text" placeholder="Last Name"
                                                           class="form-control" name="last_name"
                                                           value="{{ Auth::user()->last_name }}">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group m-n">
                                                <label class="control-label">Sex</label>
                                                <div class="p-t-xs">
                                                    <input type="radio" placeholder="women" name="sex"
                                                           value="women" class="radio-controls"
                                                           @if(Auth::user()->sex == 'women') checked @endif
                                                    > Women
                                                    <input type="radio" placeholder="man" name="sex"
                                                           value="man" class="radio-controls"
                                                           @if(Auth::user()->sex == 'man') checked @endif
                                                    > Man
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group m-n">
                                                <label class="control-label">Date of Birth</label>
                                                <div class="p-t-xs">
                                                    <input type="text" placeholder="Date of Birth"
                                                           class="form-control" name="birthday"
                                                           value="{{ Auth::user()->birthday }}">
                                                </div>
                                            </div>

                                        </div>
                                        {{----}}
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="form-group m-n">--}}
                                                {{--<label class="control-label">Password</label>--}}
                                                {{--<div class="p-t-xs">--}}
                                                    {{--<input type="password" placeholder="Password" ng-model="appCtrl.Password"--}}
                                                           {{--class="form-control" name="Password">--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                    </div>
                                    {{--<h4>Location info</h4>--}}
                                    {{--<div class="location_form_wrap location" id="1">--}}
                                        {{--<div>--}}
                                            {{--<div class="location_form">--}}
                                                {{--<p class="h5">Address <span>1</span>--}}
                                                {{--<div class="clearfix">--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group m-n">--}}
                                                            {{--<label class="control-label">Country</label>--}}
                                                            {{--<div class="p-t-xs">--}}
                                                                {{--<input type="text" placeholder="Country" class="form-control" name="Country">--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group m-n">--}}
                                                            {{--<label class="control-label">State</label>--}}
                                                            {{--<div class="p-t-xs">--}}
                                                                {{--<input type="text" placeholder="State" class="form-control" name="State">--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="clearfix">--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group m-n">--}}
                                                            {{--<label class="control-label">City</label>--}}
                                                            {{--<div class="p-t-xs">--}}
                                                                {{--<input type="text" placeholder="City" class="form-control" name="City">--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group m-n">--}}
                                                            {{--<label class="control-label">County</label>--}}
                                                            {{--<div class="p-t-xs">--}}
                                                                {{--<input type="text" placeholder="County"  class="form-control" name="County">--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="clearfix">--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group m-n">--}}
                                                            {{--<label class="control-label">Address</label>--}}
                                                            {{--<div class="p-t-xs">--}}
                                                                {{--<input type="text" placeholder="Address" class="form-control" name="Address">--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group m-n">--}}
                                                            {{--<label class="control-label">Zip</label>--}}
                                                            {{--<div class="p-t-xs">--}}
                                                                {{--<input type="text" placeholder="Zip" class="form-control" name="Zip">--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="p-t-md" style="padding-left:15px">--}}
                                        {{--<button type="button" class="btn btn-default addAdress"><i class="fa fa-plus" aria-hidden="true"></i> Add address</button>--}}
                                    {{--</div>--}}
                                    <div class="clearfix p-t-sm">
                                        <div class="clearfix">
                                            <div class="col-md-12">
                                                <label class="control-label">Receive Marketing Emails</label>

                                                <div class="p-t-xs">
                                                    <input type="checkbox"
                                                           class="" name="marketing_unsubscribed" value="1"
                                                           @if(Auth::user()->marketing_unsubscribed == 1) checked @endif
                                                    >
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="control-label">Receive System Emails</label>

                                                <div class="p-t-xs">
                                                    <input type="checkbox"
                                                           class="" name="system_unsubscribed" value="1"
                                                           @if(Auth::user()->system_unsubscribed == 1) checked @endif
                                                    >
                                            </div>
                                            <div class="p-t-sm">
                                                </div>
                                                <div class="text-right" style="display: inline-block;">
                                                    <button type="button" class="btn btn-default">
                                                        Change password
                                                    </button>
                                                </div>
                                                <div class="text-right pull-right" style="display: inline-block;">
                                                    <button class="btn btn-default">
                                                        <i class="fa fa-btn fa-refresh apply"></i>Apply Changes
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@section('jscontent')
<script>
    $('.addAdress').click(function(){
        var num = $('.location').last().attr("id");
        var num_new=parseInt(num)+1;
        $('.myaccountform .location').last().after('<div class="location_form location" id='+num_new+'><p class="h5">Address '+num_new+' <span></span><a class="p-l-sm delete_address" id="_'+num_new+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></p><div class="clearfix"><div class="col-md-6"><div class="form-group m-n"> <label class="control-label">Country</label><div class="p-t-xs"><input type="text" placeholder="Country" class="form-control" name="Country"></div></div></div><div class="col-md-6"><div class="form-group m-n"><label class="control-label">State</label><div class="p-t-xs"><input type="text" placeholder="State" class="form-control" name="State"></div></div></div></div> <div class="clearfix"> <div class="col-md-6"><div class="form-group m-n"><label class="control-label">City</label><div class="p-t-xs"><input type="text" placeholder="City" class="form-control" name="City"></div></div></div><div class="col-md-6"><div class="form-group m-n"><label class="control-label">County</label><div class="p-t-xs"><input type="text" placeholder="County" class="form-control" name="County"></div></div></div></div><div class="clearfix"><div class="col-md-6"><div class="form-group m-n"><label class="control-label">Address</label><div class="p-t-xs"><input type="text" placeholder="Address" class="form-control" name="Address"></div></div></div><div class="col-md-6"><div class="form-group m-n"><label class="control-label">Zip</label><div class="p-t-xs"><input type="text" placeholder="Zip" class="form-control" name="Zip"></div></div></div></div></div>')
        $('.delete_address').click(function () {
            var id = $(this).attr("id").replace('_', '');
            $('#' + id).remove();
        })
    })

</script>
@endsection