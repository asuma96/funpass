<div id="rowView">
@if(count($products) == 0)
    <div class="row emptylistborder">
        <h4 class="p-md" align="center">No products found by your request</h4>
    </div>
    @else
    @foreach($products as $product)
    <div class="row">
        <div class="col-md-12">
            <div class="single_list_product">
                <div class="col-md-4 no-padding-left">
                    <div class="single_list_image">
                        <div class="single_tab_content">
                             <div class="product_icon">

                            </div>
                            <figure class="effect-lexi">
                                <a href="#"><img src="/dist/img/3a.jpg" alt=""></a>
                                <div class="product-preview__users-style"></div>

                            </figure>

                        </div>
                    </div>
                </div>



                <div class="col-md-8  no-padding-right">
                    <div class="single_list_details">
                        <div class="product_description">
                            <a></a>

{{--                            <div class="star">
                                <a href="#">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                </a>
                            </div>--}}
                            <div class="price">
                                {{--ID: <span data-ng-bind="product.ID"></span>--}}
                                <p>
                                    Adult:

                                    <span></span><span class="new_price"></span>
                                    <span>
                                        (<span>{{$product -> price1}}</span><span class="new_price"></span>)
                                    </span>

                                    <span><br>Child:
                                        <span></span><span class="new_price"></span>
                                            <span>(<span>{{$product -> price2}}</span><span class="new_price"></span>)
                                        </span>
                                    </span>

                                   <span>
                                       <br>Infant:
                                       <span></span><span class="new_price"></span>
                                       <span>
                                           (<span>{{$product -> price3}}</span><span class="new_price"></span>)
                                       </span>
                                   </span>

                                </p>
                                <p>Min Age: <span>{{$product -> min_age}}</span></p>
                                <p>
                                    Start Sale Date: <span>{{$product -> atsrt_sell_date}}</span><br>
                                    End Sale Date: <span>{{$product -> end_sell_date}}</span><br>

{{--                                    <b ng-show="product.StartSellDate > appCtrl.today()">
                                        Start Sell Date: <span ng-bind="product.StartSellDate.split(' ')[0]"></span><br>
                                    </b>--}}
                                    @if ($product -> EndSellDate < date("m.d.y"))
                                    <b>

                                        <b>Out of stock</b><br>

                                    </b>
                                    @endif
                                    @if($product -> tickets_count = 0)
                                    <b>
                                        <b>Sold out</b><br>
                                    </b>
                                    @endif
                                </p>
                            <!--
                                <div>
                                    <p>
                                        Keywords: <span><a href="@{{window.location.host + '/list/' + keyword.Keyword.replaceAll(' ', '_');}}"></a><b>, </b></span>
                                    </p>
                                </div>-->

                            </div>
                            <div class="product_detail">
                                <p></p>
                            </div>

                            <ul class="social_icons">
                                <li id="in{{$product -> id}}" style="display: none"><a class="add_cart_btn inCart"  href="{{ URL::to('/shopping-cart')}}">In Cart</a></li>
                                <li><a class="add_cart_btn"
                                       href="#">Buy now</a></li>

                                <li id={{$product -> id}}><a class="add_cart_btn addCart"
                                       href="#">Add to cart</a></li>
                                <li><a class="add_cart_btn" href="#">Add to wishlist</a></li>

                                {{--                                <li><a href="#"><i
                                            class="fa fa-refresh"></i></a>
                                </li>
                                <li><a href="#"><i
                                            class="fa fa-heart"></i></a>
                                </li>
                                <li><a href="#"><i
                                            class="fa fa-search"></i></a>
                                </li>--}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    @endif
    <div class="row">
        <div class="col-md-1">
            <button class=" btn single_tab_content">Prev</button>
        </div>
        <div class="col-md-1">
            <button class=" btn single_tab_content">Next</button>
        </div>
    </div>

</div>