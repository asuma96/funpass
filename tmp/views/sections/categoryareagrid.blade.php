<!-- Start category area -->
<div class="category_area">
    <div class="container">
        <div class="row">
            <div class="category">
                <div class="">
                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-12">
                    @include('sections.categorysidebar')
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                        <div class="category_products">
                            <div class="row">
                                <div class="grid_view">
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <div class="sorting">
                                            <label>
                                                {{--<select>--}}
                                                    {{--<option selected> Default Sorting</option>--}}
                                                    {{--<option>Grid View</option>--}}
                                                    {{--<option>List View</option>--}}
                                                {{--</select>--}}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-md-offset-6 col-sm-6 col-xs-6">
                                        <div class="sorting_view floatright">
                                            <a href="grid"><i class="fa fa-th"></i></a>
                                            <a href="list"><i class="fa fa-list deactive"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @include('sections.gridproduct')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End category area -->