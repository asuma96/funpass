<!-- Start Shopping Cart -->
<section class="shopping-cart-area">
    <div class="container">
        <div class="row">
            <div class="table-responsive" style="overflow-x: inherit!important;">
                <table class="table table-bordered" >
                    <tbody>
                    <tr class="table-title">
                        <td class="cart-del">Remove</td>
                        <td class="cart-img">Image</td>
                        <td class="cart-name">Product Name</td>
                        <td class="cart-qty">Quantity</td>
{{--                        <td class="cart-subtotal">Subtotal</td>--}}
                        <td class="cart-price">Grandtotal</td>
                        <td class="cart-price">Arrive Date</td>
                        <td class="cart-price">Shipping method</td>
                    </tr>
                    <tr class="table-product-info table-start">
                    </tr>
                    <tr>
                        <td colspan="8">
                            <a style="margin: 10px;" href="{{URL::to('list')}}"
                               class="left_table_btn btn btn_reverse floatleft">continue shopping</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="shopping-cart margin-top-70">
                <div class="col-lg-4 col-md-4 col-sm-4 estimate">
                    {{--<h6>Estimate shipping and tax</h6>--}}
                    {{--<h5>Enter your destination to get a shipping estimate.</h5>--}}

                    {{--<div class="reg-count">--}}
                        {{--<label>Country<span>&nbsp;*</span></label><br>--}}
                        {{--<input type="text">--}}
                    {{--</div>--}}
                    {{--<div class="reg-state">--}}
                        {{--<label>State/Province</label><br>--}}
                        {{--<input type="text">--}}
                    {{--</div>--}}
                    {{--<div class="reg-zip">--}}
                        {{--<label>Zip/Postal Code</label><br>--}}
                        {{--<input type="text"><br>--}}
                    {{--</div>--}}
                    {{--<a href="#" class="btn btn_reverse">Get a quote</a>--}}
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 cart_discount estimate">
                    {{--<h6>Discount code</h6>--}}
                    {{--<h5>Enter your destination to get a shipping estimate.</h5>--}}
                    {{--<div class="p-t-md">--}}
                        {{--<input type="text" class=""><br>--}}
                    {{--</div>--}}
                    {{--<a href="#" class="btn btn_reverse">apply coupon</a>--}}
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 cart_checkout">
                    <div class="info-checkout">
                        <div>
                            <div class="subtotal-grandtotal">
                                <div class="p-b-md clearfix">
                                    <div class="sub-total">
{{--                                        <span class="check-subtot">Subtotal</span><br>--}}
                                        <span class="check-grandtot">Grand Total</span>

                                    </div>
                                    <div class="grand-total">
                                        <div class="grand-total">
{{--                                            <span data-ng-bind="dollarIcon"></span><span class="Subtotal" ng-bind="(appCtrl.getSubtotal()*1).toFixed(2)"></span>
                                            <span ng-if="currentCurrency != 'USD'">
                                                (<span data-ng-bind="currentIcon"></span><span class="Subtotal" ng-bind="(appCtrl.getSubtotal()*currentIndex).toFixed(2)"></span>)
                                            </span>
                                            <br>--}}
                                            <span></span><span class="check-grandtot GrandTotal"></span>
                                            <span>
                                                (<span></span><span class="check-grandtot GrandTotal"></span>)
                                            </span>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <a href="checkout" class="btn">procced to Checkout</a><br>
                                <span class="description">Checkout with multiple address!</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Shopping Cart -->