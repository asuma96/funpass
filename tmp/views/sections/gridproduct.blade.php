<div class="row_category row">
    <div class="item_category item">
        <div class="well_category well">
            <div class="">
                <div class="single_tab_content">
                    <div class="product_icon">

                    </div>
                    <figure class="effect-lexi">
                        <a>
                            <div class="fling-kb product-preview">
                                <img class="catrgory_img product-preview__img" alt="">
                                {{--<div class="product-preview__users-style" data-ng-bind-html="appCtrl.trustAsHtml(product.Badge.Content)">--}}

                                {{--</div>--}}
                            </div>
                        </a>
                    </figure>
                    <div class="product_description">
                        <b>Title</b>
                        <br>
                        <br>
                        <div class="price">
                            <p>
                                Adult:
                                <span></span><span class="new_price"></span>
                                <span>
                                    (<span></span><span class="new_price"></span>)
                                </span>
                                <span>
                                    <br>Child:
                                    <span></span><span class="new_price"></span>
                                    <span>
                                        (<span></span><span class="new_price"></span>)
                                    </span>
                                </span>
                                <span>
                                    <br>Infant:
                                    <span></span><span class="new_price"></span>
                                    <span>
                                        (<span></span><span class="new_price"></span>)
                                    </span>
                                </span>
                            </p>
                            <p>Min Age: <span></span></p>
                            <br>
                            <p>
                                Start Sale Date: <span></span><br/>
                                End Sale Date: <span></span>
                            <p>
                                <b>Out of stock</b>
                            </p>
                            </p>
                            <div>
                                <p>
                                    Keywords: <span><a
                                                href="@{{window.location.host + '/list/' + keyword.Keyword.replaceAll(' ', '_');}}"></a><b>, </b></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-1">
        <button class=" btn single_tab_content">Prev</button>
    </div>
    <div class="col-md-1">
        <button class=" btn single_tab_content">Next</button>
    </div>
</div>