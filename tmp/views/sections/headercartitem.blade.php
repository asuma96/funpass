<li>
    <div class="add_to_cart">
        <div class="row">
            <div class="col-md-4">
                <img class="headercart-img" src="/dist/img/cart-home-one.png" alt="">
            </div>
            <div class="col-md-8">
                <p class="dress_name"><span>Title</span></p>

                <div>
                    <p><span></span> X
                        <span></span><span></span>
                        <span>
                            (<span></span><span></span>)
                        </span>
                    </p>
                    <p><span></span> X
                        <span></span><span></span>
                        <span>
                            (<span></span><span></span>)
                        </span>
                    </p>
                    <p><span></span> X
                        <span></span><span></span>
                        <span>
                            (<span></span><span></span>)
                        </span>
                    </p>
                </div>
            </div>
        </div>

    </div>
    <i class="fa fa-trash-o header_remove_btn"></i>
</li>