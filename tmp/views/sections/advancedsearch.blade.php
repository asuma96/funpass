<!-- start advanced search -->

<div class="col-md-12">
    <h5 class="control-label">Search text</h5>
    <div class="form-group">
        <div>
            <input type="text" placeholder="Enter text" class="form-control" name="Search"
                   value="">
            {{--{{ old('Search') }}--}}
        </div>
    </div>
</div>

<div class="col-md-12">
    <h5 class="control-label">Search by:</h5>
    <div class="form-group row">
        <div class="col-sm-3">
            <input type="checkbox" class="" name="Title" value="">
            {{--{{ old('Title') }}--}}
            <label class="control-label m-l-sm" align="right">Title</label>
        </div>
        <div class="col-sm-3">
            <input type="checkbox" class="" name="Description"
                   value="">
            {{--{{ old('Description') }}--}}
            <label class="control-label m-l-sm" align="right">Description</label>
        </div>
        <div class="col-sm-3">
            <input type="checkbox" class="" name="Title"
                   value="">
            {{--{{ old('Category') }}--}}
            <label class="control-label m-l-sm" align="right">Category</label>
        </div>
        <div class="col-sm-3">
            <input type="checkbox" class="" name="Keyword"
                   value="">
            {{--{{ old('Keyword') }}--}}
            <label class="control-label m-l-sm" align="right">Keywords</label>
        </div>
    </div>
</div>

<div class="col-md-6">
    <h5 class="control-label">Dates:</h5>
    <div class="row">
        <div class="col-md-6">
            <div class="">
                <label class=" control-label" align="right">Arrive Date</label>
                <div class="">
                    <div class="">
                        <div class='input-group date' id='advancedSearchArive'>
                            <input type='text' placeholder="Enter arrive date" class="form-control" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="">
                <label class="control-label" align="right">Depart Date</label>
                <div class="">
                    <div class="">
                        <div class='input-group date' id='advancedSearchDepart'>
                            <input type='text' placeholder="Enter depart date" class="form-control" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="col-md-12">
        <h5 class="control-label">Price</h5>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" align="right">Min</label>

                    <div class="">
                        <input type="number" min="0" placeholder="Enter min price" class="form-control" name="ArriveDate"
                               value="">
                        {{--{{ old('Price_1') }}--}}
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" align="right">Max</label>

                    <div class="">
                        <input type="number" placeholder="Enter max price" class="form-control" name="DepartDate"
                               value="">
                        {{--{{ old('Price_1_max') }}--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


{{--<div class="col-md-4">--}}
    {{--<h5 class="control-label">Price 2 (child):</h5>--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-6">--}}
            {{--<div class="form-group">--}}
                {{--<label class="control-label" align="right">Min</label>--}}

                {{--<div class="">--}}
                    {{--<input type="text" ng-model="appCtrl.advsearch.Price_2" class="form-control" name="ArriveDate"--}}
                           {{--value="{{ old('Price_2') }}">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-6">--}}
            {{--<div class="form-group">--}}
                {{--<label class="control-label" align="right">Max</label>--}}

                {{--<div class="">--}}
                    {{--<input type="text" ng-model="appCtrl.advsearch.Price_2_max" class="form-control" name="DepartDate"--}}
                           {{--value="{{ old('Price_2_max') }}">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

{{--<div class="col-md-4">--}}
    {{--<h5 class="control-label">Price 3 (infant):</h5>--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-6">--}}
            {{--<div class="form-group">--}}
                {{--<label class="control-label" align="right">Min</label>--}}

                {{--<div class="">--}}
                    {{--<input type="text" ng-model="appCtrl.advsearch.Price_3" class="form-control" name="ArriveDate"--}}
                           {{--value="{{ old('Price_3') }}">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-6">--}}
            {{--<div class="form-group">--}}
                {{--<label class="control-label" align="right">Max</label>--}}

                {{--<div class="">--}}
                    {{--<input type="text" ng-model="appCtrl.advsearch.Price_3_max" class="form-control" name="DepartDate"--}}
                           {{--value="{{ old('Price_3_max') }}">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="col-md-12">
    <div class="form-group">
        <div class="pull-right">
            <button class="btn btn-primary">
                <i class="fa fa-btn fa-sign-in"></i>Search
            </button>
        </div>
    </div>
</div>

<!-- End advanced search -->