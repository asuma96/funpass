<!-- start our blog -->
<section class="our_blog_area">
    <div class="container">
        <div class="row">
            <div class="our_blog">
                <div class="row">
                    <div class="col-md-9">
                        <div class="blog_list">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="single_bloglist_post first_bloglist">
                                        <div class="post_content">
                                            <div class="post_thumbnail">
                                                <a href="#"><img src="dist/img/blog_post_large_img.png" alt=""></a>
                                            </div>
                                            <h4 class="post_title">30 Beautiful and Creative Ecommerce Website</h4>

                                            <div class="post_info">
                                                <span><a href="#">By Admin</a></span>
                                                -
                                                <span><a href="#">Dec 23,2014</a></span>
                                            </div>
                                            <div class="blog_article">
                                                <p>
                                                    Cras condimentum venenatis enim a facilisis. Fusce in mauris velit.
                                                    Ut ipsum lectus, laoreet ac condimentum nec, rhoncus ac augue.
                                                    Maecenas sollicitudin euismod congue. Nunc in vestibulum arcu. Donec
                                                    vitae enim est. Phasellus fermentum odio quam. Donec aliquam lacinia
                                                    nisi, idrhs oncus sapien pharetra ac. Quisque sed pharetra eros, at
                                                    molestie erat
                                                </p>
                                                <a class="btn btn_reverse" href="#">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="single_bloglist_post">
                                        <div class="post_content">
                                            <h4 class="post_title">30 Beautiful and Creative Ecommerce Website</h4>

                                            <div class="post_info">
                                                <span><a href="#">By Admin</a></span>
                                                -
                                                <span><a href="#">Dec 23,2014</a></span>
                                            </div>
                                            <div class="blog_article">
                                                <p>
                                                    Cras condimentum venenatis enim a facilisis. Fusce in mauris velit.
                                                    Ut ipsum lectus, laoreet ac condimentum nec, rhoncus ac augue.
                                                    Maecenas sollicitudin euismod congue. Nunc in vestibulum arcu. Donec
                                                    vitae enim est. Phasellus fermentum odio quam. Donec aliquam lacinia
                                                    nisi, idrhs oncus sapien pharetra ac. Quisque sed pharetra eros, at
                                                    molestie erat
                                                </p>
                                                <a class="btn btn_reverse" href="#">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="single_bloglist_post">
                                        <div class="post_content">
                                            <div class="post_thumbnail">
                                                <div class="embed-responsive embed-responsive-16by9">
                                                    <iframe height="354" src="https://www.youtube.com/embed/ePbKGoIGAXY"
                                                            style="border:none; width: 100%"></iframe>
                                                </div>
                                            </div>
                                            <h4 class="post_title">30 Beautiful and Creative Ecommerce Website</h4>

                                            <div class="post_info">
                                                <span><a href="#">By Admin</a></span>
                                                -
                                                <span><a href="#">Dec 23,2014</a></span>
                                            </div>
                                            <div class="blog_article">
                                                <p>
                                                    Cras condimentum venenatis enim a facilisis. Fusce in mauris velit.
                                                    Ut ipsum lectus, laoreet ac condimentum nec, rhoncus ac augue.
                                                    Maecenas sollicitudin euismod congue. Nunc in vestibulum arcu. Donec
                                                    vitae enim est. Phasellus fermentum odio quam. Donec aliquam lacinia
                                                    nisi, idrhs oncus sapien pharetra ac. Quisque sed pharetra eros, at
                                                    molestie erat
                                                </p>
                                                <a class="btn btn_reverse" href="#">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="single_bloglist_post">
                                        <div class="post_content">
                                            <div class="post_thumbnail">
                                                <a href="#"><img src="dist/img/Bloglist_third_img.png" alt=""></a>
                                            </div>
                                            <h4 class="post_title">30 Beautiful and Creative Ecommerce Website</h4>

                                            <div class="post_info">
                                                <span><a href="#">By Admin</a></span>
                                                -
                                                <span><a href="#">Dec 23,2014</a></span>
                                            </div>
                                            <div class="blog_article">
                                                <p>
                                                    Cras condimentum venenatis enim a facilisis. Fusce in mauris velit.
                                                    Ut ipsum lectus, laoreet ac condimentum nec, rhoncus ac augue.
                                                    Maecenas sollicitudin euismod congue. Nunc in vestibulum arcu. Donec
                                                    vitae enim est. Phasellus fermentum odio quam. Donec aliquam lacinia
                                                    nisi, idrhs oncus sapien pharetra ac. Quisque sed pharetra eros, at
                                                    molestie erat
                                                </p>
                                                <a class="btn btn_reverse" href="#">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pagination_area">
                                        <nav>
                                            <ul class="pagination">
                                                <li>
                                                    <a href="#" aria-label="Previous">
                                                        <i class="fa fa-caret-left"></i>
                                                    </a>
                                                </li>
                                                <li class="active"><a href="#">1</a></li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li>
                                                    <a href="#" aria-label="Next">
                                                        <i class="fa fa-caret-right"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="blog_sidebar fix">
                            <aside class="sidebar_widget">
                                <div class="widget_title">
                                    <h4>Category</h4>
                                </div>
                                <ul>
                                    <li><a href="#">Web Design</a></li>
                                    <li><a href="#">PSD Template</a></li>
                                    <li><a href="#">WordPress</a></li>
                                    <li><a href="#">E-Commerce</a></li>
                                    <li><a href="#">Magento</a></li>
                                    <li><a href="#">Prestashop</a></li>
                                </ul>
                            </aside>
                            <aside class="sidebar_widget">
                                <div class="widget_title">
                                    <h4>Category</h4>
                                </div>
                                <div class="side_post">
                                    <ul>
                                        <li>
                                            <a href="#">sep 29, 2014</a>

                                            <p>Quisque faucibus enim non tempus gravis da Morbi non sem sagittis,
                                                venenatis neqd ue. consequat justo sem <a href="#">[ more ]</a></p>
                                        </li>
                                        <li>
                                            <a href="#">sep 29, 2014</a>

                                            <p>Quisque faucibus enim non tempus gravis da Morbi non sem sagittis,
                                                venenatis neqd ue. consequat justo sem <a href="#">[ more ]</a></p>
                                        </li>
                                        <li>
                                            <a href="#">sep 29, 2014</a>

                                            <p>Quisque faucibus enim non tempus gravis da Morbi non sem sagittis,
                                                venenatis neqd ue. consequat justo sem <a href="#">[ more ]</a></p>
                                        </li>
                                    </ul>

                                </div>
                            </aside>
                            <aside class="sidebar_widget">
                                <div class="widget_title">
                                    <h4>Archive</h4>
                                </div>
                                <ul>
                                    <li><a href="#">September 2014</a></li>
                                    <li><a href="#">August 2014</a></li>
                                    <li><a href="#">July 2014</a></li>
                                    <li><a href="#">October 2013</a></li>
                                    <li><a href="#">January 2013</a></li>
                                    <li><a href="#">December 2012</a></li>
                                </ul>
                            </aside>
                            <aside class="sidebar_widget">
                                <div class="widget_title">
                                    <h4>Tags</h4>
                                </div>
                                <div class="tags_widget">
                                    <a href="#" class="tag">Fashion</a>
                                    <a href="#" class="tag">Women’s</a>
                                    <a href="#" class="tag">Child</a>
                                    <a href="#" class="tag">Lookbook</a>
                                    <a href="#" class="tag">Accessories</a>
                                    <a href="#" class="tag">Dress</a>
                                    <a href="#" class="tag">Sale Off</a>
                                    <a href="#" class="tag">Accessories</a>
                                    <a href="#" class="tag">Clothing</a>
                                    <a href="#" class="tag">Men's</a>
                                    <a href="#" class="tag">Carry bag</a>
                                    <a href="#" class="tag">Clothing</a>
                                </div>
                            </aside>
                            <aside class="sidebar_widget">
                                <div class="widget_title">
                                    <h4>Gallery</h4>
                                </div>
                                <div class="sidebar_image_gallery">
                                    <a href="#"><img src="dist/img/sidebar_image_one.png" alt=""></a>
                                    <a href="#"><img src="dist/img/sidebar_image_two.png" alt=""></a>
                                    <a href="#"><img src="dist/img/sidebar_image_three.png" alt=""></a>
                                    <a href="#"><img src="dist/img/sidebar_image_four.png" alt=""></a>
                                    <a href="#"><img src="dist/img/sidebar_image_five.png" alt=""></a>
                                    <a href="#"><img src="dist/img/sidebar_image_six.png" alt=""></a>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- start our blog -->