@extends('layouts.app')

@section('content')

@include('header')
        <!-- end header -->

<!-- start breadcrumb -->
<section class="breadcrumb_main_area margin-bottom-50">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="breadcrumb_main">
                <h2>Order Ticket Successfully Confirmed!</h2>
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Order</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- End breadcrumb -->
{{--@{{appCtrl.fakeuser}}11--}}
{{--@{{appCtrl.fakepayment}}11--}}
{{--@{{appCtrl.orders}}11--}}


<section class="shopping-cart-area">
    <div class="container">
        <div class="row">
            <div class="table-responsive p-t-sm"  style="overflow-x: inherit!important;">
                <h3 style="font-size: 1.8em;">Guest name: @{{appCtrl.fakeuser.Username}}</h3>
                <table class="table table-bordered m-t-sm m-b-none">
                    <tbody>
                    <tr class="table-title">
                        <td class="cart-img">Image</td>
                        <td class="cart-name">Product Name</td>
                        <td class="cart-qty">Quantity</td>
                        <td class="cart-subtotal">Subtotal</td>
                        <td class="cart-price">Grandtotal</td>
                        <td class="cart-price">Shipping method</td>
                    </tr>
                    <tr class="table-product-info">
                        <td class="prod-img-center">
                            <div class="prod-table-img-c"><img alt="product-name" src="http://placehold.it/96x123">
                            </div>
                        </td>
                        <td class="prod-name">
                            <p><b></b></p>
                            <p>Order Date: <span></span></p>
                            <p>Arrive Date: <span></span></p>
                        </td>
                        <td class="prod-price">
                            <p>Adult:<span></span></p>
                            <p>Child:<span></span></p>
                            <p>Infant:<span></span></p>
                        </td>
                        <td class="prod-qty">
                            <span></span><span></span>
                            <span>
                                <br>(<span></span><span></span>)
                            </span>
                        </td>
                        <td class="prod-tot">
                            <span></span><span></span>
                            <span>
                                <br>(<span></span><span></span>)
                            </span>
                        </td>
                        <td class="prod-tot">
                            <span></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="p-t-md">
                    <div class="col-sm-7"></div>
                    <div class="col-sm-5 p-r-none">
                        <div class="subtotal-grandtotal p-md" style="border: 1px solid #ddd;">
                            <div class="clearfix p-b-sm">
                                <div class="sub-total">
{{--                                    <span class="check-subtot">Subtotal</span><br>--}}
                                    <span class="check-grandtot">Grand Total</span>
{{--                                    <p class="check-grandtot">Status</p>--}}
                                </div>
                                <div class="grand-total">
{{--                                    <span data-ng-bind="currentIcon"></span><span class="Subtotal" ng-bind="(appCtrl.getSubtotalOrders()*currentIndex).toFixed(2)"></span><br>--}}
                                    <span></span><span class="check-grandtot GrandTotal"></span><br>
                                    {{--<p class="check-grandtot GrandTotal">@{{appCtrl.fakepayment == null ? 'Unpaid' : appCtrl.fakepayment.Deleted == 1 ? 'Refunded' : 'Ok'}}</p>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
