    <!-- start supports area -->
    <section class="our_supports_area">
        <div class="container">
            <div class="row">
                <div class="our_supports">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="row">
                            <div class="single_supports">
                                <div class="supports_icon">
                                    <a href="#"><i class="fa fa-money"></i></a>
                                </div>
                                <div class="supports_content">
                                    <h4>Fun Pass USA</h4>
                                    <p>
                                        Florida’s leading supplier of discount leisure activities, offering huge discounts on theme park tickets, restaurant dining and much more.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="row">
                            <div class="single_supports">
                                <div class="supports_icon">
                                    <a href="#"><i class="fa fa-paper-plane"></i></a>
                                </div>
                                <div class="supports_content">
                                    <h4>Price match</h4>
                                    <p>
                                        We are committed to providing you with the guaranteed best price, which is why we offer a price match guarantee. If you find the same legitimate ticket at a cheaper price from an authorized online ticket seller, let us know and we will match that price for you.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="row">
                            <div class="single_supports">
                                <div class="supports_icon">
                                    <a href="#"><i class="fa fa-life-ring"></i></a>
                                </div>
                                <div class="supports_content">
                                    <h4>Ticket center</h4>
                                    <p>
                                        Our ticket center is conveniently located at West Highway 192 in Kissimmee and is open from 9:00 a.m. to 5 p.m. EST Mon – Sat for ticket pick up!
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end supports area -->