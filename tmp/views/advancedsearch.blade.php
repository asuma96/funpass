@extends('layouts.app')

@section('content')
    <!-- start header -->
    @include('header')
    <!-- end header -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Advanced search</div>

                    <div class="panel-body">
                        @include('sections.advancedsearch')
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <h4 class="p-md" align="center">No results found for your request</h4>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Results</div>

                    <div class="panel-body">
                        @include('sections.listproduct')
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
