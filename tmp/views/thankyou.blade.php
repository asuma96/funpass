@extends('layouts.app')

@section('content')

        <!-- start header -->
@include('header')
        <!-- end header -->

<!-- start breadcrumb -->
<section class="breadcrumb_main_area margin-bottom-50">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="breadcrumb_main">
                <h2>Thank you</h2>
                <ul class="breadcrumb">
                    <li><a href="#">Very much!</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- End breadcrumb -->

<!-- start checkout area -->
<section class="checkout_area">
    <div class="container">
        <div class="row">
            <div class="checkout fix">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="checkout_form">
                            <div>
                                <div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="register_part">
                                                    <div class="section_title">
                                                        <h6 class="section_title_l">Congratulations! You have bought it successfully! Thank you</h6>
                                                    </div>
                                                    <br/>

                                                    <div class="row">
                                                        <h6 class="section_title_r">Guest name</h6>
                                                    </div>

                                                    <div class="row">
                                                        <div class="table-responsiv body_list_item_l">
                                                            <table class="table table-bordered">
                                                                <tbody>
                                                                <tr class="table-title">
                                                                    <td class="cart-img">Image</td>
                                                                    <td class="cart-name">Product Name</td>
                                                                    <td class="cart-qty">Quantity</td>
                                                                    <td class="cart-price">Grandtotal</td>
                                                                    <td class="cart-price">Shipping method</td>
                                                                </tr>
                                                                @include('sections.cartitem_thankyou')
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                    </div>
                                                    <div class="clearfix">
                                                        <div class="col-lg-4 col-md-4 col-sm-4 cart_checkout subtotal-grandtotal_l">

                                                            <div>
                                                                <div class="subtotal-grandtotal">
                                                                    <div class="sub-total">
{{--                                                                        <span class="check-subtot">Subtotal</span><br>--}}
                                                                        <span class="check-grandtot">Grand Total</span>

                                                                    </div>
                                                                    <div class="grand-total">
{{--                                                                        $<span ng-bind="(appCtrl.getSubtotalThank() * 1).toFixed(2)"></span>
                                                                        <span ng-if="currentCurrency != 'USD'">
                                                                           (<span data-ng-bind="currentIcon"></span><span ng-bind="(appCtrl.getSubtotalThank() * currentIndex).toFixed(2)"></span>)
                                                                        </span>
                                                                        <br>--}}

                                                                        <span class="check-grandtot"></span>
                                                                        <span>
                                                                            (<span><span class="check-grandtot"></span>)
                                                                        </span>
                                                                        <br>
                                                                        </span>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="subtotal-grandtotal_r">
                                                            {{--<br/>--}}
                                                            <div class="padding_b">
                                                                <h6>Shipping address:</h6>
                                                                <strong class="line-hh"></strong>
                                                            </div>
                                                            <div class="padding_b">
                                                                <h6>Payment information:</h6>
                                                                <p class="line-hh">
                                                                    Card number: <strong></strong><br/>
                                                                    Cardholder name: <strong></strong><br/>
                                                                    Expiration date: <strong></strong>/<strong></strong><br/>
                                                                    CCV: <strong></strong><br/>
                                                                </p>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <br/>
                                                    <a class="btn btn_reverse">Back to site</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End checkout area -->


<!-- End checkout area -->


@endsection
