
<!-- start footer -->
<footer >
    <!-- start footer slider -->
    <div class="footer_slider_area">
        <div class="container">
            <div class="row">
                <div class="footer_slider">
                    <div class="testimonial">
                        <div class="footer_top_slider">
                            <div class="item">
                                <a href="#"><img src="/dist/img/our-logo-one.png" alt=""></a>
                            </div>
                            <div class="item">
                                <a href="#"><img src="/dist/img/our-logo-two.png" alt=""></a>
                            </div>
                            <div class="item">
                                <a href="#"><img src="/dist/img/our-logo-three.png" alt=""></a>
                            </div>
                            <div class="item">
                                <a href="#"><img src="/dist/img/our-logo-four.png" alt=""></a>
                            </div>
                            <div class="item">
                                <a href="#"><img src="/dist/img/our-logo-five.png" alt=""></a>
                            </div>
                            <div class="item">
                                <a href="#"><img src="/dist/img/our-logo-six.png" alt=""></a>
                            </div>
                            <div class="item">
                                <a href="#"><img src="/dist/img/our-logo-seven.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end footer slider -->

    <!-- start footer top area -->
    <div class="footer_top_area">
        <div class="container">
            <div class="row">
                <div class="footer_top">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="footer_top_widget fix">
                                <h4 class="special_border_right">About us</h4>

                                <p class="margin-tb-30">Are you planning a trip to Orlando, but need a quick and easy place to get all your attraction tickets? Then look no further - Fun Pass has it all from the magical Walt Disney World, to the exciting thrills at Busch Gardens, you are guranteed to get the best prices for all your attraction tickets. So stop waiting, and book your trip today!</p>

                                <ul class="social_icons">
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="footer_top_widget fix">
                                <h4 class="special_border_right">Latest tweet</h4>
                                <ul class="latest_tweet">
                                    <li>
                                        <p class="margin-top-30"><i class="fa fa-twitter"></i>@Brittany_Spears Fun Pass is the best ticketing site out there for your Orlando vacation! Go get your tickets now!!
                                            <span>12 hour ago</span>
                                        </p>
                                    </li>
                                    <li>
                                        <p class="margin-top-30"><i class="fa fa-twitter"></i>@ZacEfron I got the cheapest tickets from Fun Pass for all the Disney Parks!
                                            <span>12 hour ago</span>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="footer_top_widget fix">
                                <h4 class="special_border_right">Newsletters</h4>

                                <p class="margin-top-30">Subscribe to Our Newsletter to get Important News.</p>

                                <form action="" class="subscribeEmailForm row" novalidate>
                                    <div class="form-group col-sm-8">
                                        <input class="form-control" placeholder="Enter email" style="width:100%; height: 37px" type="email" name="email">
                                    </div>
                                    <div class="col-sm-4 p-l-none">
                                        <button type="submit" class="btn btn-block" style=" padding: 8.5px 15px;">SUBSCRIBE</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end footer top area -->
    <!-- start footer bottom area -->
    <div class="footer_bottom_area">
        <div class="container">
            <div class="row">
                <div class="footer_bottom">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="footer_widget">
                                <div class="logo">
                                    <a href="#"><img src="{{ URL::asset('dist/img/logo.png')}}" alt=""><span>- fashion -</span></a>
                                </div>
                                <ul class="special_widget">
                                    <li>
                                        <i class="fa fa-phone"></i>
                                        +1 (00) 86 868 868 666 <br>
                                        +1 (00) 42 868 666 888
                                    </li>
                                    <li>
                                        <i class="fa fa-envelope"></i>
                                        admin@bootexperts.com<br>
                                        info@bootexperts.com
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker"></i>
                                        68 Dohava Stress, Lorem isput Spusts <br>
                                        New York- United State
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="footer_widget">
                                <h4>My account</h4>
                                <ul>
                                    <li><a href="{{ URL::to('myaccount')}}"><i class="fa fa-angle-right"></i>My Account</a></li>
                                    <li><a href="{{ URL::to('about-us')}}"><i class="fa fa-angle-right"></i>About Us</a></li>
                                    <li><a href="{{ URL::to('contact')}}"><i class="fa fa-angle-right"></i>Contact</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i>Affiliates</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i>Meet The Maker</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="footer_widget">
                                <h4>Information</h4>
                                <ul>
                                    <li><a href="#"><i class="fa fa-angle-right"></i>Return & Exchanges</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i>Gift Cards</a></li>
                                    <li><a href="{{ URL::to('/shopping')}}"><i class="fa fa-angle-right"></i>Order Status</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i>Free Shipping</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i>Returns & Exchanges</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="footer_widget">
                                <h4>Opening time</h4>
                                <ul>
                                    <li>
                                        <span class="floatleft"><i class="fa fa-angle-right"></i>Monday - Friday</span>
                                        <span class="floatright">8.00 AM - 8.00 PM</span>
                                    </li>
                                    <li>
                                        <span class="floatleft"><i class="fa fa-angle-right"></i>Saturday</span>
                                        <span class="floatright">9.00 AM - 9.00 PM</span>
                                    </li>
                                    <li class="no_border">
                                        <span class="floatleft"><i class="fa fa-angle-right"></i>Sunday</span>
                                        <span class="floatright">10.00 AM - 6.00 PM</span>
                                    </li>
                                </ul>
                                <div class="payment">
                                    <a href="#"><img src="{{ URL::asset('dist/img/visa.png')}}" alt=""></a>
                                    <a href="#"><img src="{{ URL::asset('dist/img/mastercard.png')}}" alt=""></a>
                                    <a href="#"><img src="{{ URL::asset('dist/img/discover.png')}}" alt=""></a>
                                    <a href="#"><img src="{{ URL::asset('dist/img/american-express.png')}}" alt=""></a>
                                    <a href="#"><img src="{{ URL::asset('dist/img/paypal.png')}}" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end footer bottom area -->
    <!-- start footer copyright area -->
    <div class="footer_copyright_area">
        <div class="container">
            <div class="row">
                <div class="footer_copyright">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="copyright">
                                <p>
                                    Copyright © 2016 <a href="#">Hastech</a> - All Rights Reseved.
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="footer_menu">
                                <nav class="footer_nav">
                                    <ul>
                                        <li><a href="{{ URL::to('about-us')}}">About Us</a></li>
                                        <li><a href="{{ URL::to('checkout')}}">Check Out</a></li>
                                        <li><a href="{{ URL::to('myaccount')}}">My Account</a></li>
                                        <li><a href="#">Support</a></li>
                                        <li><a href="{{ URL::to('contact')}}">Contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end footer copyright area -->
</footer>
<!-- end footer.blade.php -->


