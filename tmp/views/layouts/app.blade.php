<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FunPass</title>
    <meta name="description" content="">
    <meta name="_token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ URL::asset('/dist/img/favicon.ico') }}" sizes="16x16">

    <link rel="stylesheet" href="{{ elixir('css/app.css') }}"/>
</head>
<body id="index_two">
<div id="loader-wrapper">
    <div class="logo"><a href="{{ URL::asset('/#') }}"><span>Fun Pass USA</span></a><div class="logo_payment">Please wait, payment</div></div>
    <div id="preloader6">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- end preloader -->
<!-- here was content -->
@yield('content')
<!-- start footer -->
@include('layouts.footer')

{{--<link href="{{ URL::asset('/css/flexslider.min.css') }}" rel="stylesheet"/>--}}
<link href='http://fonts.googleapis.com/css?family=Open+Sans:700,300,600,800,400' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
      type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

<!-- WOW Animation -->
{{--<script src="{{ URL::asset('/js/libs.min.js') }}"></script>
--}}
<script src="{{elixir('js/app.js') }}"></script>
{{--<script src="{{ URL::asset('/js/components/wow.min.js') }}"></script>--}}
<!--Activating WOW Animation only for modern browser-->
<!--[if !IE]><!-->
<!--!!!!!!!!!!!!!!!!!!<script type="text/javascript">new WOW().init();</script>-->
<!--<![endif]-->
<!--Oh Yes, IE 9+
Supports animation, lets activate for IE 9+-->
<!--[if gte IE 9]>
<!--!!!!!!!!!!!!!!!!!!<script type="text/javascript">new WOW().init();</script>-->
<![endif]-->
<!--Opacity & Other IE fix for older browser-->
<!--[if lte IE 8]>
<!--<script type="text/javascript" src="{{ URL::asset('vendor/ie-opacity-polyfill/ie-opacity-polyfill.js') }}"></script>-->
<![endif]-->

<!-- JavaScripts -->

{{--}}<script src="{{ URL::asset('/js/components/vendor.min.js') }}"></script>
<script src="{{ URL::asset('/js/components/fotorama.js') }}"></script>
<script src="{{ URL::asset('/js/components/jquery.themepunch.plugins.min.js') }}"></script>
<script src="{{ URL::asset('/js/components/jquery.themepunch.revolution.min.js') }}"></script>

<script src="{{ URL::asset('/js/components/jquery.flexslider.js') }}"></script>

<script src="{{ URL::asset('/js/components/qrcode.js') }}"></script>

<script src="{{ URL::asset('/js/components/modernizr-custom.js') }}"></script>
<script src="{{ URL::asset('/js/components/owl.carousel.min.js') }}"></script>
<script src="{{ URL::asset('/js/components/rs.home.js') }}"></script>
<script src="{{ URL::asset('/js/components/plugins.js') }}"></script>

<script src="{{ URL::asset('/js/components/stretch_gallery.js') }}"></script>
<script src="{{ URL::asset('/js/components/owl.carousel.min.js') }}"></script>


<script src="{{ URL::asset('/js/components/index.min.js') }}"></script>
<script src="{{ URL::asset('/js/components/jquery.glasscase.js') }}"></script>--}}

@yield('jscontent')

@include('sections.jscarusel')
<script>
/*
 $(document).ready(function() {
     $(".m_nav").click(function () {
         $("#cssmenu").slideToggle("slow", function () {
         });
     });

     if(localStorage.getItem('json_id')){
       var cart = JSON.parse (localStorage.getItem('json_id'));
           for (var j=0;j<cart.length; j++) {
            var button_id = cart[j].id;
            $('#'+button_id).css('display','none')
            $('#in'+button_id).css('display','block')
        }
     }
        $('.addCart').click(function () {
            var id = $(this).parent('li').attr("id");
            $(this).parent('li').css("display", "none")
              var i = localStorage.length

            localStorage.setItem('product_id' + i, id);
            var events = JSON.parse(localStorage.getItem('json_id')) || [];

            events.push({id:id});
            localStorage.setItem('json_id', JSON.stringify(events));
            var perem = JSON.stringify({id:id})
            $('#in'+id).css('display', "block")
            return false
        })

    })
*/
</script>
@include('sections.jsgallery')
</body>
</html>