<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>cruise</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="dist/img/favicon.ico" sizes="16x16">

    <!-- font-family -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:700,300,600,800,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>


    <!-- fontawesome -->
    <link rel="stylesheet" href="css/font-awesome.css"/>
    <link rel="stylesheet" href="css/normalize.css"/>
    <!-- bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.css"/>
    <!-- uikit -->
    <link rel="stylesheet" href="css/uikit.min.css"/>
    <!-- slicknav menu -->
    <link rel="stylesheet" href="css/slicknav.css"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="css/owl.carousel.css"/>
    <!-- rev slider -->
    <link rel="stylesheet" href="css/rev-slider/settings.css"/>
    <!-- animate -->
    <link rel="stylesheet" href="css/animate.css"/>
    <!-- hover effect -->
    <link rel="stylesheet" href="css/hover-effect.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <!-- responsive -->
    <link rel="stylesheet" href="css/responsive.css"/>
    <!-- modernizr from html5 boilerplate -->
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <!-- HomePage Slider-->
    <link rel="stylesheet" type="text/css" href="css/stretch_gallery.css"/>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

</head>
<body id="index_two">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!-- aftab zaman -->
<!--[if lt IE 9]>
<script src="/dist/vendor_local/html5shiv.min.js"></script>
<script src="dist/vendor_local/respond.min.js"></script>
<![endif]-->

<!-- start preloader -->
<div id="loader-wrapper">
    <div class="logo"><a href="#"><span>Fun Pass USA</span></a></div>
    <div id="preloader6">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- end preloader -->

<!-- here was content -->

@yield('content')

        <!-- start footer -->
@include('layouts.footer')
        <!-- end footer -->

<!-- jquery library -->
<script src="js/vendor/jquery-1.11.2.min.js"></script>
<!-- bootstrap -->
<script src="dist/vendor_local/bootstrap.js"></script>
<!-- uikit -->
<script src="dist/vendor_local/uikit.min.js"></script>
<!-- slicknav menu -->
<script src="dist/vendor_local/jquery.slicknav.min.js"></script>
<!-- owl carousel -->
<script src="dist/vendor_local/owl.carousel.min.js"></script>
<!-- rev slider -->
<script src="dist/vendor_local/rev-slider/jquery.themepunch.plugins.min.js"></script>
<script src="dist/vendor_local/rev-slider/jquery.themepunch.revolution.min.js"></script>
<script src="dist/vendor_local/rev-slider/rs.home.js"></script>
<!-- plugins from boilerplate -->
<script src="dist/vendor_local/plugins.js"></script>


<!-- WOW Animation -->
<script src="dist/vendor_local/wow.min.js"></script>

<!--Activating WOW Animation only for modern browser-->
<!--[if !IE]><!-->
<script type="text/javascript">new WOW().init();</script>
<!--<![endif]-->


<!--Oh Yes, IE 9+ Supports animation, lets activate for IE 9+-->
<!--[if gte IE 9]>
<script type="text/javascript">new WOW().init();</script>
<![endif]-->

<!--Opacity & Other IE fix for older browser-->
<!--[if lte IE 8]>
<script type="text/javascript" src="/dist/vendor_local/ie-opacity-polyfill.js"></script>
<![endif]-->


<!-- my js -->
<script src="dist/js/main.js"></script>
{{--<script type="text/javascript" src="js/jquery.js"></script>--}}
<script>
    $(".m_nav").click(function () {
        $("#cssmenu").slideToggle("slow", function () {
        });
    });
</script>

<script type="text/javascript" src="/dist/vendor_local/stretch_gallery.js"></script>

</body>
<script>

    /* Initialize Stretch Gallery jQuery plugin with unique id #stretch_gallery1 */

    $('#stretch_gallery1').stretch_gallery({});

</script>
</html>


