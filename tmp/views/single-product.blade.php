@extends('layouts.app')

@section('content')

@include('header')
        <!-- end header -->

<!-- start breadcrumb -->
<section class="breadcrumb_main_area margin-bottom-50">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="breadcrumb_main">
                <h2></h2>
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Product Name</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- End breadcrumb -->


<!-- start category area -->
@include('sections.categoryareasingleproduct')
        <!-- End category area -->

@endsection
@section('jscontent')
    @include('sections.jsgallery')

    <script>
        $(".m_nav").click(function () {
            $("#cssmenu").slideToggle("slow", function () {
            });
        });
    </script>
@endsection