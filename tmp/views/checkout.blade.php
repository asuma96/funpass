@extends('layouts.app')

@section('content')

        <!-- start header -->
@include('header')
        <!-- end header -->

<!-- start breadcrumb -->
<section class="breadcrumb_main_area margin-bottom-50">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="breadcrumb_main">
                <h2>Check out</h2>
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Checkout</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- End breadcrumb -->

<!-- start checkout area -->
@include('sections.checkoutarea')
        <!-- End checkout area -->


@endsection
