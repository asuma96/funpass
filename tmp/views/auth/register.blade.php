@extends('layouts.app')

@section('content')
        <!-- start header -->
@include('header')
        <!-- end header -->
<div class="container p-none">
    <div class="">
        <div class="">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" id="registr_init" name="registr_init" role="form" method="POST" action="{{ URL::to('/register')}}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name">
                                <i class="form-control-feedback" data-bv-icon-for="Username" style="display: none;"></i>
                                <small class="help-block" data-bv-validator="notEmpty" data-bv-for="Username" data-bv-result="NOT_VALIDATED" style="display: none;">The Username is required and cannot be empty</small><small class="help-block" data-bv-validator="stringLength" data-bv-for="Username" data-bv-result="NOT_VALIDATED" style="display: none;">The Username must be more than 3 and less than 20 characters long</small><small class="help-block" data-bv-validator="different" data-bv-for="Username" data-bv-result="NOT_VALIDATED" style="display: none;">The username and password cannot be the same as each other</small>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
{{--                        <div class="form-group">
                            <label class="col-md-4 control-label"> </label>
                            <div class="col-md-6">
                                {!! app('captcha')->display() !!}
                            </div>
                        </div>--}}
                        <div class="form-group">

                            <div class="col-md-6 col-md-offset-4">
                                <button form="registr_init"  type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-user"></i>Register
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('jscontent')
<script>
    $(document).ready(function() {
        $('#notEmptyForm').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                fullName: {
                    validators: {
                        notEmpty: {
                            message: 'The full name is required'
                        }
                    }
                }
            }
        });
    });
</script>
@endsection