@extends('layouts.app')

@section('content')
        <!-- start header -->
@include('header')
        <!-- end header -->
<div class="container p-none">
    <div class="">
        <div class="">
            <div class="panel panel-default">
                <div class="panel-heading">Enter email and password to continue checkout</div>
                <div class="panel-body">
                    <form class="form-horizontal" id="registr_init" name="registr_init"  role="form" method="POST" action="{{ URL::to('/rest/v1/joincheckout')}}">

                        <input type="hidden" class="form-control" name="Group_ID" value="1">

                        <div class="form-group">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="Email">
                                @if ($errors->has('Email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('Email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="Password">
                                @if ($errors->has('Password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('Password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-6 col-md-offset-4">
                                <button form="registr_init" type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-user"></i>Continue
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
