@extends('layouts.app')

@section('content')
        <!-- start header -->
@include('header')
<!-- end header -->
<div class="container p-l-none p-r-none">
        <div class="">
                <div class="">
                        <div class="panel panel-default">
                                <div class="panel-heading">Login</div>
                                <div class="panel-body">
                                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/loginciirus') }}">
                                                {!! csrf_field() !!}

                                                <div class="form-group{{ $errors->has('MCUser_ID') ? ' has-error' : '' }}">
                                                        <label class="col-md-4 control-label">MC User ID</label>

                                                        <div class="col-md-6">
                                                                <input type="text" class="form-control" name="MCUser_ID">

                                                                @if ($errors->has('MCUser_ID'))
                                                                        <span class="help-block">
                                                                            <strong>Errors</strong>
                                                                        </span>
                                                                @endif
                                                        </div>
                                                </div>

                                                <div class="form-group">
                                                        <label class="col-md-4 control-label">Guest ID</label>

                                                        <div class="col-md-6">
                                                                <input type="text" class="form-control" name="Guest_ID" >

                                                                @if ($errors->has('Guest_ID'))
                                                                        <span class="help-block">
                                                                            <strong></strong>
                                                                        </span>
                                                                @endif
                                                        </div>
                                                </div>

                                                <div class="form-group">
                                                        <div class="col-md-9 col-md-offset-3">
                                                                <button type="submit" class="btn btn-primary">
                                                                        <i class="fa fa-btn fa-sign-in"></i>Login
                                                                </button>
                                                        </div>
                                                </div>
                                        </form>
                                </div>
                        </div>
                </div>
        </div>
</div>

