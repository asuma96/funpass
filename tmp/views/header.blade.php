 <!-- start header -->
<header class="header_area">
    <!-- start header top area -->
    <div class="header_top_area">
        <div class="container">
            <div class="row">
                <div class="header_top">
                    {{--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">--}}
                    <div class="col-lg-5 col-md-5 col-sm-8 col-xs-12">
                        <div class="row">
                            <div class="left_header_top">
                                <ul style="padding-top: 3px;">
                                    @if(Auth::check())
                                        <li><a href="{{ URL::to('/logout')}}">Logout</a></li>
                                    @else
                                        <li><a href="{{ URL::to('/login')}}">Login</a></li>
                                        <li><a href="{{ URL::to('/loginciirus')}}">Login with Ciirus</a></li>
                                    @endif

                                        @if(Auth::check())
                                        <li style="min-width: 110px;"><a style="padding-top:11px" href="{{ URL::to('/myaccount')}}">
                                                {{ Auth::user()->name  }}
                                                <img class="top_photo"  src="dist/img/avatar.png"  alt="">
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul>
                                                <li><a href="{{ URL::to('/myaccount')}}">My Account</a></li>
                                                <li><a href="{{ URL::to('/wishlist')}}">Wishlist</a></li>
                                                <li><a href="{{ URL::to('/checkout')}}">Check Out</a></li>
                                                <li><a href="{{ URL::to('/shopping')}}">Shopping History</a></li>
                                            </ul>
                                        </li>
                                    @else
                                        {{--<li><a href="{{ URL::to('/myaccount')}}">My Account<i class="fa fa-angle-down"></i></a>--}}
                                            {{--<ul>--}}
                                                {{--<li><a href="{{ URL::to('/myaccount')}}">My Account</a></li>--}}
                                                {{--<li><a href="#">Wishlist</a></li>--}}
                                                {{--<li><a href="{{ URL::to('/checkout')}}">Check Out</a></li>--}}
                                                {{--<li><a href="#">Shopping</a></li>--}}
                                            {{--</ul>--}}
                                        {{--</li>--}}
                                    @endif


                                    <li>
                                        <select class="js-example-templating select-lang">
                                            <option value="USD">USD</option>
                                            <option value="EUR">EUR</option>
                                            <option value="RUB">RUB</option>
                                            <option value="KZT">KZT</option>
                                            <option value="CNY">CNY</option>
                                            <option value="PLN">PLN</option>
                                        </select>
                                    </li>
                                    {{--<li><a href="#"><span>$</span> Usd</a></li>--}}
                                </ul>
                            </div>
                        </div>
                    </div>



                        <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 floatright">

                        <div class="row">
                            <div class="right_header_top floatright"  style="width: 100%;">
                                <ul>
                                    @if(!isset($user))
                                        <li class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
                                    @endif
                                    @if(isset($user))
                                        <li class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    @endif
                                        <form  class="search-form p-r-md clearfix">
                                            <div class="form-group has-feedback m-r-sm">
                                                <label for="search" class="sr-only">Search</label>
                                                <input auto-complete ui-items="searchMatches" class="form-control" name="search" id="search" placeholder="search">
                                                <button class="btn btn-xs header_search_enter"><i class="fa fa-search" aria-hidden="true"></i></button>
                                                <button class="btn btn-xs header_search_clear"><i class="fa fa-times" aria-hidden="true"></i></button>

                                            </div>
                                        </form>

                                    </li>
                                    @if(isset($user))
                                    <li class="col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center">

                                        <a class="floatright" href="{{URL::to('wishlist')}}">
                                            my wishlist
                                            <span class="badge header_badge">
                                                <i class="fa fa-heart fa-lg"></i>
                                            </span>
                                        </a>

                                    </li>
                                    @endif
                                            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center">
                                                <a class="" href="{{URL::to('shopping-cart')}}">
                                                    my cart
                                            <span class="badge">
                                                (<span class="badge"></span>)
                                            </span>
                                                </a>
                                                <ul class="cart_list">
                                                    <li class="elem_list">Recently Added Item(s)</li>
                                                    @include('sections.headercartitem')
                                                    <li>
                                                            <span class="subtotal">
                                                                Subtotal:
                                                            </span>

                                                        <span></span><span class="Subtotal ammount"></span>

                                                        <span>
                                                            <br>(<span></span><span class="Subtotal ammount"></span>)
                                                        </span>
                                                        <br>

                                                    </li>
                                                    <li>
                                                        <a style="width: 49%" class="btn btn-primary" href="{{URL::to('shopping-cart')}}">View Cart</a>
                                                        <a style="width: 49%" class="btn" href="{{ URL::to('/checkout')}}">Check Out</a>
                                                    </li>
                                                </ul>
                                            </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @section('jscontent')
        <script>
          /*  $(document).ready(function(){

                var ids = new Array;
                if(localStorage.getItem('json_id')){
                var cart2 = JSON.parse(localStorage.getItem('json_id'));
              for (var i = 0; i<cart2.length; i++) {
                   ids.push(parseInt(cart2[i].id))
               }
                }
            console.log(ids);
            if (ids.length > 0) {
                $.ajax({
                        type: 'post',
                        url: '/get-products-in',
                        data: {ids: ids},
                        headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
                        success: function (data) {
                            var count = data.length;
                            $('.badge').html(count);
                            for (var j=0; j<data.length-1; j++) {
                                $('.elem_list').after('' +
                                    '<li>'+
                                    '<div class="add_to_cart">'+
                                   '<div class="row">'+
                                   ' <div class="col-md-4">'+
                                '<img class="headercart-img" src="/dist/img/cart-home-one.png" alt="">'+
                                ' </div>'+
                                '<div class="col-md-8">'+
                                '<p class="dress_name"><span>'+data[j].title+'</span></p>'+

                                '<div>'+
                                    '<p><span></span> '+
                                    '<span></span><span></span>'+
                                    '<span>'+
                                    '(<span></span><span></span>)'+
                            '</span>'+
                            '    </p>'+
                            '    <p><span></span>'+data[j].price1+'' +
                                    '    <span></span><span></span>'+
                            '    <span>'+
                            '    (<span></span><span></span>)'+
                            '</span>'+
                            '    </p>'+
                            '    <p><span></span> X'+
                            '    <span></span><span></span>'+
                            '    <span>'+
                            '    (<span></span><span></span>)'+
                            '</span>'+
                            '    </p>'+
                            '    </div>'+
                            '    </div>'+
                            '    </div>'+
                            '</div>'+
                            '<i class="fa fa-trash-o header_remove_btn"></i>'+
                                   ' </li>')



                            }

                            if ((window.location.pathname) == "/shopping-cart" ) {
                                          if (data.length == 0){
                                    ('.table-product-info').after(''+
                                '<tr>'+
                                    '<td class="text-center" colspan="7"><h4 class="p-md">Your cart are empty for now!</h4></td>'+
                                    '</tr>')
                                } else{


                                for (var j = 0; j < data.length; j++) {

                                    $('.table-start').after('' +
                                        '<tr class="table-product-info" id="table_'+data[j].id+'">' +
                                        ' <td class="prod-del"><a class="trash-table" id ="'+data[j].id+'" href="#">' +
                                        ' <i class="fa fa-trash"></i></a></td>' +
                                        '   <td class="prod-img-center">' +
                                        '   <div class="prod-table-img-c">' +
                                        '<img alt="product-name" src="http://placehold.it/96x123">' +
                                        '   </div>' +
                                        '   </td>' +
                                        '   <td class="prod-name"><span>'+data[j].title+'</span></td>' +
                                        '   <td class="prod-price">' +
                                        '   <span>Min age:<span>'+data[j].min_age+'</span></span>' +
                                        '<span>Adult<br><input type="number" min="1"  max="1000" required><br></span>' +
                                        '<span>Child<br><input type="number" min="0"  max="1000" required><br></span>' +
                                        '<span>Infant<br><input type="number" min="0"  max="1000" required></span>' +
                                        '</td>' +
                                        '<td class="prod-tot">' +
                                        '   <span></span>' +
                                        '   <span>' +
                                        '   <br>(<span></span>)' +
                                        '</span>' +
                                        '</td>' +
                                        ' <td class="prod-tot">' +
                                        '     <p style="color:red">' +
                                        '     Select correct date!' +
                                        ' </p>' +
                                        ' <datepicker  date-year-title="selected title">' +
                                        '     <input placeholder="Enter date" class="form-control"/>' +
                                        '     </datepicker>' +
                                        '     </td>' +
                                        '     </td>' +
                                        '     <td class="prod-tot">' +
                                        '     <div class="btn-group" data-toggle="buttons">' +
                                        '     <label id="lb1" class="btn_shippingmethod btn btn-primary"> Electronic Ticket' +
                                        ' </label>' +
                                        ' <label id="lb2" class="btn_shippingmethod btn btn-primary"> Ship to Home' +
                                        ' </label>' +
                                        ' <label id="lb3" class="btn_shippingmethod btn btn-primary"> Pick Up at Ticket Center' +
                                        ' </label>' +
                                        ' </div>' +
                                        ' </td>' +
                                        ' </tr>'
                                    )
                                    $('.trash-table').click(function(){
                                       var id_del =($(this).attr('id'));
                                       var del = -1;
                                        var cart2 = JSON.parse(localStorage.getItem('json_id'));
                                        for (var j=0; j<cart2.length; j++) {
                                            if (cart2[j].id == id_del) {
                                                del = j
                                            }

                                        }
                                        if (del != -1) {
                                           cart2.splice(del, 1);
                                            console.log(cart2)
                                            localStorage.setItem('json_id', JSON.stringify(cart2));
                                       }
                                      $('#table_'+id_del).remove();
                                    })
                                }
                            }
                            }




                        }

                    }
                )

            }

            })*/
        </script>
@endsection
    <!-- end header top area -->
    <!-- start header bottom area -->
    <div class="header_bottom_area">
        <div class="container">
            <div class="row">

                <!-- LOGO -->
                <div class="logo">
                    <a href="/"><img src="{{ URL::asset('dist/img/logo.png')}}" alt=""></a>
                </div>

                <!-- start menga menu -->
                <div class="nav_wrap">
                    <nav class="no320">
                        <ul>
                            <!--  <li class="dropdown_menu">
                                  <a href="index.html">Home</a>
                                  <div class="d_menu">
                                      <span>
                                          <a href="index-two.html">Home - Alternative 1</a>
                                          <a href="index-three.html">Home - Alternative 2</a>
                                      </span>
                                  </div>
                              </li>-->
                            <!--<li class="dropdown_mmenu">
                                <a href="#">shop</a>
                                <div class="megamenu">
                                    <div class="megamenu1">
                                        <span>
                                            <em>category</em>
                                            <a href="#">My Account</a>
                                            <a href="about-us.html">About Us</a>
                                            <a href="contact.html">Contact</a>
                                            <a href="#">Affiliates</a>
                                            <a href="#">Meet The Maker</a>
                                        </span>

                                        <span>
                                            <em>category</em>
                                            <a href="#">My Account</a>
                                            <a href="about-us.html">About Us</a>
                                            <a href="contact.html">Contact</a>
                                            <a href="#">Affiliates</a>
                                            <a href="#">Meet The Maker</a>
                                        </span>

                                        <span>
                                            <em>category</em>
                                            <a href="#">My Account</a>
                                            <a href="about-us.html">About Us</a>
                                            <a href="contact.html">Contact</a>
                                            <a href="#">Affiliates</a>
                                            <a href="#">Meet The Maker</a>
                                        </span>

                                        <span>
                                            <em>category</em>
                                            <a href="#">My Account</a>
                                            <a href="about-us.html">About Us</a>
                                            <a href="contact.html">Contact</a>
                                            <a href="#">Affiliates</a>
                                            <a href="#">Meet The Maker</a>
                                        </span>

                                        <span>
                                            <a href="#">
                                                <img src="img/mega-item.png" class="img-responsive" alt=""/>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </li>-->
                            <li class="dropdown_mmenu">
                                <a href="{{ URL::to('list')}}">Categories</a>
                                <div class="megamenu">

                                        <div class="megamenu1">
                                          {{--  @foreach(cache('menus') as $item)
                                            --}}<span>
                                                <a href="/product-category/{{--$item->id --}}">
                                                    <img src="{{-- $item->cover --}}" class="img-responsive" alt="" />
                                                    <p align="center">{{-- $item->title --}}</p>
                                                </a>
                                            </span>
                                            {{--@endforeach--}}
                                        </div>

                                </div>
                            </li>
                            <li>
                                <a href="{{ URL::to('ticket-bundles')}}">Ticket Bundles</a>
                            </li>
{{--                            <li>
                                <a href="{{ URL::to('list')}}">Attractions Guide</a>
                            </li>--}}
                            {{--<li>--}}
                                {{--<a href="{{ URL::to('blog')}}">Blog</a>--}}
                            {{--</li>--}}
                            <li>
                                <a href="{{ URL::to('bestsellers')}}">Best Sellers</a>
                            </li>
                            <li>
                                <a href="{{ URL::to('specialoffers')}}">Special Offers</a>
                            </li>
                            <li class="dropdown_menu">
                                <a href="javascript:void(0)">Help</a>
                                <div class="d_menu">
                                                <span>
                                                    <a  href="{{ URL::to('faq')}}">FAQ's</a>
                                                </span>
                                                <span>
                                                    <a href="{{ URL::to('contact')}}">Contact Us</a>
                                                </span>
                                </div>
                            </li>
                        </ul>
                    </nav>

                    <!-- MOBILE ONLY CONTENT -->
                    <div class="only-for-mobile">
                        <ul class="ofm">
                            <li class="m_nav"><i class="fa fa-bars"></i> Navigation</li>
                        </ul>

                        <!-- MOBILE MENU -->
                        <div class="mobi-menu">
                            <div id='cssmenu'>
                                <ul>
                                    <li class='has-sub'>
                                        <a href='{{ URL::to('/')}}'><span>Home</span></a>
                                        <ul class="sub-nav">
                                            <li><a href="index-two.html"><span>Home - Alternative 1</span></a></li>
                                            <li><a href="index-three.html"><span>Home - Alternative 2</span></a></li>
                                        </ul>
                                    </li>

                                    <li class='has-sub'>
                                        <a href='#'><span>shop</span></a>
                                        <ul>
                                            <li class='has-sub'>
                                                <a href='#'><span>category</span></a>
                                                <ul>
                                                    <li><a href="{{ URL::to('myaccount')}}"><span>My Account</span></a></li>
                                                    <li><a href="{{ URL::to('about-us')}}"><span>About US</span></a></li>
                                                    <li><a href="{{ URL::to('contact')}}"><span>Contact</span></a></li>
                                                    <li><a href="#"><span>Affiliates</span></a></li>
                                                    <li class="last"><a href="./#"><span>Meet Marker</span></a></li>
                                                </ul>
                                            </li>
                                            <li class='has-sub'>
                                                <a href='#'><span>Portfolio</span></a>
                                                <ul>
                                                    <li><a href="#"><span>Portfolio - 4col</span></a></li>
                                                    <li><a href="#"><span>Portfolio - 3col</span></a></li>
                                                    <li><a href="#"><span>Portfolio - 2col</span></a></li>
                                                    <li><a href="#"><span>Portfolio - Single</span></a></li>
                                                    <li class='last'><a href="#"><span>404 page</span></a></li>
                                                </ul>
                                            </li>
                                            <li class='has-sub'>
                                                <a href='#'><span>Shop</span></a>
                                                <ul>
                                                    <li><a href="#"><span>Shop - List style</span></a></li>
                                                    <li><a href="#"><span>Shop - Grid style</span></a></li>
                                                    <li><a href="#"><span>Shop - Single</span></a></li>
                                                    <li><a href="#"><span>Shop - Cartpage</span></a></li>
                                                    <li class='last'><a href="{{ URL::to('checkout')}}"><span>Shop - Checkout</span></a></li>
                                                </ul>
                                            </li>
                                            <li class='has-sub'>
                                                <a href='#'><span>Extras</span></a>
                                                <ul>
                                                    <li><a href="#"><span>Compare</span></a></li>
                                                    <li><a href="{{ URL::to('about-us')}}"><span>About us</span></a></li>
                                                    <li><a href="{{ URL::to('blog')}}"><span>Blog overview</span></a></li>
                                                    <li><a href="{{ URL::to('blog')}}"><span>Blog Single</span></a></li>
                                                    <li class='last'><a href="{{ URL::to('contact')}}"><span>Contact</span></a></li>
                                                </ul>
                                            </li>
                                            <li class="img-nav">
                                                <div class="container">
                                                    <div class="clearfix"></div>
                                                    <div class="space20"></div>
                                                    <div class="clearfix"></div>
                                                    <div class="row in1">
                                                        <div class="col-md-6">
                                                            <a href="#"><img src="http://placehold.it/190x210" class="img-responsive" alt=""/></a>
                                                        </div>
                                                        <div class="col-md-6">
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="space20"></div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="space20"></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class='has-sub'>
                                        <a href='#'><span>Product</span></a>
                                        <ul class="sub-nav">
                                            <li><a href="gridl"><span>Product grid</span></a></li>
                                            <li><a href="listl"><span>Product List</span></a></li>
                                            <li><a href="{{ URL::to('single-product')}}"><span>Single Product</span></a></li>
                                            <li><a href="{{ URL::to('checkout')}}"><span>Checkout</span></a></li>
                                        </ul>
                                    </li>
                                    <li class='has-sub'>
                                        <a href='#'><span>Blog</span></a>
                                        <ul class="sub-nav">
                                            <li><a href="bloglist.html"><span>Bloglist</span></a></li>
                                            <li><a href="{{ URL::to('our-blog')}}"><span>Our Blog</span></a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href='#'><span>Portfolio</span></a>
                                    </li>
                                    <li>
                                        <a href='#'><span>Shortcodes</span></a>
                                    </li>
                                    <li>
                                        <a href='#'><span>More</span></a>
                                    </li>
                                    <li class='has-sub'>
                                        <a href='#'><span>Buy Theme</span></a>
                                        <ul class="sub-nav">
                                            <li><a href="{{ URL::to('about-us')}}"><span>About Us</span></a></li>
                                            <li><a href="{{ URL::to('contact')}}"><span>Contact</span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- MOBILE ONLY CONTENT -->

                </div>
                <!-- end mega menu -->

            </div>

        </div>
    </div>
    <!-- end header bottom area -->
</header>
<!-- end header -->
<script>
    window.userID = "{{ isset($user) ? $user->ID  : null }}";
    window.Token = "{{ isset($user) ? $user->remember_token  : null }}";
    window.search = "{{ isset($param) ? $param  : '' }}";
</script>