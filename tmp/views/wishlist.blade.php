@extends('layouts.app')

@section('content')

@include('header')
        <!-- end header -->

<!-- start breadcrumb -->
<section class="breadcrumb_main_area margin-bottom-50">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="breadcrumb_main">
                <h2>Wishlist</h2>
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Wishlist</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- End breadcrumb -->


<!-- Start Shopping History -->
@include('sections.wishlistarea')
        <!-- End Shopping History -->

@endsection
