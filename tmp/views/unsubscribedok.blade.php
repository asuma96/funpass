@extends('layouts.app')

@section('content')

@include('header')

<h4 class="p-md" align="center">You have successfully unsubscribed!</h4>

@endsection

@section('jscontent')
    @include('sections.jsgallery')
@endsection