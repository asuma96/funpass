@extends('layouts.app')

@section('content')

@include('header')
        <!-- end header -->

<!-- start breadcrumb -->
<section class="breadcrumb_main_area margin-bottom-50">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="breadcrumb_main">
                <h2>Best Sellers</h2>
                <ul class="breadcrumb">
                    <li><a href="index">Home</a></li>
                    <li><a href="#">Best Sellers</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- End breadcrumb -->

<!-- start category area -->
@include('sections.categoryarealist')
        <!-- End category area -->

@endsection

@section('jscontent')
    @include('sections.jsgallery')
@endsection