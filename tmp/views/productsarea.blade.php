<!-- start product area -->
<section class="products_area">
    <div class="container">
        <div class="row">
            <div class="products">
                <div class="row">
                    <!-- start third product row -->
                    <div class="third_product_row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="single_product">
                                <div class="grid">
                                    <figure class="effect-winston">
                                        <img src="img/disney3.png" alt="img01"/>
                                        <figcaption>
                                            <h2>Disney <span>Tickets</span></h2>
                                            <p>Hot sale</p>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="half_row margin-bottom-40">
                                        <div class="single_product">
                                            <div class="grid">
                                                <figure class="effect-julia">
                                                    <img src="img/product-six.png" alt="img01"/>
                                                    <figcaption>
                                                        <h2>Universal <span>Studios</span></h2>
                                                        <p>Discounted Tickets</p>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="half_row half_last_row margin-bottom-40">
                                        <div class="single_product">
                                            <div class="grid">
                                                <figure class="effect-julia">
                                                    <img src="img/discove.png" alt="img01"/>
                                                    <figcaption>
                                                        <h3>Discovery Cove</h3>
                                                        <p>20% off!</p>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="single_product">
                                <div class="grid">
                                    <figure class="effect-winston">
                                        <img src="img/product-eight.png" alt="img01"/>
                                        <figcaption>
                                            <h2>Make  <span>a Splash</span></h2>
                                            <p>Cheap Tickets</p>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end third product row -->
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <!-- start new featured sales tab -->
                        <div class="new_featured_sales">
                            <div role="tabpanel">

                                <!-- Nav tabs -->
                                <div class="latest_post">
                                    <div class="section_title">
                                        <h4>Package Deals</h4>
                                    </div></div>

                                <div class="row">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active sales_tab" id="new_tab">
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="single_tab_content">
                                                    <figure class="effect-lexi">
                                                        <a href="#"><img src="img/special1.jpg" alt=""></a>
                                                        <figcaption>
                                                            <a href="#" class="add_to_cart">View Details</a>
                                                            <p class="discount"></p>
                                                        </figcaption>
                                                    </figure>
                                                    <div class="product_description">
                                                        <h4>Magical Package Deal!</h4>
                                                        <a href="#">Et harum quidem rerum dress</a>
                                                        <div class="price">
                                                            <p><span class="old_price">$409.00</span> <span class="new_price">$369.00</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="single_tab_content">
                                                    <figure class="effect-lexi">
                                                        <a href="#"><img src="img/special2.jpg" alt=""></a>
                                                        <figcaption>
                                                            <a href="#" class="add_to_cart">View Details</a>
                                                            <p class="discount"></p>
                                                        </figcaption>
                                                    </figure>
                                                    <div class="product_description">
                                                        <h4>Magical Package Deal!</h4>
                                                        <a href="#">Et harum quidem rerum dress</a>
                                                        <div class="price">
                                                            <p><span class="old_price">$409.00</span> <span class="new_price">$369.00</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="single_tab_content">
                                                    <figure class="effect-lexi">
                                                        <a href="#"><img src="img/special3.jpg" alt=""></a>
                                                        <figcaption>
                                                            <a href="#" class="add_to_cart">View Details</a>
                                                            <p class="discount"></p>
                                                        </figcaption>
                                                    </figure>
                                                    <div class="product_description">
                                                        <h4>Magical Package Deal!</h4>
                                                        <a href="#">Et harum quidem rerum dress</a>
                                                        <div class="price">
                                                            <p><span class="old_price">$409.00</span> <span class="new_price">$369.00</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="single_tab_content">
                                                    <figure class="effect-lexi">
                                                        <a href="#"><img src="img/special4.jpg" alt=""></a>
                                                        <figcaption>
                                                            <a href="#" class="add_to_cart">View Details</a>
                                                            <p class="discount"></p>
                                                        </figcaption>
                                                    </figure>
                                                    <div class="product_description">
                                                        <h4>Magical Package Deal!</h4>
                                                        <a href="#">Et harum quidem rerum dress</a>
                                                        <div class="price">
                                                            <p><span class="old_price">$409.00</span> <span class="new_price">$369.00</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end new featured sales tab -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end product area -->