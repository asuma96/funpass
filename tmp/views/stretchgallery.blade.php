    <!-- Stretch Gallery containter -->
    <div id="stretch_gallery1">
        <div class="sg">

            <!-- Slide begining -->
            <div class="sg_slide" style="background: url('dist/img/1a.jpg')">
                <a href="#">
                    <div class="sg_dummy">

                                <span class="sg_text_block">
                                    <div class="sg_caption">Caption</div>
                                    <div class="sg_description">Here is short description of this photo</div>
                                </span>

                    </div>
                </a>
            </div>
            <!-- End of slide -->
            <!-- Slide begining -->
            <div class="sg_slide" style=" background: url('dist/img/2a.jpg')">
                <a href="#">
                    <div class="sg_dummy">

                            <span class="sg_text_block">
                                <div class="sg_caption">Caption</div>
                                <div class="sg_description">Here is short description of this photo</div>
                            </span>

                    </div>
                </a>
            </div>
            <!-- End of slide -->
            <!-- Slide begining -->
            <div class="sg_slide" style=" background: url('dist/img/3a.jpg')">
                <a href="#">
                    <div class="sg_dummy">

                        <span class="sg_text_block">
                            <div class="sg_caption">Caption</div>
                            <div class="sg_description">Here is short description of this photo</div>
                        </span>

                    </div>
                </a>
            </div>
            <!-- End of slide -->
            <!-- Slide begining -->
            <div class="sg_slide" style=" background: url('dist/img/4a.jpg')">
                <a href="#">
                    <div class="sg_dummy">

                    <span class="sg_text_block">
                        <div class="sg_caption">Caption</div>
                        <div class="sg_description">Here is short description of this photo</div>
                    </span>

                    </div>
                </a>
            </div>
            <!-- End of slide -->
            <!-- Slide begining -->
            <div class="sg_slide" style=" background: url('dist/img/5a.jpg')">
                <a href="#">
                    <div class="sg_dummy">

                    <span class="sg_text_block">
                        <div class="sg_caption">Caption</div>
                        <div class="sg_description">description</div>
                    </span>

                    </div>
                </a>
            </div>
            <!-- End of slide -->
        </div>
    </div>
    <!-- End of Stretch Gallery containter -->