@extends('layouts.app')

@section('content')

@include('header')
        <!-- end header -->

<!-- start breadcrumb -->
<section class="breadcrumb_main_area margin-bottom-50">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="breadcrumb_main">
                <h2>Shopping Cart</h2>
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Shopping Cart</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- End breadcrumb -->


<!-- Start Shopping Cart -->
@include('sections.shoppingcartarea')
        <!-- End Shopping Cart -->

@endsection
