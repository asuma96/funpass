@extends('layouts.app')

@section('content')

@include('header')
        <!-- end header -->

<!-- start breadcrumb -->
<section class="breadcrumb_main_area margin-bottom-50">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="breadcrumb_main">
                <h2>Our blog</h2>
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">blog</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- End breadcrumb -->

<!-- start our blog -->
@include('sections.ourblog')
        <!-- start our blog -->

@endsection
