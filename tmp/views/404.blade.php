@extends('layouts.app')

@section('content')

@include('header')
        <!-- end header -->

<!-- start breadcrumb -->
<section class="breadcrumb_main_area margin-bottom-50">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="breadcrumb_main">
                <h2>Page not found</h2>
                <ul class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li><a href="#">{{$page}}</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- End breadcrumb -->
<h1 class="p-sm" align="center">404</h1>
<h4 class="p-sm" align="center">We not found requested page</h4>
<h6 class="p-lg" align="center">Try to find it<br/>manually<br/></h6>




@endsection

@section('jscontent')
    @include('sections.jsgallery')
@endsection