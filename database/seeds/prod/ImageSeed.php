<?php

use App\Image;
use Illuminate\Database\Seeder;

class ImageSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $old_image = DB::select('select * from Image');
        foreach ($old_image as $image) {
            Image::create([
                'file_name' => $image->File_Name,
            ]);
        }
        Schema::drop('Image');
    }
}
