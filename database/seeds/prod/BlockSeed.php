<?php

use Illuminate\Database\Seeder;

class BlockSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i<=10; $i++) {
            \App\Block::create([
                'code' => $faker->unique()->countryCode,
                'title' => $faker->title,
                'body' => $faker->text(500),
            ]);
        }
    }
}
