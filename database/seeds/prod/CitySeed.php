<?php

use Illuminate\Database\Seeder;

class CitySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $states = \App\State::query()->get()->all();
        foreach ($states as $state) {
            $state->cities()->create([
                'title' => $faker->unique(200)->city
            ]);
        }
    }
}
