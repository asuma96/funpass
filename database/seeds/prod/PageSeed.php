<?php

use Illuminate\Database\Seeder;

class PageSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i<=10; $i++) {
            \App\Page::create([
                'meta_title' => $faker->title,
                'meta_keyword' => $faker->title,
                'title' => $faker->title,
                'body' => $faker->text(200),
                'url' => str_replace(' ', '-', $faker->unique()->paragraphs(1, true)),
            ]);
        }
    }
}
