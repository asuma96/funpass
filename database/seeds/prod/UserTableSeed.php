<?php

use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UserTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $old_users = DB::select('select * from User');
        foreach ($old_users as $user) {
            User::create([
                'name' => $user->Username,
                'email' => $user->email,
                'password' => $user->password,
                'remember_token' => $user->remember_token,
                'api_token' => str_random(60),
                'active' => $user->Active_User,
                'role' => $user->Group_ID == 1 ? 'admin' : 'user',
                'marketing_subscribed' => is_null($user->Marketing_Emails_Unsubscribed) ? 0 : $user->Marketing_Emails_Unsubscribed,
                'system_subscribed' => is_null($user->System_Emails_Unsubscribed) ? 0 : $user->System_Emails_Unsubscribed,
                'avatar' => $user->avatar,
                'phone' => $user->Telephone,
                'first_name' => $user->FirstName,
                'last_name' => $user->LastName,
                'mc_user_id' => $user->MCUser_ID,
                'guest_user_id' => $user->GuestUser_ID,
                'ciirus_token' => $user->Ciirus_Token,
//                'profile_id' => $user->Profile_ID,
//                'payment_profile_id' => $user->Payment_Profile_ID,
            ]);
        }
        Schema::drop('User');
    }
}
