<?php

use App\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class CategoriesTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories_old = DB::select('SELECT * FROM Product_Category_Table');
        foreach ($categories_old as $cat) {
            Category::create([
                'title' => $cat->Title,
                'description' => $cat->Description,
                'cover' => $cat->Cover,
                'show_in_menu' => $cat->Show_In_Menu,
            ]);
        }
        Schema::drop('Product_Category_Table');
    }
}
