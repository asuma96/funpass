<?php

use Illuminate\Database\Seeder;

class StateSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $countries = \App\Country::query()->get()->all();
        foreach ($countries as $country) {
            $country->states()->create([
                'title' => $faker->randomElements(['Alabama', 'Alaska', 'California', 'Kansas', 'Massachusetts'])[0]
            ]);
        }
    }
}
