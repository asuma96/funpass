<?php

use Illuminate\Database\Seeder;

class KeywordSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i<=10; $i++) {
            \App\Keyword::create([
                'title' => $faker->unique()->name,
                'created_at' => \Carbon\Carbon::now(),
            ]);
        }
    }
}
