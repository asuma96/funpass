<?php

use App\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class ProductTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $old_products = DB::select('select * from Product');
        foreach ($old_products as $product) {
            $product = Product::create([
                'price1' => $product->Price_1,
                'price2' => $product->Price_2,
                'price3' => $product->Price_3,
                'start_sell_date' => $product->StartSellDate,
                'end_sell_date' => $product->EndSellDate,
                'start_show_on_web_site_date' => $product->StartShowOnWebsiteDate,
                'end_show_on_web_site_date' => $product->EndShowOnWebsiteDate,
                'cover' => $product->Cover,
                'title' => $product->Title,
                'electronic_ticket' => $product->Electronic_Tickets_Allowed,
                'ship_to_home_ticket' => $product->Ship_To_Home_Allowed,
                'pickup_at_center_ticket' => $product->Pick_Up_At_Center_Allowed,
                'special_offer' => $product->Special_Offer,
                'min_age' => $product->Min_Age,
                'ticket_count' => $product->Tickets_Left,
                'default_shipping_method' => $product->Default_Shipping_Method,
                'item_description' => $product->Item_Description,
                'item_term' => $product->Item_Terms,
            ]);
        }
        Schema::drop('Product');
    }
}
