<?php

use Illuminate\Database\Seeder;

class DevOrderSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $products = \App\Product::query()->count();
        $users = \App\User::query()->count();
        for ($i=0; $i<1000; $i++) {
            $order = \App\Order::create([
                'user_id' => rand(12, $users),
                'amount' => $faker->randomFloat(2, 2000, 5000),
                'auth_code' => $faker->toUpper(str_replace(' ', '', $faker->unique()->text(10))),
                'trans_id' => (string) $faker->unique()->numberBetween(),
                'status' => 'Pending Settlement',
                'profile_id' => $faker->unique()->numberBetween(),
                'payment_profile_id' => $faker->unique()->numberBetween(),
            ]);
            $order->items()->attach([
                random_int(1, $products) => [
                    'quantity1' => random_int(1, 10),
                    'quantity2' => random_int(1, 20),
                    'quantity3' => random_int(1, 15),
                    'shipping_method' => 'electronic ticket',
                    'arrive_date' => $faker->dateTimeBetween('now', '+1 years'),
                ],
                random_int(1, $products) => [
                    'quantity1' => random_int(1, 10),
                    'quantity2' => random_int(1, 20),
                    'quantity3' => random_int(1, 15),
                    'shipping_method' => 'electronic ticket',
                    'arrive_date' => $faker->dateTimeBetween('now', '+1 years'),
                ],random_int(1, $products) => [
                    'quantity1' => random_int(1, 10),
                    'quantity2' => random_int(1, 20),
                    'quantity3' => random_int(1, 15),
                    'shipping_method' => 'electronic ticket',
                    'arrive_date' => $faker->dateTimeBetween('now', '+1 years'),
                ]
            ]);
        }
    }
}
