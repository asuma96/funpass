<?php

use Illuminate\Database\Seeder;

class DevImageSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i=0; $i<200; $i++) {
            $img = $faker->imageUrl();
            $filename = md5($img);
            Intervention\Image\Facades\Image::make($img)->save(storage_path('app/public/product/'.$filename));
            \App\Image::create([
                'file_name' => $filename,
            ]);
        }
    }
}
