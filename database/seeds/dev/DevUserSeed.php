<?php

use Illuminate\Database\Seeder;

class DevUserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $products = \App\Product::query()->count();
        \App\User::create([
            'name' => 'a@b.ru',
            'email' => 'a@b.ru',
            'password' => bcrypt('a@b.ru'),
            'api_token' => str_random(60),
            'active' => 1,
            'role' => 'admin',
            'avatar' => $faker->imageUrl(),
            'sex' => 'man',
            'phone' => $faker->phoneNumber,
            'first_name' => 'Dmitriy',
            'last_name' => 'Selihov',
            'birthday' => $faker->dateTimeBetween('-60 years', '-30 years')->format('Y-m-d')
        ]);
        for ($i=0; $i<999; $i++) {
            $user = \App\User::create([
                'name' => $faker->userName,
                'email' => $faker->unique()->email,
                'password' => bcrypt($faker->password()),
                'api_token' => str_random(60),
                'active' => (int) $faker->boolean,
                'role' => 'user',
                'avatar' => $faker->imageUrl(),
                'sex' => $faker->randomElements(['man', 'woman'])[0],
                'phone' => $faker->phoneNumber,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'birthday' => $faker->dateTimeBetween('-60 years', '-30 years')->format('Y-m-d')
            ]);
            $user->wishlist()->sync([random_int(1, $products),random_int(1, $products),random_int(1, $products),random_int(1, $products)]);
        }
    }
}
