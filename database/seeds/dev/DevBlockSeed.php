<?php

use Illuminate\Database\Seeder;

class DevBlockSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Block::create([
            'code' => '[[first]]',
            'title' => 'first block',
            'body' => '<strong>first block</strong>',
        ]);
        \App\Block::create([
            'code' => '[[second]]',
            'title' => 'second block',
            'body' => '<h1>second block</h1>',
        ]);
        \App\Block::create([
            'code' => '[[last]]',
            'title' => 'last block',
            'body' => '<i>last block</i>',
        ]);
    }
}
