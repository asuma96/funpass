<?php

use Illuminate\Database\Seeder;

class DevPageSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i=0; $i<10; $i++) {
            \App\Page::create([
                'meta_keywords' => str_replace(' ', ',', $faker->text(50)),
                'meta_description' => $faker->text(200),
                'title' => $faker->text(200),
                'body' => $faker->text(100) . $faker->randomElements(['[[first]]', '[[second]]', '[[last]]'])[0] . $faker->text(150),
                'url' => str_replace(' ', '-', $faker->unique()->text(50))
            ]);
        }
    }
}
