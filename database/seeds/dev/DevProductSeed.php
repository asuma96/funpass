<?php

use Illuminate\Database\Seeder;

class DevProductSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $images = \App\Image::query()->count('id');
        $categories = \App\Category::query()->count('id');
        $keywords = \App\Keyword::query()->count('id');
        for ($i=0; $i<10000; $i++) {
            $product = \App\Product::create([
                'price1' => $faker->randomFloat(2, 2000, 4000),
                'price2' => $faker->randomFloat(2, 1000, 2000),
                'price3' => $faker->randomFloat(2, 500, 1000),
                'start_sell_date' => $faker->dateTimeBetween('-1 year'),
                'end_sell_date' => $faker->dateTimeBetween('now', '+2 year'),
                'start_show_on_web_site_date' => $faker->dateTimeBetween('-1 year'),
                'end_show_on_web_site_date' => $faker->dateTimeBetween('now', '+2 year'),
                'cover' => \App\Image::query()->where('id', rand(1, $images))->get()->first()->file_name,
                'title' => $faker->unique()->text(120),
                'electronic_ticket' => (int) $faker->boolean,
                'ship_to_home_ticket' => (int) $faker->boolean,
                'pickup_at_center_ticket' => (int) $faker->boolean,
                'special_offer' => (int) $faker->boolean,
                'min_age' => $faker->randomElements([0,1,2,3,4,5,6,7,8,9,10])[0],
                'ticket_count' => $faker->randomElements(['-1', $faker->randomDigit])[0],
                'default_shipping_method' => $faker->randomElements(['ship to home', 'electronic ticket', 'pick up at center ticket'])[0],
                'item_description' => $faker->text(1000),
                'item_term' => $faker->text(1000),
                'ageLabel1' => 'Adults',
                'ageLabel2' => 'Children',
                'ageLabel3' => 'Infants',
                'ageRange1' => '18+',
                'ageRange2' => '7-18',
                'ageRange3' => '0-6',
            ]);
            $product->categories()->sync([random_int(1, $categories),random_int(1, $categories),random_int(1, $categories),random_int(1, $categories),random_int(1, $categories),random_int(1, $categories)]);
            $product->images()->sync([random_int(1, $images), random_int(1, $images), random_int(1, $images), random_int(1, $images), random_int(1, $images), random_int(1, $images),random_int(1, $images)]);
            $product->keywords()->sync([random_int(1, $keywords),random_int(1, $keywords),random_int(1, $keywords),random_int(1, $keywords),random_int(1, $keywords),random_int(1, $keywords),random_int(1, $keywords)]);
        }
    }
}
