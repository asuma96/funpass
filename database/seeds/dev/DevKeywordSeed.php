<?php

use Illuminate\Database\Seeder;

class DevKeywordSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i=0; $i<500; $i++) {
            \App\Keyword::create([
                'title' => $faker->unique()->text(random_int(10, 50)),
            ]);
        }
    }
}
