<?php

use Illuminate\Database\Seeder;

class DevCategorySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $images = \App\Image::query()->count('id');
        for ($i=0; $i<100; $i++) {
            \App\Category::create([
                'title' => $faker->unique()->text(50),
                'description' => $faker->text(1000),
                'cover' => \App\Image::query()->where('id', rand(1, $images))->get()->first()->file_name,
                'show_in_menu' => (int) $faker->boolean,
            ]);
        }
    }
}
