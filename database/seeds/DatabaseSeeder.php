<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DevBlockSeed::class);
        $this->call(DevPageSeed::class);
        $this->call(DevImageSeed::class);
        $this->call(DevCategorySeed::class);
        $this->call(DevKeywordSeed::class);
        $this->call(DevProductSeed::class);
        $this->call(DevUserSeed::class);
        $this->call(DevOrderSeed::class);
    }
}
