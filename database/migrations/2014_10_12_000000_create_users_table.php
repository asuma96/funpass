<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('name');
            $table->string('email')->unique()->index();
            $table->string('password')->index();
            $table->rememberToken()->index();
            $table->string('api_token',60)->unique()->index();
            $table->tinyInteger('active')->default(1)->index();
            $table->enum('role',['admin','moderator','user'])->default('user');
            $table->tinyInteger('marketing_subscribed')->default(1);
            $table->tinyInteger('system_subscribed')->default(1);
            $table->string('avatar')->nullable();
            $table->enum('sex',['man','woman'])->nullable();
            $table->string('phone')->nullable();
            $table->string('first_name',60)->nullable();
            $table->string('last_name',150)->nullable();
            $table->date('birthday')->nullable();
            $table->integer('mc_user_id')->nullable()->index();
            $table->integer('guest_user_id')->nullable()->index();
            $table->string('ciirus_token')->nullable()->index();
            $table->integer('profile_id')->nullable()->index();
            $table->integer('payment_profile_id')->nullable()->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
