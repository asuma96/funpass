<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->decimal('price1');
            $table->decimal('price2');
            $table->decimal('price3');
            $table->dateTime('start_sell_date')->default(Carbon::now());
            $table->dateTime('end_sell_date')->default(Carbon::now());
            $table->dateTime('start_show_on_web_site_date')->default(Carbon::now());
            $table->dateTime('end_show_on_web_site_date')->default(Carbon::now());
            $table->string('cover')->nullable();
            $table->string('title')->default('')->index();
            $table->string('address')->default('');
            $table->tinyInteger('electronic_ticket')->default(1);
            $table->tinyInteger('ship_to_home_ticket')->default(1);
            $table->tinyInteger('pickup_at_center_ticket')->default(1);
            $table->string('commission1', 512)->default('p1 * 1');
            $table->string('commission2', 512)->default('p2 * 1');
            $table->string('commission3', 512)->default('p3 * 1');
            $table->string('discount1', 512)->default('p1 * 1');
            $table->string('discount2', 512)->default('p2 * 1');
            $table->string('discount3', 512)->default('p3 * 1');
            $table->string('sub_total', 512)->default('p1 * q1 + p2 * q2 + p3 * q3');
            $table->string('total', 512)->default('price * (q1 > 1 ? 1 : 1)');
            $table->tinyInteger('special_offer')->default(0);
            $table->integer('min_age')->default(0);
            $table->integer('ticket_count')->default('-1');
            $table->string('default_shipping_method')->default('');
            $table->text('item_description')->nullable();
            $table->text('item_term')->nullable();
            $table->string('ageLabel1')->default('');
            $table->string('ageLabel2')->default('');
            $table->string('ageLabel3')->default('');
            $table->string('ageRange1')->default('');
            $table->string('ageRange2')->default('');
            $table->string('ageRange3')->default('');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('wishlists', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE')->onUpdate('CASCADE');

            $table->integer('product_id')->unsigned()->index();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('CASCADE')->onUpdate('CASCADE');

            $table->tinyInteger('receive_email')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wishlists');
        Schema::dropIfExists('products');
    }
}
