<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id')->index();

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE')->onUpdate('CASCADE');

            $table->decimal('amount')->index();
            $table->string('auth_code')->index();
            $table->string('trans_id')->index();
            $table->string('status')->index();
            $table->integer('profile_id')->index();
            $table->integer('payment_profile_id')->index();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::update('alter table orders AUTO_INCREMENT = 10000;');

        Schema::create('order_product', function (Blueprint $table) {
            $table->integer('product_id')->unsigned()->index();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('CASCADE')->onUpdate('CASCADE');

            $table->integer('order_id')->unsigned()->index();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('CASCADE')->onUpdate('CASCADE');

            $table->integer('quantity1');
            $table->integer('quantity2');
            $table->integer('quantity3');
            $table->string('shipping_method')->index();
            $table->string('actual_shipping_method')->nullable();
            $table->string('tracking_code')->nullable();
            $table->dateTime('arrive_date')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_product');
        Schema::dropIfExists('orders');
    }
}
