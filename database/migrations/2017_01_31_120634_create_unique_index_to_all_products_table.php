<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniqueIndexToAllProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_keyword', function (Blueprint $table) {
            $table->unique(['product_id', 'keyword_id'], 'product_keyword_id_unique');
        });
        Schema::table('product_image', function (Blueprint $table) {
            $table->unique(['product_id', 'image_id'], 'product_image_id_unique');
        });
        Schema::table('product_category', function (Blueprint $table) {
            $table->unique(['product_id', 'category_id'], 'product_category_id_unique');
        });
        Schema::table('product_badge', function (Blueprint $table) {
            $table->unique(['product_id', 'badge_id'], 'product_badge_id_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_keyword', function (Blueprint $table) {
            $table->dropUnique('product_keyword_id_unique');
        });
        Schema::table('product_image', function (Blueprint $table) {
            $table->dropUnique('product_image_id_unique');
        });
        Schema::table('product_category', function (Blueprint $table) {
            $table->dropUnique('product_category_id_unique');
        });
        Schema::table('product_badge', function (Blueprint $table) {
            $table->dropUnique('product_badge_id_unique');
        });
    }
}
