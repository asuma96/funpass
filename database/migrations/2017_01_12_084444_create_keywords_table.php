<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keywords', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('title')->unique()->index();
            $table->timestamps();
        });

        Schema::create('product_keyword', function (Blueprint $table) {
            $table->integer('product_id')->unsigned()->index();
            $table->integer('keyword_id')->unsigned()->index();

            $table->foreign('product_id')->references('id')->on('products')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreign('keyword_id')->references('id')->on('keywords')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_keyword');
        Schema::dropIfExists('keywords');
    }
}
