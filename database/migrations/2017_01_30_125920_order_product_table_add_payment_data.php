<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderProductTableAddPaymentData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_product', function (Blueprint $table) {
            $table->decimal('p1');
            $table->decimal('p2');
            $table->decimal('p3');
            $table->decimal('p1_c');
            $table->decimal('p2_c');
            $table->decimal('p3_c');
            $table->decimal('p1_d');
            $table->decimal('p2_d');
            $table->decimal('p3_d');
            $table->decimal('subtotal');
            $table->decimal('total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_product', function (Blueprint $table) {
            $table->dropColumn('p1');
            $table->dropColumn('p2');
            $table->dropColumn('p3');
            $table->dropColumn('p1_c');
            $table->dropColumn('p2_c');
            $table->dropColumn('p3_c');
            $table->dropColumn('p1_d');
            $table->dropColumn('p2_d');
            $table->dropColumn('p3_d');
            $table->dropColumn('subtotal');
            $table->dropColumn('total');
        });
    }
}
