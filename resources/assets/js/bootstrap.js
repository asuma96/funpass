const $ = window.$ = window.jQuery = require('jquery')

require('jquery.easing')

require('bootstrap')

require('owl.carousel')

require('slicknav/dist/jquery.slicknav.min')

require('../plugins/stretch_gallery/stretch_gallery')
require('../../../node_modules/flexslider/jquery.flexslider')

$.ajaxSetup({
  headers: {
    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
  }
})

