import $ from 'jquery'

export default class HomePage {
  static get name () {
    return 'home'
  }

  constructor () {
    $('#stretch_gallery1').stretch_gallery({})
  }
}
