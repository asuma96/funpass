import $ from 'jquery'

export default class SpecialOffers {
  static get name () {
    return 'special-offers'
  }

  constructor () {
    $('#carousel').flexslider({
      animation: 'slide',
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      itemWidth: 210,
      itemMargin: 5,
      asNavFor: '#slider'
    })

    $('#slider').flexslider({
      animation: 'slide',
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      sync: '#carousel'
    })
  }
}
