import $ from 'jquery'

export default class HomePage {
  static get name () {
    return 'global'
  }

  constructor () {
    $('.footer_top_slider').owlCarousel({
      items: 6,
      autoplay: true,
      loop: true,
      autoplayTimeout: 3000,
      margin: 10,
      nav: false,
      dots: false
    })

    // $(window).scroll(() => $('#back-to-top').toggleClass('top--on', $(window).scrollTop() > 0).toggleClass('top--off', $(window).scrollTop() <= 0))
    //
    // $('#back-to-top').click(() => $('body').animate({ 'scrollTop': 0 }, 'slow'))
  }
}
