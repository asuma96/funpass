import $ from 'jquery'

export default class ProductPage {
  static get name () {
    return 'products'
  }

  constructor () {
    $('.active_category').removeClass('active_category')

    $('.sub_widget.badge_widget li a').each(function () {
      if ($(this).attr('id') === location.href.split('/')[4].split('?')[0]) {
        $(this).parent('li').addClass('active_category')
      }
    })

    $('#carousel').flexslider({
      animation: 'slide',
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      itemWidth: 210,
      itemMargin: 5,
      asNavFor: '#slider'
    })

    $('#slider').flexslider({
      animation: 'slide',
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      sync: '#carousel'
    })
  }
}
