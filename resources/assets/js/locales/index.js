import Vue from 'vue'
import VueI18n from 'vue-i18n'
import ru from './ru'
import en from './en'

Vue.use(VueI18n)

const locales = { en, ru }

Vue.config.lang = 'en'

Object.keys(locales).forEach(lang => Vue.locale(lang, locales[lang]))
