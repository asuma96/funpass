import * as types from './mutation-types'
import { API } from '../constants'
import { request } from '../api'

export default {
  [types.INITIALIZE] ({ state, commit }) {
    commit(types.INITIALIZE, { })
  },
  [types.ADD_TO_CART] ({ commit }, id) {
    request(API.GET_PRODUCT, { id })
      .then(response => commit(types.ADD_TO_CART, { product: response.data }))
      .catch(err => commit(types.ERROR, { err }))
  },
  [types.ADD_PRODUCT_TO_CART] ({ commit }, product) {
    commit(types.ADD_PRODUCT_TO_CART, { product: product })
  },
  [types.ADD_ADDRESS] ({ commit }, products) {
    request(API.ADD_ADDRESS, {})
      .then(response => commit(types.ADD_ADDRESS, { products: response.data }))
      .catch(err => commit(types.ERROR, { err }))
  },
  [types.LOGOUT] ({ commit }) {
    request(API.LOGOUT, {})
      .then(response => commit(types.LOGOUT))
      .catch(err => commit(types.ERROR, { err }))
  },
  [types.REMOVE_FROM_CART] ({ commit }, product) {
    request(API.REMOVE_FROM_CART, {})
      .then(response => commit(types.REMOVE_FROM_CART, { product }))
      .catch(err => commit(types.ERROR, { err }))
  },
  [types.REMOVE_ADDRESS] ({ commit }, address) {
    request(API.REMOVE_ADDRESS, {})
      .then(response => commit(types.REMOVE_ADDRESS, { address }))
      .catch(err => commit(types.ERROR, { err }))
  },

  [types.SINGLE_QUANTITY] ({ commit }, id) {
    request(API.GET_PRODUCT, { id })
      .then(response => commit(types.SINGLE_QUANTITY, { product: response.data }))
      .catch(err => commit(types.ERROR, { err }))
  },
  [types.CONFIRM] ({ commit }, id) {
    request(API.GET_PRODUCT, { id })
      .then(response => commit(types.CONFIRM, { product: response.data }))
      .catch(err => commit(types.ERROR, { err }))
  },
  [types.MODAL] ({ commit }, url) {
    request(API.WISHLIST, { url })
      .then(response => commit(types.MODAL, { product: response.data }))
      .catch(err => commit(types.ERROR, { err }))
  },
  [types.MODAL_DEL] ({ commit }, url) {
    request(API.WISHLIST, { url })
      .then(response => commit(types.MODAL_DEL, { product: response.data }), location.href = '/wishlist')
      .catch(err => commit(types.ERROR, { err }))
  },
  [types.POST] ({ state, commit }) {
    const card = state.cart.products.filter(p => p.check).map(({ default_shipping_method, id, quantity1, quantity2, quantity3, arrive_date }) => ({ shipping_method: default_shipping_method, product_id: id, quantity1, quantity2, quantity3, arrive_date }))
    const address = state.request.address
    const user_card_data = state.request.user_card

    request(API.BUY_POST, { card, address, user_card_data })
      .then(response => {
        commit(types.MODAL_CHECK, { product: response.data })
        setTimeout(() => {
          commit(types.CLEAR_CART)
          location.href = '/'
        }, 1000)
      })
      .catch(err => commit(types.ERROR, { err }))
  },
  [types.SEARCH] ({ commit }, query) {
    request(API.QUERY, { query })
      .then(response => commit(types.SEARCH, { product: response.data }))
      .catch(err => commit(types.ERROR, { err }))
  },
  [types.SEARCH_GO] ({ commit }, p) {
    location.href = '/single-product/' + p.id
  },
  [types.CLEAR_CART] ({ commit }) {

  }
}
