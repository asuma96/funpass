import * as types from './mutation-types'

export default {
  [types.INITIALIZE] (state, { products }) {
    state.cart.products = products
  },
  [types.MODAL] (state, { product }) {
    state.modalAlarm = false
    state.modalSuccess = false
    if (product) {
      state.modalSuccess = true
      state.modalAlarm = false
    } else {
      state.modalAlarm = true
      state.modalSuccess = false
    }
  },
  [types.MODAL_CHECK] (state, { product }) {
    state.modalSuccessCheck = true
  },
  [types.ADD_TO_CART] (state, { product }) {
    state.modalSuccess = false
    state.modalAlarm = false
    if (!state.cart.products.find(p => p.id === product.id)) {
      product.quantity1 = 1
      product.quantity2 = 0
      product.quantity3 = 0
      product.arrive_date = '2017-01-28'
      product.default_shipping_method = 0
      state.cart.products.push(product)
    }
  },
  [types.ADD_PRODUCT_TO_CART] (state, { product }) {
    state.modalSuccess = false
    state.modalAlarm = false
    if (!state.cart.products.find(p => p.id === product.id)) {
      state.cart.products.push(product)
    }
  },
  [types.REMOVE_FROM_CART] (state, { product }) {
    state.modalSuccess = false
    state.modalAlarm = false
    state.cart.products.splice(state.cart.products.indexOf(product), 1)
  },
  [types.LOGOUT] (state) {
    state.user = {}
    location.href = '/'
  },
  [types.ADD_ADDRESS] (state, { product }) {
    state.address.push({ id: !state.address.length ? 1 : parseInt(state.address[state.address.length - 1].id) + 1 })
  },
  [types.REMOVE_ADDRESS] (state, { address }) {
    state.address.splice(state.address.indexOf(address), 1)
  },
  [types.CLEAR_CART] (state) {
    state.cart = { products: [] }
  },
  [types.ERROR] (state, { err }) {
    state.modalError = true
    state.modalSuccessCheck = false
  },
  [types.SEARCH] (state, { product }) {
    state.search = product.data
  },
  [types.SEARCH_GO] (state, { str }) {
    state.searchVal.v = str.title
  },
  [types.SINGLE_QUANTITY] (state, { product }) {
    if (!state.cart.products.find(p => p.id === product.id)) {
      state.cart.products.push(product)
    }
  }
}
