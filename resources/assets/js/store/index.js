import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import actions from './actions'
import mutations from './mutations'

Vue.use(Vuex)

const state = {
  cart: {
    products: []
  },
  search: [{
    title: '',
    id: 0
  }],
  searchVal: { v: '' },
  searchId: {},

  cc: {},
  address: [

    { id: 1 }
  ],
  singleProduct: [
    {
      quantity1: 0,
      price1: 0,
      quantity2: 0,
      price2: 0,
      quantity3: 0,
      price3: 0
    }
  ],
  loginModel: { email: '', password: '' },
  registrModel: { name: '', email: '', password: '', password_confirmation: '' },
  checkoutModel: { address: '', city: '', state: '', country: '', zip: '' },
  request: {
    user_card: {
      card_number: '',
      card_year: '',
      card_month: '',
      card_code: ''
    },
    address: {
      country: '',
      state: '',
      city: '',
      zip: '',
      address: ''
    }
  },
  modalSuccess: false,
  modalAlarm: false,
  modalError: false,
  modalSuccessCheck: false
}

const { Store } = Vuex

export default new Store({
  plugins: [createPersistedState({
    getState: (key) => JSON.parse(localStorage.getItem(key)),
    setState: (key, state) => localStorage.setItem(key, JSON.stringify({ cart: state.cart, address: state.address }))
  })],
  state: { ...state, ...JSON.parse(localStorage.getItem('vuex')) },
  actions,
  mutations
})
