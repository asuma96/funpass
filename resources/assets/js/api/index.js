import { API, METHOD } from '../constants'
import Vue from 'vue'

export const ENDPOINTS = {
  [API.GET_PRODUCT]: { url: '/api/v1/products/:id', method: METHOD.GET },
  [API.REMOVE_FROM_CART]: { url: '', method: METHOD.GET },
  [API.REMOVE_ADDRESS]: { url: '', method: METHOD.GET },
  [API.ADD_ADDRESS]: { url: '', method: METHOD.GET },
  [API.LOGOUT]: { url: '/logout', method: METHOD.POST },
  [API.SINGLE_QUANTITY]: { url: '', method: METHOD.GET },
  [API.CONFIRM]: { url: '', method: METHOD.POST },
  [API.BUY_POST]: { url: '/buy', method: METHOD.POST },
  [API.WISHLIST]: { url: ':url', method: METHOD.GET },
  [API.QUERY]: { url: '/api/v1/products?search=:query&searchBy=title,address,id&orderBy=title', method: METHOD.GET }
}

export function request (api, data = {}, headers = {}) {
  const base = 'http://127.0.0.1:7777'

  const endpoint = ENDPOINTS[api]

  let url = endpoint.url

  for (const key in data) {
    url = url.replace(`:${key}`, data[key])
  }

  return Vue.http[(endpoint.method || 'get').toLowerCase()](`${base}${url}`, data)
}

