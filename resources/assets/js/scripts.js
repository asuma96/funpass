import $ from 'jquery'

import Global from './pages/Global'
import HomePage from './pages/Home'
import ProductPage from './pages/Product'
import MyAccount from './pages/MyAccount'
import BestSellers from './pages/BestSellers'
import SpecialOffers from './pages/SpecialOffers'

import './scripts'

$(() => {
  new Global()

  switch ($('[page]:eq(0)').attr('page')) {
    case HomePage.name:
      new HomePage()
      break
    case ProductPage.name:
      new ProductPage()
      break
    case MyAccount.name:
      new MyAccount()
      break
    case BestSellers.name:
      new BestSellers()
      break
    case SpecialOffers.name:
      new SpecialOffers()
      break
  }
})