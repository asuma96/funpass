import './bootstrap'

import $ from 'jquery'

import Vue from 'vue'
import VueResource from 'vue-resource'
import VeeValidate from 'vee-validate'

import { mapState, mapActions } from 'vuex'

import './scripts'
import './filters'
import './locales'

import store from './store'

import * as types from './store/mutation-types'

import cartComponent from './components/cart.vue'
import shoppingCartComponent from './components/shoppingCart.vue'
import addressComponent from './components/address.vue'
import singleComponent from './components/single.vue'
import searchComponent from './components/search.vue'
import addresspaymentComponent from './components/addressPayment.vue'
import datepicker from './components/bootstrap/Datepicker.vue'
import modal from './components/bootstrap/Modal.vue'
import modaltestComponent from './components/modal.vue'

Vue.use(VueResource)
Vue.use(VeeValidate)

Vue.http.interceptors.push((request, next) => {
  request.headers.set('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'))

  next()
})

new Vue({
  store,
  el: '#app',
  components: {
    datepicker,
    modal,
    cartComponent,
    shoppingCartComponent,
    addressComponent,
    singleComponent,
    addresspaymentComponent,
    searchComponent,
    modaltestComponent
  },
  data () {
    return {
      value: '2015-06-10',
      format: 'yyyy-MM-dd',
      clear: true,
      placeholder: 'yyyy-MM-dd'
    }
  },
  watch: {
    format () {
      this.value = this.$refs.dp.stringify()
    }
  },

  computed: {
    ...mapState({
      products: state => state.cart.products,
      request: state => state.request.user_card_data,
      loginModel: state => state.loginModel,
      registrModel: state => state.registrModel,
      checkoutModel: state => state.checkoutModel
    }),
    total () {
      return this.products.reduce((total, p) => total + p.price * p.quantity, 0)
    }
  },
  methods: {
    ...mapActions([
      types.INITIALIZE,
      types.ADD_TO_CART,
      types.LOGOUT,
      types.REMOVE_FROM_CART,
      types.ADD_ADDRESS,
      types.REMOVE_ADDRESS,
      types.SINGLE_QUANTITY,
      types.CONFIRM,
      types.POST,
      types.MODAL,
      types.MODAL_DEL,
      types.SEARCH,
      types.DATA_INPUT,
      types.SEARCH_GO
    ]),
    VALIDATE_FORM (e) {
      this.$validator.validateAll().then(success => {
        if (!success) {
          e.preventDefault()
        }
      })
    },
    productInCart (id) {
      return this.$store.state.cart.products.find(p => p.id === id)
    },
    modalSuccess () {
      return this.$store.state.modalSuccess
    },
    modalAlarm () {
      return this.$store.state.modalAlarm
    },
    modalError () {
      return this.$store.state.modalError
    },
    modalSuccessCheck () {
      return this.$store.state.modalSuccessCheck
    }
  },
  mounted () {
    this.INITIALIZE()
  }
})
