<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<html>
<head>
    @include('includes.head')
</head>
<body id="index_two" page="@yield('page')" class="page-@yield('page')">
    <div id="app">
        <?php $user = \Illuminate\Support\Facades\Auth::user() ?>
        @include('includes.header')

        <div id="main">
            @yield('content')
        </div>

        @include('includes.footer')
    </div>

    @include('includes.foot')
</body>
</html>