    <!-- end header -->
    <div class="container p-none">
        <div class="">
            <div class="">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>
                    <div class="panel-body">
                         <form class="form-horizontal ng-pristine ng-valid bv-form"  role="form" method="POST" action="/register">
                            {{ csrf_field() }}
                            <div class="form-group has-feedback">
                                <label class="col-md-4 control-label">Username</label>
                                <div class="col-md-6">
                                        <input type="text" class="form-control"  v-model="loginModel.name" v-validate="" data-vv-rules="required|min:2" name="name" placeholder="username"  >
                                    <p class="text-danger" v-if="errors.has('name')" v-html="errors.first('name')"></p>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <label class="col-md-4 control-label">E-Mail Address</label>
                                <div class="col-md-6">
                                    <input type="email" v-model="loginModel.email" v-validate="" data-vv-rules="required|email" class="form-control" name="email" placeholder="email"  required>
                                    <p class="text-danger" v-if="errors.has('email')" v-html="errors.first('email')"></p>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control"  name="password" v-model="loginModel.password" v-validate="" data-vv-rules="required|min:6" placeholder="Password" >
                                    <p class="text-danger" v-if="errors.has('password')" v-html="errors.first('password')"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Confirm Password</label>
                                <div class="col-md-6">
                                    <input type="password" v-model="loginModel.password_confirmation" v-validate="" data-vv-rules="required|min:6" class="form-control" name="password_confirmation"  placeholder="Password">
                                    <p class="text-danger" v-if="errors.has('password_confirmation')" v-html="'The Confirm password field is required.'"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button  type="submit" class="btn btn-default" :disabled="fields.clean() || errors.any()">
                                        <i class="fa fa-btn fa-user"></i>
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

