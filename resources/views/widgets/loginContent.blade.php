<div class="container p-l-none p-r-none">
    <div class="panel panel-default">
        <div class="panel-heading">Login</div>
            <div class="panel-body">

                <form id="form" class="form-horizontal" role="form" method="POST" @submit="VALIDATE_FORM">
                {{csrf_field()}}
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="email">E-Mail Address</label>
                        <div class="col-md-6">
                            <input v-model="loginModel.email" v-validate="" data-vv-rules="required|email" class="form-control" name="email" type="email" placeholder="Email">
                            <p class="text-danger" v-if="errors.has('email')" v-html="errors.first('email')"></p>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Password</label>
                        <div class="col-md-6">
                            <input v-model="loginModel.password" v-validate="" data-vv-rules="required|min:6" type="password" class="form-control" name="password">
                            <p class="text-danger" v-if="errors.has('password')" v-html="errors.first('password')"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember_token"> Remember Me
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button :disabled="fields.clean() || errors.any()" type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-sign-in"></i>Login
                            </button>
                            <a class="btn btn-link" href="https://www.facebook.com/">Login with Facebook</a>
                            <a class="btn btn-link" href="{{ URL::to('/register')}}">Register</a>
                            <a class="btn btn-link" href="{{ URL::to('/password/reset')}}">Forgot Your Password?</a>
                        </div>
                    </div>
                </form>
            </div>
    </div>
</div>