<section class="latest_post_area">
    <div class="container">
        <div class="row">
            <div class="latest_post">
                <div class="section_title">
                    <h4>SEASONAL EVENTS</h4>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="single_latest_post">
                            <div class="grid">
                                <figure class="effect-honey">
                                    <a href="#"><img src="/images/starwars.png" alt=""></a>
                                    <figcaption>
                                        <h6><span>Starts</span> 1 Dec 2016</h6>
                                    </figcaption>
                                </figure>
                            </div><!-- grid -->
                        </div><!-- .single_latest_post -->
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="single_latest_post">
                            <div class="grid">
                                <figure class="effect-honey">
                                    <a href="#"><img src="/images/mardi.png" alt=""></a>
                                    <figcaption>
                                        <h6><span>Starts</span> 8 Feb 2016</h6>
                                    </figcaption>
                                </figure>

                            </div><!-- grid -->
                        </div><!-- .single_latest_post -->
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="single_latest_post">
                            <div class="grid">
                                <figure class="effect-honey">
                                    <a href="#"><img src="/images/epcotflower.png" alt=""></a>
                                    <figcaption>
                                        <h6><span>Starts</span> 2 Mar 2016</h6>
                                    </figcaption>
                                </figure>

                            </div><!-- grid -->
                        </div><!-- .single_latest_post -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
