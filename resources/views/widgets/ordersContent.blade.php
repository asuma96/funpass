<section class="breadcrumb_main_area margin-bottom-50">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="breadcrumb_main">
                <h2>Wishlist</h2>
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Wishlist</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="shopping-cart-area">
    <div class="container">
        <div class="row">
            <div class="panel-group" id="accordion">
                    @if(count($orders) == 0)
                        <tr>
                            <td class="text-center" colspan="5"><h4 class="p-md">Your shopping history is empty for now!</h4></td>
                        </tr>
                    @else
                        @foreach($orders as $order)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$order->id}}">Order № {{$order->id}} </a>
                                </h4>
                            </div>
                            <div id="collapse{{$order->id}}" class="panel-collapse collapse">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr class="table-title">
                                                <td class="cart-img">Image</td>
                                                <td class="cart-info"> Product Title</td>
                                                <td class="cart-name">Product information</td>
                                                <td class="cart-status">Status</td>
                                            </tr>
                                            @foreach($order->items as $item)
                                            <tr class="table-product-info">
                                                <td class="prod-name">
                                                    <div class="prod-table-img-c">
                                                        <img class="orderimg" alt="product-name" src="/storage/product/{{$item -> cover}}" >
                                                    </div>
                                                </td>
                                                <td class="prod-img-center">
                                                    <div>{{$item -> title}}</div>
                                                </td>
                                                <td class="prod-tot">
                                                    <p>Start Sell Date: <span>{{ $item->start_sell_date }}</span></p>
                                                    <p>End Sell Date: <span>{{ $item->end_sell_date }}</span></p>
                                                    <p>Min Age: <span>{{ $item->min_age }}</span></p>
                                                </td>
                                                <td class="prod-tot">
                                                   {{$order -> status}}
                                                </td>
                                            </tr>
                                            @endforeach
                                         </tbody>
                                    </table>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>