<section class="breadcrumb_main_area margin-bottom-50">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="breadcrumb_main">
                <h2>Attractions</h2>
                <ul class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li><a href="#">Attractions</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- start category area -->
<div class="content_area" id ="top_page" >
    <div class="container">
        <div class="row">
            <div class="products_content">
                <div class="row">
                    @include('includes.categoriesList')
                    <div id="park-info" class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Walt Disney World Resort</h2>
                                <h4><i>Orlando</i></h4>
                                @include('includes.productsGallery')
                            </div>
                        </div>
                        <div class="products_in_category">
                            <!--Start Row View-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="product_item_details no_border_with_50_pad ticketChooser">
                                        <div role="tabpanel">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active">
                                                    <a href="#tickets" aria-controls="description" role="tab" data-toggle="tab">tickets</a>
                                                </li>
                                                <li>
                                                    <select class="form-control" name="" id="">
                                                        <option value="date_up">Date up</option>
                                                        <option value="date_down">Date down</option>
                                                        <option value="price_up">Price up</option>
                                                        <option value="price_down">Price down</option>
                                                        <option value="title_up">Title up</option>
                                                        <option value="title_down">Title down</option>
                                                    </select>
                                                </li>
                                            </ul>
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="tickets">
                                                    <div id="rowView">
                                                        @if(count($products) == 0)
                                                        <div class="row emptylistborder">
                                                            <h4 class="p-md" align="center">No products found by your request</h4>
                                                        </div>
                                                        @else
                                                            @foreach($products as $product)
                                                                @include('includes.productInformation')
                                                            @endforeach
                                                            {{ $products->links() }}
                                                        @endif

                                                    </div>

                                                    <!--end row view-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="#top_page" id="back-to-top" class="top--off">Back to Top</a>
</div>

<modal :show.sync="modalSuccess()" class="success_wishlist" effect="zoom">
    <div slot="modal-header" class="modal-header">
        <h4 class="modal-title">Success</h4>
    </div>
    <div slot="modal-body" class="modal-body">Product added to wishlist
        <a href="/wishlist" class="add_cart_btn">See more</a>
    </div>
</modal>

<modal :show.sync="modalAlarm()" class="alarm_wishlist" effect="zoom">
    <div slot="modal-header" class="modal-header">
        <h4 class="modal-title">Attention</h4>
    </div>
    <div slot="modal-body" class="modal-body">
        The product is already in the wishlist
        <a href="/wishlist" class="add_cart_btn">See more</a>
    </div>
</modal>
<!-- End category area -->