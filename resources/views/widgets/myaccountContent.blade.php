<div class="container p-l-none p-r-none myAccount">
            <div class="panel panel-default">
                <div class="panel-heading">My Account</div>
                <div class="panel-body">
                    <div class="container">
                            <!-- left column -->
                            <div class="col-md-3">
                                <div class="text-center">
                                    <div class="avatar_account">
                                        @if (Auth::user()->avatar == null)
                                            <img src="/images/logo.png">
                                        @else
                                        <img src="/storage/avatar/{{ Auth::user()->id }}/{{ Auth::user()->avatar }}">
                                        @endif
                                    </div><!--Question-Lita-->
                                    <form enctype="multipart/form-data" action="/uploadavatar" method="POST">
                                        {{ csrf_field() }}
                                        <div class="clearfix p-t-sm">
                                            <div class="clearfix">
                                                <input type="hidden" name="ID" value='' type="text"/>
                                                <input type="hidden" name="MAX_FILE_SIZE" value="900000"/>
                                                <label class="file_upload pull-left">
                                                    <span class="button btn btn-default btn-sm">Select file</span>
                                                    <input name="file" type="file">
                                                </label>
                                                <button class="btn btn-default btn-sm" type="submit">SET AVATAR</button>
                                            </div>
                                          </div>
                                          <div class="col-md-12">
                                                <div class="form-group p-l-md">
                                                    <span class="help-block">
                                                        <strong>{{-- $errors->first('Avatar') --}}</strong>
                                                    </span>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                            <!-- edit form column -->
                            <div class="col-md-9 personal-info">
                                <form class="form-horizontal myaccountform" role="form" method="post" action="/myaccount">
                                    {{ csrf_field() }}
                                    <h4>Personal info</h4>
                                    <div class="clearfix">
                                        <div class="col-md-6">
                                            <div class="form-group m-n">
                                                <label class="control-label">Username</label>
                                                <div class="p-t-xs">
                                                    <input type="text" placeholder="Username"
                                                           class="form-control ng-pristine ng-valid ng-not-empty ng-touched" name="name"
                                                           value="{{ Auth::user()->name }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group m-n">
                                                <label class="control-label">Telephone</label>
                                                <div class="p-t-xs">
                                                    <input type="text" placeholder="Telephone"
                                                           class="form-control" name="phone"
                                                           value="{{ Auth::user()->phone }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-b-sm clearfix">
                                        <div class="col-md-6">
                                            <div class="form-group m-n">
                                                <label class="control-label">First Name</label>
                                                <div class="p-t-xs">
                                                    <input type="text" placeholder="First Name"
                                                           class="form-control" name="first_name"
                                                           value="{{ Auth::user()->first_name }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group m-n">
                                                <label class="control-label">Last Name</label>
                                                <div class="p-t-xs">
                                                    <input type="text" placeholder="Last Name"
                                                           class="form-control" name="last_name"
                                                           value="{{ Auth::user()->last_name }}">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group m-n">
                                                <label class="control-label">Sex</label>
                                                <div class="p-t-xs">
                                                    <input type="radio" placeholder="woman" name="sex"
                                                           value="woman" class="radio-controls"
                                                           @if(Auth::user()->sex == 'woman') checked @endif
                                                    > Women
                                                    <input type="radio" placeholder="man" name="sex"
                                                           value="man" class="radio-controls"
                                                           @if(Auth::user()->sex == 'man') checked @endif
                                                    > Man
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group m-n">
                                                <label class="control-label">Date of Birth</label>
                                                <div class="p-t-xs myacc_birthday">
                                                    <div>
                                                        <datepicker
                                                                :value.sync="value"
                                                                :format="format"
                                                                :clear-button="clear"
                                                                :placeholder="placeholder"  :class="form-control" :name="birthday">
                                                        </datepicker>
                                                    </div>
                                                 <!--   <input type="text" placeholder="Date of Birth"
                                                           class="form-control" name="birthday"
                                                           value="{{ Auth::user()->birthday }}">-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <address-component></address-component>
                                    <div class="p-t-md padding_class">
                                        <button type="button" id="addAdress" class="btn btn-default" @click="ADD_ADDRESS()"><i class="fa fa-plus" aria-hidden="true"></i> Add address</button>
                                    </div>
                                    <div class="clearfix p-t-sm">
                                        <div class="clearfix">
                                            <div class="col-md-12">
                                                <label class="control-label">Receive Marketing Emails</label>
                                                <div class="p-t-xs">
                                                    <input type="checkbox"
                                                           class="" name="marketing_unsubscribed" value="1">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="control-label">Receive System Emails</label>
                                                <div class="p-t-xs">
                                                    <input type="checkbox"
                                                           class="" name="system_unsubscribed" value="1">
                                                </div>
                                                <div class="text-right display_ib float_left">
                                                    <button type="button" class="btn btn-default">
                                                        Change password
                                                    </button>
                                                </div>
                                                <div class="text-right pull-right display_ib">
                                                    <button class="btn btn-default" >
                                                        <i class="fa fa-btn fa-refresh apply"></i>Apply Changes
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                   </div>
                </div>
            </div>
        </div>
