<!-- Stretch Gallery containter -->
<div class="page_class" id="stretch_gallery1">
    <div class="sg">
        <!-- Slide begining -->
        <div class="sg_slide slider_image_1">
            <a href="{{ URL::to('/product-category/1')}}">
                <div class="sg_dummy">
                    <span class="sg_text_block">
                        <div class="sg_caption">Legoland</div>
                            <div class="sg_description">Let the kids take control for a little while</div>
                    </span>
                </div>
            </a>
        </div>
        <!-- End of slide -->
        <!-- Slide begining -->
        <div class="sg_slide slider_image_2">
            <a href="{{ URL::to('/product-category/2')}}">
                <div class="sg_dummy">
                    <span class="sg_text_block">
                        <div class="sg_caption">Universal Studios & Islands of Adventure</div>
                        <div class="sg_description">Explore the magic at the wizarding world of Harry Potter</div>
                    </span>
                </div>
            </a>
        </div>
        <!-- End of slide -->
        <!-- Slide begining -->
        <div class="sg_slide slider_image_3">
            <a href="{{ URL::to('/product-category/3')}}">
                <div class="sg_dummy">
                    <span class="sg_text_block">
                        <div class="sg_caption">Disney World</div>
                        <div class="sg_description">Experience the happiest place in the world</div>
                    </span>
                </div>
            </a>
        </div>
        <!-- End of slide -->
        <!-- Slide begining -->
        <div class="sg_slide slider_image_4">
            <a href="{{ URL::to('/product-category/4')}}">
                <div class="sg_dummy">
                  <span class="sg_text_block">
                    <div class="sg_caption">Sea World</div>
                    <div class="sg_description">Fish are friends, not food!</div>
                  </span>
                </div>
            </a>
        </div>
        <!-- End of slide -->
        <!-- Slide begining -->
        <div class="sg_slide slider_image_5">
            <a href="{{ URL::to('/product-category/5')}}">
                <div class="sg_dummy">
                    <span class="sg_text_block">
                        <div class="sg_caption">Orlando 360</div>
                        <div class="sg_description">Get ready for some serious fun!</div>
                    </span>
                </div>
            </a>
        </div>
        <!-- End of slide -->
    </div>
</div>
<!-- End of Stretch Gallery containter -->