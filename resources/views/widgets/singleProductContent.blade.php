<section class="breadcrumb_main_area margin-bottom-50">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="breadcrumb_main">
                <h2>{{$product -> title}}</h2>
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Product Name</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- start category area -->
<div class="category_area">
    <div class="container">
        <div class="row">
            <div class="category">
                <div class="row">
                        @include('includes.categoriesList')
                    <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                        <div class="category_products">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="single_product_page fix">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-5 no-padding-right">
                                                    <div class="single_product_image">
                                                        <div class="fotorama" data-nav="thumbs" data-fit="cover" data-width="100%" data-loop="true" data-autoplay="true">
                                                            <img src="/storage/product/{{$product -> cover}}" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-7 no-padding-left">
                                                    <div class="single_product_details">
                                                        <div class="single_product_description">
                                                            <h6>Ticket information</h6>
                                                            <?php $user = \Illuminate\Support\Facades\Auth::user()?>
                                                            @if($user)
                                                                <a class="btn btn_reverse add_to_wishlist_single blue_button" @CLICK="MODAL('/api/v1/wishlist/attach/{{$product ->id}}?api_token={{ Auth::user()->api_token }}')">Add to wishlist    <i class="fa fa-heart-o icon-toolbar"></i></a>
                                                            @endif
                                                            <div class="clean"></div>
                                                            <p>
                                                                <b>Out of stock</b>
                                                            </p>
                                                            <p>
                                                                @if($product->tickets_count = 0)
                                                                    <b>Sold out</b><br>
                                                                @endif
                                                            </p>
                                                                <div>
                                                                    <single-component :product="{{$product->toJson()}}"></single-component>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<modal :show.sync="modalSuccess()" class="success_wishlist" effect="zoom">
    <div slot="modal-header" class="modal-header">
        <h4 class="modal-title">Success</h4>
    </div>
    <div slot="modal-body" class="modal-body">Product added to wishlist
        <a href="/wishlist" class="add_cart_btn">See more</a>
    </div>

</modal>
<modal :show.sync="modalAlarm()" class="alarm_wishlist" effect="zoom">
    <div slot="modal-header" class="modal-header">
        <h4 class="modal-title">Attention</h4>
    </div>
    <div slot="modal-body" class="modal-body">
        The product is already in the wishlist
        <a href="/wishlist" class="add_cart_btn">See more</a>
    </div>

</modal>


