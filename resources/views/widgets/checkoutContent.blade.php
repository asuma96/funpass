<section class="breadcrumb_main_area margin-bottom-50">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="breadcrumb_main">
                <h2>Checkout</h2>
                <ul class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li><a href="/faq">Checkout</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="welcome_area">
    <div class="container">
        <div class="row">
            <div class="welcome">
                <div class="row">
                    <div class="section_title">
                        <h4>Checkout</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <modal :show.sync="modalError()" class="" effect="zoom">
        <div slot="modal-header" class="modal-header">
            <h4 class="modal-title">Error</h4>
        </div>
        <div slot="modal-body" class="modal-body">
            Unable to perform action
        </div>
    </modal>
    <modal :show.sync="modalSuccessCheck()" class="" effect="zoom">
        <div slot="modal-header" class="modal-header">
            <h4 class="modal-title">Success</h4>
        </div>
        <div slot="modal-body" class="modal-body">
            Payment was successful!
        </div>
    </modal>
</section>
<section class="container checkout" >
        <?php
        $user = \Illuminate\Support\Facades\Auth::user()
        ?>
        @if($user)
    <addresspayment-component  :user="{{$user->toJson()}}"></addresspayment-component>
            @else
                <div class="text-center error_autorize" colspan="5"><h4 class="p-md">You are not authorized. <a href="/login">Login</a> or <a href="/register"> register</a> to pay for your order</h4></div>
            @endif
</section>
