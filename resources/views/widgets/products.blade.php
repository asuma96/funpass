<section class="products_area">
    <div class="container">
        <div class="row">
            <div class="products">
                <div class="row">
                    <!-- start third product row -->
                    <div class="third_product_row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="single_product">
                                <div class="grid">
                                    <figure class="effect-winston">
                                        <img src="/images/disney3.png" alt="img01">
                                        <figcaption>
                                            <h2>Disney <span>tickets</span></h2>
                                            <p>Hot sale</p>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="half_row margin-bottom-40">
                                        <div class="single_product">
                                            <div class="grid">
                                                <figure class="effect-julia">
                                                    <img src="/images/product-six.png" alt="img01">
                                                    <figcaption>
                                                        <h2>universal studios</h2>
                                                        <div>
                                                            <p>on oder of $369.00</p>
                                                            <p>Use Code Freeship <span></span>FS02</p>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="half_row half_last_row margin-bottom-40">
                                        <div class="single_product">
                                            <div class="grid">
                                                <figure class="effect-julia">
                                                    <img src="/images/discove.png" alt="img01">
                                                    <figcaption>
                                                        <h3>discovery cove</h3>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="single_product">
                                <div class="grid">
                                    <figure class="effect-moses">
                                        <img src="/images/product-eight.png" alt="img01">
                                        <figcaption>
                                            <h2>make</h2>
                                            <p>a splash</p>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end third product row -->
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <!-- start new featured sales tab -->
                        <div class="new_featured_sales">
                            <div role="tabpanel" class="products_list">

                                <!-- Nav tabs -->
                                <div class="section_title">
                                    <h4>PACKAGE DEALS</h4>
                                </div>
                                <div class="row">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="new_tab">
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="single_tab_content">
                                                    <figure class="effect-lexi">
                                                        <a href="#"><img src="/images/special1.jpg" alt=""></a>
                                                        <figcaption>
                                                            <a href="#" class="add_to_cart">Add to cart</a>
                                                            <p class="discount"></p>

                                                        </figcaption>
                                                    </figure>
                                                    <div class="product_description">
                                                        <h4>MAGICAL PACKAGE DEAL!</h4>
                                                        <a href="#">Et harum quidem rerum dress</a>
                                                        <div class="star">
                                                            <a href="#">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </a>
                                                        </div>
                                                        <div class="price">
                                                            <p><span class="old_price">$409.00</span> <span
                                                                        class="new_price">$369.00</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="single_tab_content">
                                                    <figure class="effect-lexi">
                                                        <a href="#"><img
                                                                    src="/images/special2.jpg"
                                                                    alt=""></a>
                                                        <figcaption>
                                                            <a href="#" class="add_to_cart">Add to cart</a>
                                                            <p class="discount"></p>

                                                        </figcaption>
                                                    </figure>
                                                    <div class="product_description">
                                                        <h4>MAGICAL PACKAGE DEAL!</h4>
                                                        <a href="#">Et harum quidem rerum dress</a>
                                                        <div class="star">
                                                            <a href="#">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </a>
                                                        </div>
                                                        <div class="price">
                                                            <p><span class="old_price">$409.00</span> <span
                                                                        class="new_price">$369.00</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="single_tab_content">
                                                    <figure class="effect-lexi">
                                                        <a href="#"><img src="/images/special3.jpg"
                                                                         alt=""></a>
                                                        <figcaption>
                                                            <a href="#" class="add_to_cart">Add to cart</a>
                                                            <p class="discount"></p>

                                                        </figcaption>
                                                    </figure>
                                                    <div class="product_description">
                                                        <h4>MAGICAL PACKAGE DEAL!</h4>
                                                        <a href="#">Et harum quidem rerum dress</a>
                                                        <div class="star">
                                                            <a href="#">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </a>
                                                        </div>
                                                        <div class="price">
                                                            <p><span class="old_price">$409.00</span> <span
                                                                        class="new_price">$369.00</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="single_tab_content">
                                                    <figure class="effect-lexi">
                                                        <a href="#"><img src="/images/special4.jpg" alt=""></a>
                                                        <figcaption>
                                                            <a href="#" class="add_to_cart">Add to cart</a>
                                                            <p class="discount"></p>

                                                        </figcaption>
                                                    </figure>
                                                    <div class="product_description">
                                                        <h4>MAGICAL PACKAGE DEAL!</h4>
                                                        <a href="#">Et harum quidem rerum dress</a>
                                                        <div class="star">
                                                            <a href="#">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </a>
                                                        </div>
                                                        <div class="price">
                                                            <p><span class="old_price">$409.00</span> <span
                                                                        class="new_price">$369.00</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="featured_tab">
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="single_tab_content">
                                                    <figure class="effect-lexi">
                                                        <a href="#"><img src="/images/tab-image-one.png" alt=""></a>
                                                        <figcaption>
                                                            <a href="#" class="add_to_cart">Add to cart</a>
                                                            <p class="discount"></p>
                                                            <p class="favourite">
                                                                <a href="#"><i class="fa fa-refresh"></i></a>
                                                                <a href="#"><i class="fa fa-heart"></i></a>
                                                                <a href="#"><i class="fa fa-search"></i></a>
                                                            </p>
                                                        </figcaption>
                                                    </figure>
                                                    <div class="product_description">
                                                        <a href="#">Et harum quidem rerum dress</a>
                                                        <div class="star">
                                                            <a href="#">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </a>
                                                        </div>
                                                        <div class="price">
                                                            <p><span class="old_price">$409.00</span> <span
                                                                        class="new_price">$369.00</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="single_tab_content">
                                                    <figure class="effect-lexi">
                                                        <a href="#"><img
                                                                    src="/images/tab-image-two-without-overlay.png"
                                                                    alt=""></a>
                                                        <figcaption>
                                                            <a href="#" class="add_to_cart">Add to cart</a>
                                                            <p class="discount"></p>
                                                            <p class="favourite">
                                                                <a href="#"><i class="fa fa-refresh"></i></a>
                                                                <a href="#"><i class="fa fa-heart"></i></a>
                                                                <a href="#"><i class="fa fa-search"></i></a>
                                                            </p>
                                                        </figcaption>
                                                    </figure>
                                                    <div class="product_description">
                                                        <a href="#">Et harum quidem rerum dress</a>
                                                        <div class="star">
                                                            <a href="#">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </a>
                                                        </div>
                                                        <div class="price">
                                                            <p><span class="old_price">$409.00</span> <span
                                                                        class="new_price">$369.00</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="single_tab_content">
                                                    <figure class="effect-lexi">
                                                        <a href="#"><img src="/images/tab-image-four.png" alt=""></a>
                                                        <figcaption>
                                                            <a href="#" class="add_to_cart">Add to cart</a>
                                                            <p class="discount"></p>
                                                            <p class="favourite">
                                                                <a href="#"><i class="fa fa-refresh"></i></a>
                                                                <a href="#"><i class="fa fa-heart"></i></a>
                                                                <a href="#"><i class="fa fa-search"></i></a>
                                                            </p>
                                                        </figcaption>
                                                    </figure>
                                                    <div class="product_description">
                                                        <a href="#">Et harum quidem rerum dress</a>
                                                        <div class="star">
                                                            <a href="#">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </a>
                                                        </div>
                                                        <div class="price">
                                                            <p><span class="old_price">$409.00</span> <span
                                                                        class="new_price">$369.00</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="single_tab_content">
                                                    <figure class="effect-lexi">
                                                        <a href="#"><img src="/images/tab-image-three.png"
                                                                         alt=""></a>
                                                        <figcaption>
                                                            <a href="#" class="add_to_cart">Add to cart</a>
                                                            <p class="discount"></p>
                                                            <p class="favourite">
                                                                <a href="#"><i class="fa fa-refresh"></i></a>
                                                                <a href="#"><i class="fa fa-heart"></i></a>
                                                                <a href="#"><i class="fa fa-search"></i></a>
                                                            </p>
                                                        </figcaption>
                                                    </figure>
                                                    <div class="product_description">
                                                        <a href="#">Et harum quidem rerum dress</a>
                                                        <div class="star">
                                                            <a href="#">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </a>
                                                        </div>
                                                        <div class="price">
                                                            <p><span class="old_price">$409.00</span> <span
                                                                        class="new_price">$369.00</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="sales_tab">
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="single_tab_content">
                                                    <figure class="effect-lexi">
                                                        <a href="#"><img
                                                                    src="/images/tab-image-two-without-overlay.png"
                                                                    alt=""></a>
                                                        <figcaption>
                                                            <a href="#" class="add_to_cart">Add to cart</a>
                                                            <p class="discount"></p>
                                                            <p class="favourite">
                                                                <a href="#"><i class="fa fa-refresh"></i></a>
                                                                <a href="#"><i class="fa fa-heart"></i></a>
                                                                <a href="#"><i class="fa fa-search"></i></a>
                                                            </p>
                                                        </figcaption>
                                                    </figure>
                                                    <div class="product_description">
                                                        <a href="#">Et harum quidem rerum dress</a>
                                                        <div class="star">
                                                            <a href="#">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </a>
                                                        </div>
                                                        <div class="price">
                                                            <p><span class="old_price">$409.00</span> <span
                                                                        class="new_price">$369.00</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="single_tab_content">
                                                    <figure class="effect-lexi">
                                                        <a href="#"><img src="/images/tab-image-one.png" alt=""></a>
                                                        <figcaption>
                                                            <a href="#" class="add_to_cart">Add to cart</a>
                                                            <p class="discount"></p>
                                                            <p class="favourite">
                                                                <a href="#"><i class="fa fa-refresh"></i></a>
                                                                <a href="#"><i class="fa fa-heart"></i></a>
                                                                <a href="#"><i class="fa fa-search"></i></a>
                                                            </p>
                                                        </figcaption>
                                                    </figure>
                                                    <div class="product_description">
                                                        <a href="#">Et harum quidem rerum dress</a>
                                                        <div class="star">
                                                            <a href="#">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </a>
                                                        </div>
                                                        <div class="price">
                                                            <p><span class="old_price">$409.00</span> <span
                                                                        class="new_price">$369.00</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="single_tab_content">
                                                    <figure class="effect-lexi">
                                                        <a href="#"><img src="/images/tab-image-four.png" alt=""></a>
                                                        <figcaption>
                                                            <a href="#" class="add_to_cart">Add to cart</a>
                                                            <p class="discount"></p>
                                                            <p class="favourite">
                                                                <a href="#"><i class="fa fa-refresh"></i></a>
                                                                <a href="#"><i class="fa fa-heart"></i></a>
                                                                <a href="#"><i class="fa fa-search"></i></a>
                                                            </p>
                                                        </figcaption>
                                                    </figure>
                                                    <div class="product_description">
                                                        <a href="#">Et harum quidem rerum dress</a>
                                                        <div class="star">
                                                            <a href="#">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </a>
                                                        </div>
                                                        <div class="price">
                                                            <p><span class="old_price">$409.00</span> <span
                                                                        class="new_price">$369.00</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="single_tab_content">
                                                    <figure class="effect-lexi">
                                                        <a href="#"><img src="/images/tab-image-three.png"
                                                                         alt=""></a>
                                                        <figcaption>
                                                            <a href="#" class="add_to_cart">Add to cart</a>
                                                            <p class="discount"></p>
                                                            <p class="favourite">
                                                                <a href="#"><i class="fa fa-refresh"></i></a>
                                                                <a href="#"><i class="fa fa-heart"></i></a>
                                                                <a href="#"><i class="fa fa-search"></i></a>
                                                            </p>
                                                        </figcaption>
                                                    </figure>
                                                    <div class="product_description">
                                                        <a href="#">Et harum quidem rerum dress</a>
                                                        <div class="star">
                                                            <a href="#">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </a>
                                                        </div>
                                                        <div class="price">
                                                            <p><span class="old_price">$409.00</span> <span
                                                                        class="new_price">$369.00</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end new featured sales tab -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>