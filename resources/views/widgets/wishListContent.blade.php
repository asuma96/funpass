<section class="breadcrumb_main_area margin-bottom-50">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="breadcrumb_main">
                <h2>Wishlist</h2>
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Wishlist</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="shopping-cart-area">
    <div class="container">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tbody>
                    <tr class="table-title">
                        <td class="cart-img">Remove</td>
                        <td class="cart-img">Image</td>
                        <td class="cart-name">Product information</td>
                        <td class="cart-price">Receive news by email</td>
                        <td class="cart-price">Actions</td>
                    </tr>
                   @if(count($wishlist) == 0)
                        <tr>
                            <td class="text-center" colspan="5"><h4 class="p-md">Your wishlist are empty for now!</h4></td>
                        </tr>
                   @else
                   @foreach($wishlist as $item)
                            <tr class="table-product-info">
                                <td class="prod-del"><a class="trash-table" @click="MODAL_DEL('/api/v1/wishlist/detach/{{$item ->id}}?api_token={{ Auth::user()->api_token }}')"><i class="fa fa-trash"></i></a></td>
                                <td class="prod-img-center">
                                    <div class="prod-table-img-c">
                                        <img class="wishlistimg" alt="product-name" src="/storage/product/{{$item -> cover}}" >
                                    </div>
                                </td>
                                <td class="prod-name">
                                    <p><a><b>{{ $item->title }}</b></a></p>
                                    <p>Start Sell Date: <span>{{ $item->start_sell_date }}</span></p>
                                    <p>End Sell Date: <span>{{ $item->end_sell_date }}</span></p>
                                    <p>Min Age: <span>{{ $item->min_age }}</span></p>
                                </td>
                                <td class="prod-tot">
                                    <input type="checkbox"
                                           parse-int=""
                                           class="">
                                </td>
                                <td class="prod-tot">
                                    <p v-if="!productInCart({{$item -> id}})"  class="btn btn-default btn_reverse btn-block blue_button" @click="ADD_TO_CART({{$item ->id}})">Add to cart
                                    <i class="fa fa-puzzle-piece" aria-hidden="true"></i>
                                    </p>
                                    <a  v-if="productInCart({{$item -> id}})" class="btn btn-default btn_reverse btn-block blue_button" href="{{ URL::to('/shopping-cart')}}">In Cart
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </a>
                                    <a class="btn btn-default btn-block btn_reverse yellow_button" @click="ADD_TO_CART({{$item ->id}})" href="{{URL::to('/checkout')}}">Buy now
                                    <i class="fa fa-shopping-cart icon-toolbar" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                       @endforeach
                    @endif
                    <tr>
                        <td colspan="7">
                            <a href="{{URL::to('/home')}}" class="left_table_btn btn btn_reverse floatleft margin_class">continue shopping</a>
                            <a href="{{URL::to('/shopping-cart')}}" class="right_table_btn btn btn_reverse floatright margin_class">View shopping cart</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
