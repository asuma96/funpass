<section class="breadcrumb_main_area margin-bottom-50">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="breadcrumb_main">
                <h2>FAQ's</h2>
                <ul class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li><a href="/faq">FAQ's</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="welcome_area">
    <div class="container">
        <div class="row">
            <div class="welcome">
                <div class="row">
                    <div class="section_title">
                        <h4>Frequentley Asked Questions</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">HOW DO I ORDER MY TICKETS?</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>On our website you can find great prices on all of the areas attractions. Our user-friendly website will help you find, order and buy your tickets online. If you have any questions or need assistance then simply contact us via our Live Chat facility or e-mail us XXX @funpassusa.com alternatively you can call us at +1 407 513 4470 between 9 a.m. and 5 p.m., Mon – Sat.</p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">WE ARE STAYING AT A VACATION HOME CAN WE HAVE OUR TICKETS DELIVERED?</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Yes, for a small charge we can arrange to deliver your tickets directly to your door.</p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">HOW SOON BEFORE I ARRIVE DO I HAVE TO ORDER MY TICKETS?</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>If you choose our shipping option, we require at least XXX weeks to ship your order. If you choose to pick up your order, you can order your tickets at any time. Tickets are available for pickup at the Fun Pass USA ticket center, ideally located on West Highway 192 in Kissimmee.<a href="http://itchief.ru/lessons/bootstrap-3/website-creation-business-cards-(part-1)" target="_blank">уроке</a>, в котором рассмотрим процесс загрузки необходимых пакетов и проектирования макета сайта.</p>
                </div>
            </div>
        </div>
    </div>

</section>