<section class="breadcrumb_main_area margin-bottom-50">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="breadcrumb_main">
                <h2>Attractions</h2>
                <ul class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li><a href="#">Attractions</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>