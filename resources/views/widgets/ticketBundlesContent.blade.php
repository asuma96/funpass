<section class="breadcrumb_main_area margin-bottom-50">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="breadcrumb_main">
                <h2>TICKET BUNDLES</h2>
                <ul class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li><a href="#">TICKET BUNDLES</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- start category area -->
<div class="content_area">
    <div class="container">
        <div class="row">
            <div class="products_content">
                <div class="row">
                    @include('includes.categoriesList')
                    <div id="park-info" class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Walt Disney World Resort</h2>
                                <h4><i>Orlando</i></h4>
                                @include('includes.productsGallery')
                            </div>
                        </div>
                        <div class="products_in_category">
                            <!--Start Row View-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="product_item_details no_border_with_50_pad ticketChooser">
                                        <div role="tabpanel">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active">
                                                    <a href="#tickets" aria-controls="description" role="tab" data-toggle="tab">tickets</a>
                                                </li>
                                                <li>
                                                    <select class="form-control" name="" id="">
                                                        <option value="date_up">Date up</option>
                                                        <option value="date_down">Date down</option>
                                                        <option value="price_up">Price up</option>
                                                        <option value="price_down">Price down</option>
                                                        <option value="title_up">Title up</option>
                                                        <option value="title_down">Title down</option>
                                                    </select>
                                                </li>
                                            </ul>
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="tickets">
                                                    <div id="rowView">
                                                      {{--  @if(count($products) == 0)}--}}
                                                        <div class="row emptylistborder">
                                                            <h4 class="p-md" align="center">No products found by your request</h4>
                                                        </div>
                                                      {{--  @else
                                                            @foreach($products as $product)--}}
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="single_list_product">
                                                                            <div class="col-md-4 no-padding-left">
                                                                                <div class="single_list_image">
                                                                                    <div class="single_tab_content">
                                                                                        <figure class="effect-lexi">
                                                                                            <img src="/images/3a.jpg" alt="">
                                                                                        </figure>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8  no-padding-right">
                                                                                <div class="single_list_details">
                                                                                    <div class="product_description">
                                                                                        <span data-title="{{--$product ->id--}}"> </span>
                                                                                        <p>
                                                                                            Adult:
                                                                                            <span class="dollarIcon">$</span>
                                                                                            <span class="new_price">{{--$product -> price1--}}</span>
                                                                                            <br>Child:
                                                                                            <span class="dollarIcon">$</span>
                                                                                            <span class="new_price">{{--$product -> price2--}}</span>
                                                                                            <br>Infant:
                                                                                            <span class="dollarIcon">$</span>
                                                                                            <span class="new_price">{{--$product -> price3--}}</span>
                                                                                            <br>Min Age:
                                                                                            <span>{{--$product -> min_age--}}</span>
                                                                                            <br>Start Sale Date: <span>{{--$product -> atsrt_sell_date--}}</span>
                                                                                            <br>End Sale Date: <span>{{--$product -> end_sell_date--}}</span>
                                                                                            <br>
                                                                                            <b>Out of stock</b>
                                                                                            <br>
                                                                                            {{--@if($product -> tickets_count = 0)--}}
                                                                                                <b>Sold out</b><br>
                                                                                            {{--@endif--}}
                                                                                        </p>

                                                                                    </div>
                                                                                    <ul class="social_icons">
                                                                                        <li id="in{{--$product -> id--}}" class="display_none">
                                                                                            <a class="add_cart_btn inCart"  href="{{ URL::to('/shopping-cart')}}">In Cart</a></li>
                                                                                        <li><a class="add_cart_btn"
                                                                                               href="#">Buy now</a></li>
                                                                                        <li id={{--$product -> id--}}>
                                                                                            <a class="add_cart_btn addCart" @click="ADD_TO_CART({{$product ->id}})"
                                                                                               href="#">Add to cart</a></li>
                                                                                        <li><a class="add_cart_btn"  href="/wishlist/attach/{{$product ->id}}">Add to wishlist</a></li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                         {{--   @endforeach
                                                            {{ $products->links() }}
                                                        @endif--}}

                                                    </div>

                                                    <!--end row view-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End category area -->