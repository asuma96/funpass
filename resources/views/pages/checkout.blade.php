@extends('layouts.default')

@section('title', 'checkout')
@section('description', '')
@section('keywords', '')
@section('page', 'checkout')

@section('content')
    @include('widgets.checkoutContent')
@stop