@extends('layouts.default')

@section('title', 'wishlist')
@section('description', '')
@section('keywords', '')
@section('page', 'wishlist')
@section('content')

    @include('widgets.wishListContent')
@stop