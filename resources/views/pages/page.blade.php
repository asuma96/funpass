@extends('layouts.default')

@section('title')
    {{ $page->title}}
@stop
@section('description')
    {{ $page->meta_description}}
@stop
@section('keywords', '')
    {{ $page->meta_keywords}}
@stop
@section('page', 'list')

@section('content')

    {{ $page->body }}

@stop