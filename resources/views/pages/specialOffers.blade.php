@extends('layouts.default')

@section('title', 'special-offers')
@section('description', '')
@section('keywords', '')
@section('page', 'special-offers')
@section('content')

    @include('widgets.specialOffersContent')
@stop