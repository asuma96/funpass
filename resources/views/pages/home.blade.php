@extends('layouts.default')

@section('title', 'Home')
@section('description', '')
@section('keywords', '')
@section('page', 'home')

@section('content')

    @include('widgets.slider')
    @include('widgets.support')
    @include('widgets.products')
    @include('widgets.latest-posts')

@stop