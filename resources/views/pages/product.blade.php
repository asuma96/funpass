@extends('layouts.default')

@section('title', 'list')
@section('description', '')
@section('keywords', '')
@section('page', 'list')

@section('content')

{{--    @include('widgets.slider')--}}

    @include('widgets.products')

    @include('widgets.support')

    @include('widgets.latest-posts')

@stop