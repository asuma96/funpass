@extends('layouts.default')

@section('title', 'best-sellers')
@section('description', '')
@section('keywords', '')
@section('page', 'best-sellers')

@section('content')
    @include('widgets.bestSellersContent')
@stop