@extends('layouts.default')

@section('title', 'single-product')
@section('description', '')
@section('keywords', '')
@section('page', 'single-product')
@section('content')

    @include('widgets.singleProductContent')
@stop