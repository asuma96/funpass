@extends('layouts.default')

@section('title', 'Products')
@section('description', '')
@section('keywords', '')
@section('page', 'products')
@section('content')

    @include('widgets.productsContent')
@stop