<div class="row">
    <div class="col-md-12">
        <div class="single_list_product">
            <div class="col-md-4 no-padding-left">
                <div class="single_list_image">
                    <div class="single_tab_content">
                        <figure class="effect-lexi">
                            <a href="/single-product/{{ $product ->id }}">
                                <img src="/storage/product/{{$product -> cover}}" alt="">
                            </a>
                        </figure>
                    </div>
                </div>
            </div>
            <div class="col-md-8  no-padding-right">
                <div class="single_list_details">
                    <div class="product_description">
                                                                                <span data-title="{{$product ->id}}">
                                                                                    <h6>{{$product ->title}}</h6>
                                                                                </span>
                        <div class="product_info_block">
                            <div>Adult:
                                <span class="dollarIcon">$</span>
                                <span class="new_price">{{$product -> price1}}</span>
                            </div>
                            <div>Child:
                                <span class="dollarIcon">$</span>
                                <span class="new_price">{{$product -> price2}}</span>
                            </div>
                            <div>Infant:
                                <span class="dollarIcon">$</span>
                                <span class="new_price">{{$product -> price3}}</span>
                            </div>
                            <div>Min Age: {{$product -> min_age}}</div>
                            <div>End Sale Date:{{$product -> end_sell_date}}</div>
                            <b>Out of stock</b>
                            <div>
                                @if($product -> tickets_count = 0)
                                    <b>Sold out</b>
                                @endif
                            </div>
                        </div>
                    </div>
                    <ul class="social_icons">
                        <ul class="social_icons">
                            <li v-if="productInCart({{$product -> id}})">
                                <a class="add_cart_btn inCart  blue_button" href="{{ URL::to('/shopping-cart')}}">In Cart
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </a>
                            </li>

                            <li id={{$product -> id}} v-if="!productInCart({{$product -> id}})" >
                                <a class="add_cart_btn addCart  blue_button" @click="ADD_TO_CART({{$product ->id}})">Add to cart
                                <i class="fa fa-puzzle-piece" aria-hidden="true"></i>
                                </a>
                            </li>
                            <?php
                            $user = \Illuminate\Support\Facades\Auth::user()
                            ?>
                            @if($user)
                                <li>
                                    <a class="add_cart_btn blue_button" @CLICK="MODAL('/api/v1/wishlist/attach/{{$product ->id}}?api_token={{ Auth::user()->api_token }}')">
                                    Add to wishlist
                                    <i class="fa fa-heart-o icon-toolbar"></i>
                                    </a>
                                </li>
                            @endif
                            <li>
                                <a class="add_cart_btn yellow_button" @click="ADD_TO_CART({{$product ->id}})"
                                href="{{ URL::to('/checkout')}}">
                                <i class="fa fa-shopping-cart icon-toolbar" aria-hidden="true"></i>
                                Buy now
                                </a>
                            </li>
                        </ul>
                </div>
            </div>
        </div>
    </div>
</div>