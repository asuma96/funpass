<div class="header_bottom_area">
    <div class="container">
        <div class="row">
            <!-- LOGO -->
            <div class="logo">
                <a href="/"><img src="/images/logo.png" alt=""></a>
            </div>
            <!-- start menga menu -->
            <div class="nav_wrap">
                <nav class="no320">
                    <ul>
                        <li class="dropdown_mmenu">
                            <a href="#">Categories</a>
                            <div class="megamenu">

                                <div class="megamenu1">
                                    @foreach(\App\Http\Controllers\CategoryController::menus() as $item)
                                    <span>
                                        <a href="/product-category/{{  $item->id }} ">
                                            <img src="/storage/product/{{  $item->cover }}" class="img-responsive" alt="" />
                                            <p align="center">{{ $item->title }}</p>
                                        </a>
                                    </span>
                                    @endforeach
                                </div>
                            </div>
                        </li>
                        <li class="">
                            <a href="{{ URL::to('product-category/7')}}">Ticket Bundles</a>
                        </li>
                        <li class="">
                            <a href="{{ URL::to('best-sellers')}}">Best Sellers</a>
                        </li>
                        <li class="">
                            <a href="{{ URL::to('special-offers')}}">Special Offers</a>
                        </li>
                        <li class="dropdown_menu last_li">
                            <a href="javascript:void(0)">Help</a>
                            <div class="d_menu">
                                <span>
                                    <a  href="{{ URL::to('faq')}}">FAQ's</a>
                                </span>
                                <span>
                                    <a href="{{ URL::to('contact')}}">Contact Us</a>
                                </span>
                            </div>
                        </li>
                        <div class="clean"></div>
                     </ul>
                </nav>
            </div>
            <!-- end mega menu -->

        </div>

    </div>
</div>