<div class="product_item_details no_border_with_50_pad search-pages">
    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#images" aria-controls="review" role="tab" data-toggle="tab">images</a>
            </li>
            <li role="presentation">
                <a href="#description" aria-controls="description" role="tab" data-toggle="tab">description</a>
            </li>
            <li role="presentation">
                <a href="#videos" aria-controls="tags" role="tab" data-toggle="tab">videos</a>
            </li>
            <li role="presentation">
                <a href="#map" aria-controls="tags" role="tab" data-toggle="tab">map</a>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane" id="description">
                <p>
                    It’s the place where dreams come true. Imagine six fantastic Disney
                    Parks, two exciting night-time entertainment districts, and a
                    state-of-the-art sports complex. Now add in over 20 themed Resort
                    Hotels, plus countless opportunities for dining, shopping and
                    recreation, and it’s easy to see why Walt Disney World Resort is the
                    number one holiday destination in the world.<br/>
                    Whether you’re looking for beloved Disney Characters, thrilling
                    attractions, splashing good Water Parks or even a relaxing beach or
                    two, Walt Disney World has it all. Meet Mickey. Ride a magic carpet
                    through Agrabah. Get right in the middle of your favourite toons
                    with outrageous, in-your-face 3D. Plunge down a waterfall. Travel
                    through time, space and under the sea. Golf from dawn ‘til dusk on
                    pro courses. Water ski on private, pristine lakes. Shop ‘til you
                    drop. Be dazzled by spectacular, world-class entertainment. Savour
                    delectable dining at award-winning restaurants. The possibilities
                    are virtually endless.<br/>
                    And with so much to explore, there’s only one ticket that lets you
                    play Disney your way: Disney’s Ultimate Ticket. Exclusively designed
                    for UK guests visiting Walt Disney World Resort in Florida, this
                    ticket means you can come and go as you please to all six Parks and
                    more at one amazing price. It’s the only ticket you need.<br/>
                    With something for everyone, it’s no wonder Walt Disney World is the
                    holiday of your dreams. And come true they do – just like magic.
                    <br/>Terms and conditions apply to Disney's Ultimate Ticket.
                </p>
            </div>
            <div role="tabpanel" class="tab-pane active" id="images">
                <div id="slider" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="/images/disney1.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney2.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney3.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney4.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney5.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney6.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney7.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney8.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney9.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney10.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney11.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney11.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney13.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney14.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney15.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney16.jpg"/>
                        </li>
                        <!-- items mirrored twice, total of 12 -->
                    </ul>
                </div>
                <div id="carousel" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="/images/disney1.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney2.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney3.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney4.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney5.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney6.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney7.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney8.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney9.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney10.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney11.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney11.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney13.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney14.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney15.jpg"/>
                        </li>
                        <li>
                            <img src="/images/disney16.jpg"/>
                        </li>
                        <!-- items mirrored twice, total of 12 -->
                    </ul>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="videos">
                <p>
                    Quisque eget elit purus. Vivamus dictum rutrum ipsum, quist ornare
                    lorem iaculis nec. Sed volutpat tincdunt justo eget lob ortis.
                    Phasellus mos lestie rutrum lorem sit. Quisque eget elit purus.
                    Vivamus dictum rutrum ipsumi. Phasellus molestie rutrum lorem sit.
                    Quisque eget elit purus be Vivamus dictum rutrum ipsumi. Quisque
                    eget elit purus. Vivamus dictum rutrum ipsum, quist ornare lorem
                    iaculis nec. Sed volutpat tincdunt jus to eget lob ortis. Phasellus
                    molestie rutrum lorem sit. Quisque eget elit purus. Vivamus dictum
                    rutrum ipsumi.
                </p>

                <p>
                    Vivamus dictum rutrum ipsum, quist ornare lorem iaculis nec. Sed
                    volutpat tincdunt justo eget lob ortis. Phasellus mos lestie rutrum
                    lorem sit. Quisque eget elit purus. Vivamus dictum rutrum ipsumi.
                    Phasellus molestie rutrum lorem sit. Quisque eget elit purus be
                    Vivamus dictum rutru m ipsumi. Quisque eget elit purus. Vivamus
                    dictum rutrum ipsum, quist ornare lorem iaculis nec. Quisque eget
                    elit purus be Vivamus dictum rutru m ipsumi. Quisque eget elit
                    purus. Vivamus dictum rutrum ipsum, quist ornare lorem iaculis nec.
                </p>
            </div>
            <div role="tabpanel" class="tab-pane" id="map">
                <img src="/images/WDW_map.jpg"/>
            </div>
        </div>
    </div>
</div>