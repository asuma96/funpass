<div class="header_top_area">
    <div class="container">
        <div class="row">
            <div class="header_top">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="row">
                        <div class="left_header_top">
                            <ul>
                                @if($user)
                                    <li  class="logout"><a @click="LOGOUT()">Logout</a></li>
                                @else
                                    <li><a href="{{ URL::to('/login')}}">Login</a></li>
                                    <li><a href="{{ URL::to('/loginciirus')}}">Login with Ciirus</a></li>
                                @endif
                                    @if($user)
                                    <li class="myAccountList"><a href="#" >{{$user -> name}}<i class="fa fa-angle-down"></i></a>
                                       <img class="top_photo" src="/storage/avatar/{{$user->id}}/{{$user -> avatar}}" alt="" src="">
                                        <ul class="hide_account_list">
                                            <li><a href="/myaccount">My Account</a></li>
                                            <li><a href="/wishlist">Wishlist</a></li>
                                            <li><a href="/checkout">Check Out</a></li>
                                            <li><a href="/orders">Shopping history</a></li>
                                        </ul>
                                    </li>
                                    @endif
                                    <li><a href="#"><span>$</span> Usd</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 right_header_form floatright">
                    <div class="row">
                        <div class="right_header_top floatright">
                            <ul>
                                <search-component></search-component>
                                <cart-component></cart-component>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>