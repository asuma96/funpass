<div class="footer_top_area">
    <div class="container">
        <div class="row">
            <div class="footer_top">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="footer_top_widget fix">
                            <h4 class="special_border_right">About us</h4>
                            <p class="margin-tb-30">Are you planning a trip to Orlando, but need a quick and easy place to get all your attraction tickets? Then look no further - Fun Pass has it all from the magical Walt Disney World, to the exciting thrills at Busch Gardens, you are guranteed to get the best prices for all your attraction tickets. So stop waiting, and book your trip today!</p>
                            <ul class="social_icons">
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="footer_top_widget fix">
                            <h4 class="special_border_right">Latest tweet</h4>
                            <ul class="latest_tweet">
                                <li>
                                    <p class="margin-top-30"><i class="fa fa-twitter"></i>@Brittany_Spears Fun Pass is the best ticketing site out there for your Orlando vacation! Go get your tickets now!! 12 hour ago
                                        <span>12 hour ago</span>
                                    </p>
                                </li>
                                <li>
                                    <p class="margin-top-30"><i class="fa fa-twitter"></i>@Jone Doe Integer sit amet commodo eros, sed dictums ipsum. Integer sit ame commodo eros. 12 hour ago
                                        <span>12 hour ago</span>
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="footer_top_widget fix">
                            <h4 class="special_border_right">Newsletters</h4>
                            <p class="margin-top-30">Subscribe to Our Newsletter to get Important News.</p>
                            <input type="text">
                            <a href="#" class="btn">SUBSCRIBE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>