<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="@yield('keywords')">
<meta name="description" content="@yield('description')">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<link rel="icon" href="../images/favicon.ico" sizes="16x16">

<title>Funpass - @yield('title')</title>

<link href="/css/app.css" rel="stylesheet">