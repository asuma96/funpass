<div class="footer_copyright_area">
    <div class="container">
        <div class="row">
            <div class="footer_copyright">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="copyright">
                            <p>
                                Copyright © 2015 <a href="#">Hastech</a> - All Rights Reseved.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="footer_menu">
                            <nav class="footer_nav">
                                <ul>
                                    <li><a href="about-us.html">About Us</a></li>
                                    <li><a href="#">Check Out</a></li>
                                    <li><a href="#">My Account</a></li>
                                    <li><a href="#">Support</a></li>
                                    <li><a href="contact.html">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>