<footer>
    <!-- start footer slider -->
@include('includes.footerSlider')
    <!-- end footer slider -->
    <!-- start footer top area -->
@include('includes.footerTop')
    <!-- end footer top area -->
    <!-- start footer bottom area -->
@include('includes.footerBottom')
    <!-- end footer bottom area -->
    <!-- start footer copyright area -->
@include('includes.footerCopyrightInfo')
    <!-- end footer copyright area -->
</footer>