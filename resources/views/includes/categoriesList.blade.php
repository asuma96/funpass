<div class="col-lg-3 col-md-3 col-sm-5 col-xs-12">
    <div class="category_sidebar">
        <aside class="sidebar_widget">
            <div class="widget_title">
                <h4 class="special_border_right">CATEGORIES</h4>
            </div>
            <div class="sub_widget badge_widget">
                <ul>
                    @foreach(\App\Http\Controllers\CategoryController::listCategory() as $item)
                        <li><a href="/product-category/{{$item->id}}" id="{{$item->id}}">{{$item->title}}</a></li>
                    @endforeach
                </ul>
            </div>
        </aside>
        <aside class="sidebar_widget"></aside>
    </div>
</div>