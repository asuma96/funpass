<div class="footer_bottom_area">
    <div class="container">
        <div class="row">
            <div class="footer_bottom">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer_widget">
                            <div class="logo">
                                <a href="#"><img src="/images/logo.png" alt=""></a>
                            </div>
                            <ul class="special_widget">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    +1 (00) 86 868 868 666 <br>
                                    +1 (00) 42 868 666 888
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    admin@bootexperts.com<br>
                                    info@bootexperts.com
                                </li>
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    68 Dohava Stress, Lorem isput Spusts <br>
                                    New York- United State
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer_widget">
                            <h4>My account</h4>
                            <ul>
                                <li><a href="#"><i class="fa fa-angle-right"></i>My Account</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>About Us</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Contact</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Affiliates</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Meet The Maker</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer_widget">
                            <h4>Information</h4>
                            <ul>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Return & Exchanges</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Gift Cards</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Order Status</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Free Shipping</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Returns & Exchanges</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer_widget">
                            <h4>Opening time</h4>
                            <ul>
                                <li>
                                            <span class="floatleft"><i
                                                        class="fa fa-angle-right"></i>Monday - Friday</span>
                                    <span class="floatright">8.00 AM - 8.00 PM</span>
                                </li>
                                <li>
                                    <span class="floatleft"><i class="fa fa-angle-right"></i>Saturday</span>
                                    <span class="floatright">9.00 AM - 9.00 PM</span>
                                </li>
                                <li class="no_border">
                                    <span class="floatleft"><i class="fa fa-angle-right"></i>Sunday</span>
                                    <span class="floatright">10.00 AM - 6.00 PM</span>
                                </li>
                            </ul>
                            <div class="payment">
                                <a href="#"><img src="/images/visa.png" alt=""></a>
                                <a href="#"><img src="/images/mastercard.png" alt=""></a>
                                <a href="#"><img src="/images/discover.png" alt=""></a>
                                <a href="#"><img src="/images/american-express.png" alt=""></a>
                                <a href="#"><img src="/images/paypal.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>