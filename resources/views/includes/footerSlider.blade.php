<div class="footer_slider_area">
    <div class="container">
        <div class="row">
            <div class="footer_slider">
                <div class="testimonial">
                    <div class="footer_top_slider owl-carousel">
                        <div class="item">
                            <a href="#"><img src="/images/our-logo-one.png" alt=""></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="/images/our-logo-two.png" alt=""></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="/images/our-logo-three.png" alt=""></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="/images/our-logo-four.png" alt=""></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="/images/our-logo-five.png" alt=""></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="/images/our-logo-six.png" alt=""></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="/images/our-logo-one.png" alt=""></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="/images/our-logo-two.png" alt=""></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="/images/our-logo-three.png" alt=""></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="/images/our-logo-four.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>