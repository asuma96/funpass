<header class="header_area">
    <!-- start header top area -->
@include('includes.headerToolbar')
    <!-- end header top area -->
    <!-- start header bottom area -->
@include('includes.headerMenubar')
    <!-- end header bottom area -->
</header>