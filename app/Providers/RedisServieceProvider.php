<?php

namespace App\Providers;

use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\ServiceProvider;

class RedisServieceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!Redis::get('menus')) {
            $menus = CategoryController::menus();
            Redis::set('menus', json_encode($menus));
        }
        if (!Redis::get('top')) {
            $top = CategoryController::listCategory();
            Redis::set('top', json_encode($top));
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
