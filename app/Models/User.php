<?php

namespace App;

use App\Http\Controllers\Traits\SearchableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, SearchableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'api_token',
        'active',
        'marketing_subscibed',
        'system_subscribed',
        'avatar',
        'sex',
        'phone',
        'first_name',
        'last_name',
        'birthday',
        'mc_user_id',
        'guest_user_id',
        'ciirus_token',
        'profile_id',
        'payment_profile_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function wishlist()
    {
        return $this->belongsToMany(Product::class, 'wishlists');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function scopeAuth($query)
    {
        return $query->where('id', Auth::user()->id);
    }

    public function scopeMarketingSubscribed($query, int $val = 1)
    {
        return $query->where('marketing_subscribed', $val);
    }
}
