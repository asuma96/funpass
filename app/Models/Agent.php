<?php

namespace App;

use App\Http\Controllers\Traits\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agent extends Model
{
    use SearchableTrait, SoftDeletes;

    protected $fillable = [
        'name',
        'information',
        'key',
        'type',
        'formula'
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
