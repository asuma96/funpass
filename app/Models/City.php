<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'title',
        'state_id',
    ];

    public function state()
    {
        return $this->belongsTo(State::class);
    }
}
