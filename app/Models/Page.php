<?php

namespace App;

use App\Http\Controllers\Traits\SearchableTrait;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use SearchableTrait;

    protected $fillable = [
        'meta_description',
        'meta_keywords',
        'title',
        'body',
        'url',
    ];
}
