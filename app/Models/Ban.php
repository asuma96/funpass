<?php

namespace App;

use App\Http\Controllers\Traits\SearchableTrait;
use Illuminate\Database\Eloquent\Model;

class Ban extends Model
{
    use SearchableTrait;

    protected $fillable = [
        'user_id',
        'ip',
        'reason'
    ];

    public function user()
    {
        return $this->hasOne(User::class);
    }
}
