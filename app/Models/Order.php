<?php

namespace App;

use App\Http\Controllers\Traits\SearchableTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SearchableTrait, SoftDeletes;

    protected $fillable = [
        'user_id',
        'agent_id',
        'referer_id',
        'auth_code',
        'amount',
        'hash',
        'trans_id',
        'status',
        'profile_id',
        'payment_profile_id'
    ];

    public function items()
    {
        return $this->belongsToMany(Product::class, 'order_product');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function logs()
    {
        return $this->hasMany(OrderLog::class);
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }

    public function scopeSearchByHash(Builder $query, string $hash)
    {
        return $query->selectRaw('id,amount,status')->where('hash', $hash)
            ->with([
                'items' => function ($query) {
                    $query->selectRaw('id,title,cover')
                            ->withPivot(
                                'quantity1',
                                'quantity2',
                                'quantity3',
                                'arrive_date',
                                'shipping_method',
                                'actual_shipping_method',
                                'p1_d',
                                'p2_d',
                                'p3_d',
                                'total'
                            );
                }
            ])->get()->first();
    }

    public function scopeSelfHistory(Builder $query, int $user)
    {
        return $query->where('user_id', $user)
            ->with([
                'items' => function ($query) {
                    $query->withPivot(
                        'quantity1',
                        'quantity2',
                        'quantity3',
                        'arrive_date',
                        'shipping_method',
                        'actual_shipping_method',
                        'p1_d',
                        'p2_d',
                        'p3_d',
                        'total'
                    );
                }
            ])->get();
    }
}
