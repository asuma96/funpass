<?php

namespace App;

use App\Http\Controllers\Traits\SearchableTrait;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use SearchableTrait;

    protected $fillable = [
        'file_name',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_image');
    }
}
