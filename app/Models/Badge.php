<?php

namespace App;

use App\Http\Controllers\Traits\SearchableTrait;
use Illuminate\Database\Eloquent\Model;

class Badge extends Model
{
    use SearchableTrait;

    protected $fillable = ['name', 'html'];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_badge');
    }
}
