<?php

namespace App;

use App\Http\Controllers\Traits\SearchableTrait;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use SearchableTrait;

    protected $fillable = ['type', 'title'];

    protected $with = ['products'];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'ticket_product')->withPivot(['quantity1','quantity2','quantity3']);
    }
}
