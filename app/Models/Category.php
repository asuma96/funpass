<?php

namespace App;

use App\Http\Controllers\Traits\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes, SearchableTrait;

    protected $fillable = [
        'title',
        'description',
        'cover',
        'show_in_menu',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_category');
    }

    public function scopeMenus($query)
    {
        return $query->where('show_in_menu', 1)->limit(5);
    }
}
