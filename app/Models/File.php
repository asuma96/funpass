<?php

namespace App;

use App\Http\Controllers\Traits\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{
    use SearchableTrait, SoftDeletes;

    protected $fillable = ['name', 'size', 'hash', 'type'];
}
