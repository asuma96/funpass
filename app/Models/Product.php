<?php

namespace App;

use App\Http\Controllers\Traits\SearchableTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes, SearchableTrait;

    protected $fillable = [
        'price1',
        'price2',
        'price3',
        'start_sell_date',
        'end_sell_date',
        'start_show_on_web_site_date',
        'end_show_on_web_site_date',
        'cover',
        'title',
        'address',
        'electronic_ticket',
        'ship_to_home_ticket',
        'pickup_at_center_ticket',
        'commission1',
        'commission2',
        'commission3',
        'discount1',
        'discount2',
        'discount3',
        'sub_total',
        'total',
        'special_offer',
        'min_age',
        'ticket_count',
        'default_shiping_method',
        'item_description',
        'item_term',
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_category');
    }

    public function images()
    {
        return $this->belongsToMany(Image::class, 'product_image');
    }

    public function keywords()
    {
        return $this->belongsToMany(Keyword::class, 'product_keyword');
    }

    public function wishlistUsers()
    {
        return $this->belongsToMany(User::class, 'wishlists');
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_product');
    }

    public function badges()
    {
        return $this->belongsToMany(Badge::class, 'product_badge');
    }

    public function tickets()
    {
        return $this->belongsToMany(Ticket::class, 'ticket_product');
    }

    public function scopeOnList($query)
    {
        $query->selectRaw('id,cover,title,price1,price2,price3,start_sell_date,end_sell_date');
        return $this->scopeVisibility($query);
    }

    public function scopeSpecialOffers($query)
    {
        return $this->scopeOnList($query)->where('special_offer', 1);
    }

    public function scopeVisibility($query)
    {
        return $query->where('start_show_on_web_site_date', '<=', Carbon::now())
        ->where('end_show_on_web_site_date', '>=', Carbon::now());
    }
}
