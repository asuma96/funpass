<?php

namespace App;

use App\Http\Controllers\Traits\SearchableTrait;
use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    use SearchableTrait;

    protected $fillable = [
        'title'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_keyword');
    }
}
