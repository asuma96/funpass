<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminAndModerAccess
{
    public function handle($request, Closure $next)
    {
        $user = Auth::guard('api')->user();
        if (in_array($user->role, ['admin', 'moderator'])) {
            return $next($request);
        }
        return response()->json(['message' => 'You are not admin or moder'], 403);
    }
}
