<?php

namespace App\Http\Middleware;

use App\Ban;
use Closure;

class isUserWasBanned
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ban = Ban::query()->where('ip', 'like', $_SERVER['REMOTE_ADDR']);
        if ($ban->count() > 0) {
            abort(403, $ban->get()->first()->reason);
        }
        return $next($request);
    }
}
