<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserSelfUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'min:3|max:20',
            'phone' => 'string|min:10',
            'first_name' => 'string|min:3',
            'last_name' => 'string|min:3',
            'sex' => 'string',
            'birthday' => 'date',
            'marketing_unsubscribed' => 'boolean',
            'system_unsubscribed' => 'boolean',
        ];
    }
}
