<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'min:3|max:20',
            'email' => 'email|max:255|unique:users',
            'password' => 'string|min:6',
            'active' => 'boolean',
            'role' => 'string',
            'marketing_subscribed' => 'boolean',
            'system_subscribed' => 'boolean',
            'avatar' => 'string',
            'sex' => 'string|min:3|max:5',
            'phone' => 'string|max:15',
            'first_name' => 'string|min:3|max:60',
            'last_name' => 'string|min:3|max:150',
            'birthday' => 'date',
            'marketing_unsubscribed' => 'boolean',
            'system_unsubscribed' => 'boolean',
        ];
    }
}
