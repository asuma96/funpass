<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'meta_description' => 'string|min:3',
            'meta_keywords' => 'string|min:3|max:200',
            'title' => 'string|min:3|max:200',
            'body' => 'string',
            'url' => 'unique:pages',
        ];
    }
}
