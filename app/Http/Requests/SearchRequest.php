<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'count' => 'integer',
            'page' => 'integer',
            'search' => 'string',
            'searchBy' => 'string',
            'order' => 'string|min:3|max:4',
            'orderBy' => 'string',
            'raw' => 'string',
            'with' => 'string',
        ];
    }
}
