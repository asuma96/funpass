<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price1' => 'numeric',
            'price2' => 'numeric',
            'price3' => 'numeric',
            'start_sell_date' => 'date',
            'end_sell_date' => 'date',
            'start_show_on_web_site_date' => 'date',
            'end_show_on_web_site_date' => 'date',
            'cover' => 'string',
            'title' => 'string',
            'address' => 'string',
            'electronic_ticket' => 'boolean',
            'ship_to_home_ticket' => 'boolean',
            'pickup_at_center_ticket' => 'boolean',
            'commission' => 'string',
            'discount' => 'string',
            'special_offer' => 'boolean',
            'min_age' => 'integer',
            'ticket_count' => 'integer',
            'item_description' => 'string',
            'item_term' => 'string',
        ];
    }
}
