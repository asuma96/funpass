<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\PageRequest;
use App\Http\Requests\SearchRequest;
use App\Page;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    use SoftDeletes;

    public function __construct()
    {
        $this->middleware('adminAndModer');
    }

    /**
     * Method show list pages with paginate, use param for searchable
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(SearchRequest $request)
    {
        return response()->json(Page::query()->searchable($request)->paginate($request->count));
    }

    /**
     * Method show admin all page data
     *
     * @param Page $page
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(Page $page)
    {
        return response()->json($page);
    }

    /**
     * Store page data to database
     *
     * @param PageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(PageRequest $request)
    {
        return response()->json(Page::create($request->all()));
    }

    /**
     * Update page data find by id
     *
     * @param PageRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(PageRequest $request, int $id)
    {
        return response()->json(Page::query()->whereId($id)->update($request->except('api_token')));
    }

    /**
     * Delete page find by id
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy(int $id)
    {
        return response()->json(Page::query()->whereId($id)->delete());
    }


    /**
     * Show create modal for add this entity
     */

    public function create()
    {

    }

    /**
     * Show edit modal for update this entity where id = param
     *
     * @param $id
     */
    public function edit($id)
    {

    }
}
