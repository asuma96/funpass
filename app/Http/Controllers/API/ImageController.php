<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\ImageRequest;
use App\Http\Requests\SearchRequest;
use App\Image;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminAndModer')->except(['upload']);
    }

    public static function upload(ImageRequest $request, string $folderSrc = '')
    {
        $img = $request->file('file')->storePublicly($folderSrc);
        return str_replace($folderSrc. '/', '', $img);
    }

    /**
     * Show list images
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(SearchRequest $request)
    {
        return response()->json(Image::query()->searchable($request)->paginate($request->count));
    }

    /**
     * Store image to database
     *
     * @param ImageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(ImageRequest $request)
    {
        $img = ImageController::upload($request, 'public/product');
        return response()->json(Image::create(['file_name' => $img]));
    }

    /**
     * Show image find by id
     *
     * @param SearchRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(SearchRequest $request, int $id)
    {
        return response()->json(
            Image::query()->whereId($id)
                ->withRaw($request->raw)->include($request->with)->get()->first()
        );
    }

    /**
     * Update image
     *
     * @param ImageRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(ImageRequest $request, int $id)
    {
        return response()->json(Image::query()->whereId($id)->update($request->except('api_token')));
    }

    /**
     * Delete image, find by id
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id)
    {
        return response()->json(Image::query()->whereId($id)->delete());
    }

    /**
     * Show create modal for add this entity
     */

    public function create()
    {

    }

    /**
     * Show edit modal for update this entity where id = param
     *
     * @param $id
     */
    public function edit($id)
    {

    }
}
