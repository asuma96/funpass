<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AutorizeNetController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\OrderTrait;
use App\Http\Requests\SearchRequest;
use App\Order;
use App\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    use OrderTrait;

    public function __construct()
    {
        $this->middleware('adminAndModer')->only([
            'refund',
            'index',
            'show',
            'search',
            'update',
            'destroy',
            'payAsUser',
        ]);
    }

    /**
     * Pay order and add to database
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $order = $this->buy($request->user(), $request->all());
        if (!isset($order['error'])) {
            return response()->json($order);
        }
        return response()->json(['message' => $order['error']]);
    }

    /**
     * Pay order as isset user and add to database
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function payAsUser(Request $request, string $api_token)
    {
        $user = User::query()->where('api_token', $api_token)->get()->first();
        $order = $this->buy($user, $request->all());
        if (!isset($order['error'])) {
            return response()->json($order);
        }
        return response()->json(['message' => $order['error']]);
    }

    /**
     * Show order history for auth user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function history(Request $request)
    {
        return response()->json(Order::selfHistory($request->user()->id));
    }

    /**
     * Refund order
     *
     * @param int $idOrder
     * @return boolean
     */
    protected function refund(Request $request, int $idOrder)
    {
        if ($request->user()->role != 'admin') {
            return response()->json(false);
        }
        $order = Order::query()->where('id', $idOrder)->with('user')->get()->first();
        $refund = AutorizeNetController::refundTransaction(
            $request->amount,
            $order->profile_id,
            $order->payment_profile_id
        );
        if ($refund == false) {
            return response()->json(false);
        }

        if ($order->update(['status' => 'Order has been refund'])) {
            $order->logs()->create([
                'user_id' => $request->user()->id,
                'data' => json_encode(
                    [
                    'status' => 'Order has been refund',
                    'amount' => $request->amount,
                    'profile_id' => $order->profile_id,
                    'payment_profile_id' => $order->payment_profile_id
                    ]
                )
            ]);
            return response()->json(true);
        }
        return response()->json(false);
    }

    /**
     * Method show list order with paginate, use param for searchable
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(SearchRequest $request)
    {
        return response()->json(
            Order::query()->search($request->search, $request->searchBy)->with([
            'user',
            'items' => function ($query) {
                $query->withPivot(
                    'quantity1',
                    'quantity2',
                    'quantity3',
                    'arrive_date',
                    'shipping_method',
                    'actual_shipping_method',
                    'tracking_code',
                    'p1',
                    'p2',
                    'p3',
                    'p1_c',
                    'p2_c',
                    'p3_c',
                    'p1_d',
                    'p2_d',
                    'p3_d',
                    'subtotal',
                    'total'
                );
            }
            ])
            ->orWhereHas('user', function ($query) use ($request) {
                $query->search($request->search, $request->searchBy);
            })
            ->orWhereHas('items', function ($query) use ($request) {
                $query->search($request->search, $request->searchBy);
            })
            ->orderBy($request->orderBy ?? 'id', $request->order ?? 'asc')->paginate($request->count ?? 10)
        );
    }

    /**
     * Method show product use param for include relation and select raw, who need
     *
     * @param SearchRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(SearchRequest $request, int $id)
    {
        return response()->json(
            Order::query()->whereId($id)
                ->with([
                    'items' => function ($query) {
                        $query->withPivot(
                            'quantity1',
                            'quantity2',
                            'quantity3',
                            'arrive_date',
                            'shipping_method',
                            'actual_shipping_method',
                            'tracking_code',
                            'p1',
                            'p2',
                            'p3',
                            'p1_c',
                            'p2_c',
                            'p3_c',
                            'p1_d',
                            'p2_d',
                            'p3_d',
                            'subtotal',
                            'total'
                        );
                    }
                ])
            ->include($request->with)->get()->first()
        );
    }

    /**
     * Update order data
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, int $id)
    {
        $order = Order::query()->whereId($id)->get()->first();
        $order->update($request->except(['api_token', 'pivot']));
        $order->items()->updateExistingPivot($request->pivot['product_id'], $request->pivot['data']);
        $order->logs()->create([
            'user_id' => $request->user()->id,
            'data' => json_encode($request->except('api_token'))
        ]);
        return response()->json($order);
    }

    /**
     * Soft delete order
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy(Request $request, int $id)
    {
        Order::query()->whereId($id)->delete();
        Order::query()->whereId($id)->get()->first()->logs()->create([
            'user_id' => $request->user()->id,
            'data' => json_encode(['status' => 'Order has been delete'])
        ]);
        return response()->json(true);
    }


    /**
     * Show create modal for add this entity
     */

    public function create()
    {

    }

    /**
     * Show edit modal for update this entity where id = param
     *
     * @param $id
     */
    public function edit($id)
    {

    }
}
