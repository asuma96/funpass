<?php

namespace App\Http\Controllers\API;

use App\Ban;
use App\Http\Controllers\Controller;
use App\Http\Requests\BanRequest;
use App\Http\Requests\SearchRequest;

class BanController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminAndModer');
    }

    /**
     * Show all banned user
     *
     * @param SearchRequest $request
     * @return mixed
     */
    protected function index(SearchRequest $request)
    {
        return response()->json(Ban::query()->searchable($request)->paginate($request->count));
    }


    /**
     * Show info user, who was banned
     *
     * @param SearchRequest $request
     * @param $id
     * @return mixed
     */
    public function show(SearchRequest $request, $id)
    {
        return response()->json(
            Ban::query()->whereId($id)->withRaw($request->raw)->include($request->with)->get()->first()
        );
    }

    /**
     * Ban user!
     *
     * @param BanRequest $request
     * @return static
     */
    protected function store(BanRequest $request)
    {
        return response()->json(Ban::create($request->all()));
    }

    /**
     * Update ban info
     *
     * @param BanRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(BanRequest $request, int $id)
    {
        return response()->json(Ban::query()->whereId($id)->update($request->except('api_token')));
    }

    /**
     * Remove user from ban table
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy(int $id)
    {
        return response()->json(Ban::query()->whereId($id)->delete());
    }

    /**
     * Show create modal for add this entity
     */

    public function create()
    {

    }

    /**
     * Show edit modal for update this entity where id = param
     *
     * @param $id
     */
    public function edit($id)
    {

    }
}
