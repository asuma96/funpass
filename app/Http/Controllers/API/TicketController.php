<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\SearchRequest;
use App\Http\Requests\TicketRequest;
use App\Ticket;
use App\Http\Controllers\Controller;

class TicketController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminAndModer');
    }

    /**
     * Display a listing of the tickets.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SearchRequest $request)
    {
        return response()->json(Ticket::query()->searchable($request)->paginate($request->count));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TicketRequest $request)
    {
        $ticket = Ticket::create($request->except(['pivot', 'api_token']));
        foreach ($request->pivot as $item => $value) {
            $ticket->products()->attach([$item => $value]);
        }
        return response()->json($ticket);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return response()->json(Ticket::query()->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  TicketRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TicketRequest $request, int $id)
    {
        $ticket = Ticket::query()->where('id', $id)->update($request->except(['pivot', 'api_token']));
        foreach ($request->pivot as $item => $value) {
            $ticket->products()->updateExistingPivot($item, $value);
        }
        return response()->json($ticket);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        return response()->json(Ticket::query()->where('id', $id)->delete());
    }
}
