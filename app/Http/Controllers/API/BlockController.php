<?php

namespace App\Http\Controllers\API;

use App\Block;
use App\Http\Requests\BlockRequest;
use App\Http\Requests\SearchRequest;
use App\Http\Controllers\Controller;

class BlockController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminAndModer')->only([
            'index',
            'show',
            'store',
            'update',
            'destroy',
        ]);
    }

    /**
     * Method show list block with paginate, use param for searchable
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(SearchRequest $request)
    {
        return response()->json(Block::query()->searchable($request)->paginate($request->count));
    }

    /**
     * Method show all block data
     *
     * @param Block $block
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(Block $block)
    {
        return response()->json($block);
    }

    /**
     * Store new block to database
     *
     * @param BlockRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(BlockRequest $request)
    {
        return response()->json(Block::create($request->all()));
    }

    /**
     * Update any block data
     *
     * @param BlockRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(BlockRequest $request, int $id)
    {
        return response()->json(Block::query()->whereId($id)->update($request->except('api_token')));
    }

    /**
     * Delete block use id
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy(int $id)
    {
        return response()->json(Block::query()->whereId($id)->delete());
    }


    /**
     * Show create modal for add this entity
     */

    public function create()
    {

    }

    /**
     * Show edit modal for update this entity where id = param
     *
     * @param $id
     */
    public function edit($id)
    {

    }
}
