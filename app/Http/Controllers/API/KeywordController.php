<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\KeywordRequest;
use App\Http\Requests\SearchRequest;
use App\Keyword;
use App\Http\Controllers\Controller;

class KeywordController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminAndModer');
    }

    /**
     * Method show list keyword with paginate, use param for searchable
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(SearchRequest $request)
    {
        return response()->json(Keyword::query()->searchable($request)->paginate($request->count));
    }

    /**
     * Method show keyword by id
     *
     * @param Keyword $keyword
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(Keyword $keyword)
    {
        return response()->json($keyword);
    }

    /**
     * Add new keyword to database use request param
     *
     * @param KeywordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(KeywordRequest $request)
    {
        return response()->json(Keyword::create($request->all()));
    }

    /**
     * Find keyword by id and update all fields, contains in request
     *
     * @param KeywordRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(KeywordRequest $request, int $id)
    {
        return response()->json(Keyword::query()->whereId($id)->update($request->except('api_token')));
    }

    /**
     * Find keyword by id and delete
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy(int $id)
    {
        return response()->json(Keyword::query()->whereId($id)->delete());
    }


    /**
     * Show create modal for add this entity
     */

    public function create()
    {

    }

    /**
     * Show edit modal for update this entity where id = param
     *
     * @param $id
     */
    public function edit($id)
    {

    }
}
