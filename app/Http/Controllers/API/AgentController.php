<?php

namespace App\Http\Controllers\API;

use App\Agent;
use App\Http\Requests\AgentRequest;
use App\Http\Requests\SearchRequest;
use App\Http\Controllers\Controller;

class AgentController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminAndModer');
    }

    /**
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(SearchRequest $request)
    {
        return response()->json(Agent::query()->searchable($request)->paginate($request->count));
    }

    /**
     * @param Agent $agent
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(Agent $agent)
    {
        return response()->json($agent);
    }

    /**
     * @param AgentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(AgentRequest $request)
    {
        return response()->json(Agent::create($request->all()));
    }

    /**
     * @param AgentRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(AgentRequest $request, int $id)
    {
        return response()->json(Agent::query()->whereId($id)->update($request->except('api_token')));
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy(int $id)
    {
        return response()->json(Agent::query()->whereId($id)->delete());
    }

    /**
     * Show create modal for add this entity
     */

    public function create()
    {

    }

    /**
     * Show edit modal for update this entity where id = param
     *
     * @param $id
     */
    public function edit($id)
    {

    }
}
