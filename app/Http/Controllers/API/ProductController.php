<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\ProductTrait;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\SyncRequest;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use ProductTrait;

    public function __construct()
    {
        $this->middleware('adminAndModer')->only([
            'store',
            'update',
            'destroy',
            'sync',
            'upload',
        ]);
    }

    /**
     * Method show list products with paginate, use param for searchable
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(SearchRequest $request)
    {
        return response()->json(Product::query()->searchable($request)->paginate($request->count));
    }

    /**
     * Add new product to database use request param
     *
     * @param ProductRequest $request
     * @return static
     */
    public function store(ProductRequest $request)
    {
        return response()->json(Product::create($request->all()));
    }

    /**
     * Method show product use param for include relation and select raw, who need
     *
     * @param SearchRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(SearchRequest $request, int $id)
    {
        return response()->json(
            Product::query()->whereId($id)
            ->withRaw($request->raw)->include($request->with)->get()->first()
        );
    }

    /**
     * Find product by id and update all fields, contains in request
     *
     * @param ProductRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ProductRequest $request, int $id)
    {
        return response()->json(Product::query()->whereId($id)->update($request->except('api_token')));
    }

    /**
     * Find product by id and delete (soft delete)
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id)
    {
        return response()->json(Product::query()->whereId($id)->delete());
    }

    /**
     * Find product by id and attach or detach with table relation
     *
     * @param SyncRequest $request
     * @param Product $product
     * @param string $relation
     * @param string $action
     * @return \Illuminate\Http\JsonResponse
     */
    public function sync(SyncRequest $request, Product $product, string $relation, string $action = 'sync')
    {
        return response()->json($product->$relation()->$action($request->ids));
    }

    /**
     * Show create modal for add this entity
     */

    public function create()
    {

    }

    /**
     * Show edit modal for update this entity where id = param
     *
     * @param $id
     */
    public function edit($id)
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function upload(ImageRequest $request)
    {
        return response()->json(ImageController::upload($request, 'public/products'));
    }
}
