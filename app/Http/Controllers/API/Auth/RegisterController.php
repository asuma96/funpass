<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Auth\RegisterController as Register;
use Illuminate\Http\Request;

class RegisterController extends Register
{
    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $this->create($request->all());
        return LoginController::login($request);
    }
}
