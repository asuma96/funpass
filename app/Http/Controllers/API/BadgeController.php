<?php

namespace App\Http\Controllers\API;

use App\Badge;
use App\Http\Controllers\Controller;
use App\Http\Requests\BadgeRequest;
use App\Http\Requests\SearchRequest;

class BadgeController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminAndModer');
    }

    /**
     * Method show full list badges
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(SearchRequest $request)
    {
        return response()->json(Badge::query()->searchable($request)->paginate($request->count));
    }

    /**
     * Method show one badge
     *
     * @param Badge $badge
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(Badge $badge)
    {
        return response()->json($badge);
    }

    /**
     * Method store to database all info
     *
     * @param BadgeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(BadgeRequest $request)
    {
        return response()->json(Badge::create($request->all()));
    }

    /**
     * Method find badge by id and update him
     *
     * @param BadgeRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(BadgeRequest $request, int $id)
    {
        return response()->json(Badge::query()->whereId($id)->update($request->except('api_token')));
    }

    /**
     * Method search badge by id and delete him
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy(int $id)
    {
        return response()->json(Badge::query()->whereId($id)->delete());
    }

    /**
     * Show create modal for add this entity
     */

    public function create()
    {

    }

    /**
     * Show edit modal for update this entity where id = param
     *
     * @param $id
     */
    public function edit($id)
    {

    }
}
