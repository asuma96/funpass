<?php

namespace App\Http\Controllers\API;

use App\File;
use App\Http\Requests\FileRequest;
use App\Http\Requests\FileUpdateRequest;
use App\Http\Requests\SearchRequest;
use App\Http\Controllers\Controller;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminAndModer');
    }

    /**
     * Method show list files with paginate, use param for searchable
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(SearchRequest $request)
    {
        return response()->json(File::query()->searchable($request)->paginate($request->count));
    }

    /**
     * Method show all file data
     *
     * @param File $file
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(File $file)
    {
        return response()->json($file);
    }

    /**
     * Get info about file and store needed info to database
     *
     * @param FileRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(FileRequest $request)
    {
        $file['size'] = $request->file('file')->getSize();
        $file['name'] = $request->file('file')->getClientOriginalName();
        $file['type'] = $request->file('file')->getMimeType();
        $file['hash'] = str_replace('public/files/', '', $request->file('file')->storePublicly('public/files'));
        return response()->json(File::create($file));
    }

    /**
     * Update file name
     *
     * @param FileUpdateRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(FileUpdateRequest $request, int $id)
    {
        return response()->json(File::query()->whereId($id)->update($request->except('api_token')));
    }

    /**
     * Delete file, find by id
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(File $file)
    {
        return response()->json($file->delete());
    }


    /**
     * Show create modal for add this entity
     */

    public function create()
    {

    }

    /**
     * Show edit modal for update this entity where id = param
     *
     * @param $id
     */
    public function edit($id)
    {

    }
}
