 <?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\UserTrait;
use App\Http\Requests\AddressRequest;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\NewsletterRequest;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserSelfPasswordUpdateRequest;
use App\Http\Requests\UserSelfUpdateRequest;
use App\Notifications\ReceivedUsersEmail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;

class UserController extends Controller
{
    use UserTrait, Notifiable;

    public function __construct()
    {
        $this->middleware('adminAndModer')->only([
            'index',
            'show',
            'store',
            'update',
            'destroy',
            'loginAsUser',
            'receiversUserSendEmail',
            'newsletter',
            'upload',
        ]);
    }

    /**
     * Return auth user data
     *
     * Method for widget
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function myAccount(Request $request)
    {
        return response()->json($request->user());
    }

    /**
     * Update auth user data
     *
     * Method for widget
     *
     * @param UserSelfUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function updateSelf(UserSelfUpdateRequest $request)
    {
        return response()->json($this->updateSelfInformation($request->except('api_token'), $request->user()));
    }

    /**
     * Update auth user password
     *
     * Method for widget
     *
     * @param UserSelfPasswordUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function changePassword(UserSelfPasswordUpdateRequest $request)
    {
        return response()->json($this->updateSelfInformation($request->except('api_token'), $request->user()));
    }

    /**
     * Add product to wishlist auth user
     *
     * @param Request $request
     * @param int $productId
     * @return \Illuminate\Http\JsonResponse
     */
    protected function attachToWishlist(Request $request, int $productId)
    {
        if ($request->user()->wishlist()->where('product_id', $productId)->count() == 0) {
            $request->user()->wishlist()->attach([$productId => ['receive_email' => 1]]);
            return response()->json(true);
        }
        return response()->json(false);
    }

    /**
     * Remove product from wishlist
     *
     * @param Request $request
     * @param int $productId
     * @return \Illuminate\Http\JsonResponse
     */
    protected function detachFromWishlist(Request $request, int $productId)
    {
        if ($request->user()->wishlist()->where('product_id', $productId)->count() != 0) {
            $request->user()->wishlist()->detach($productId);
            return response()->json(true);
        }
        return response()->json(false);
    }

    /**
     * Get all product it wishlist
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getWishlists(Request $request)
    {
        return response()->json($this->wishlist($request->user()));
    }

    /**
     * Method show list users with paginate, use param for searchable
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(SearchRequest $request)
    {
        return response()->json(User::query()->searchable($request)->paginate($request->count));
    }

    /**
     * Method show user use param for include relation and select raw, who need
     *
     * @param SearchRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(SearchRequest $request, int $id)
    {
        return response()->json(
            User::query()->whereId($id)
            ->withRaw($request->raw)->include($request->with)
            ->get()->first()
        );
    }

    /**
     * Method search all users which was received and send notifications
     *
     * return true|false
     *
     * @param NewsletterRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function receiversUserSendEmail(NewsletterRequest $request, int $id)
    {
        // after include redis driver rewrite base on queues
        $users = User::query()->selectRaw('email')->join('wishlists', 'id', '=', 'user_id')
            ->where('product_id', $id)->where('receive_email', 1)->get();
        foreach ($users as $user) {
            $user->notify(new ReceivedUsersEmail($request));
        }
        return response()->json(true);
    }

    /**
     * Method search all users which was marketing subscribed and send notifications
     *
     * return true|false
     *
     * @param NewsletterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function newsletter(NewsletterRequest $request)
    {
        // after include redis driver rewrite base on queues
        $users = User::query()->selectRaw('email')->marketingSubscribed();
        if (isset($request->ids)) {
            $users->whereIn('id', $request->ids);
        }
        $users = $users->get();
        foreach ($users as $user) {
            $user->notify(new ReceivedUsersEmail($request));
        }
        return response()->json(true);
    }

    /**
     * Add new user
     *
     * @param UserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(UserRequest $request)
    {
        return response()->json(User::create(array_merge($request->all(), ['api_token' => str_random(60)])));
    }

    /**
     * Find user by id and update all fields, contains in request
     *
     * @param UserRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(UserRequest $request, int $id)
    {
        return response()->json(User::query()->whereId($id)->update($request->except('api_token')));
    }

    /**
     * Find user by id and delete (soft delete)
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy(int $id)
    {
        return response()->json(User::query()->whereId($id)->delete());
    }


    /**
     * Show create modal for add this entity
     */

    public function create()
    {

    }

    /**
     * Show edit modal for update this entity where id = param
     *
     * @param $id
     */
    public function edit($id)
    {

    }

    /**
     * Upload image for user, what find by id
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function upload(ImageRequest $request, int $id)
    {
        $user = User::query()->whereId($id)->get()->first();
        $avatar = ImageController::upload($request, 'public/avatar/'. $user->id);
        return response()->json($user->update(['avatar' => $avatar]));
    }

    /**
     * Add new address for auth user
     *
     * @param AddressRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addAddress(AddressRequest $request)
    {
        return response()->json($request->user()->addresses()->create($request->except('api_token')));
    }


    /**
     * Remove address auth user, find by id
     *
     * @param AddressRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeAddress(AddressRequest $request, int $id)
    {
        return response()->json($request->user()->addresses()->whereId($id)->delete());
    }
}
