<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\SearchRequest;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminAndModer')->only([
            'store',
            'update',
            'destroy',
            'upload'
        ]);
    }

    /**
     * Method show list categories with paginate, use param for searchable
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(SearchRequest $request)
    {
        return response()->json(Category::query()->searchable($request)->paginate($request->count));
    }

    /**
     * Method show category use param for include relation and select raw, who need
     *
     * @param SearchRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(SearchRequest $request, int $id)
    {
        return response()->json(
            Category::query()->whereId($id)
            ->withRaw($request->raw)->include($request->with)
            ->get()->first()
        );
    }

    /**
     * Add new category to database use $request param
     *
     * @param CategoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CategoryRequest $request)
    {
        return response()->json(Category::create($request->all()));
    }

    /**
     * Find category by id and update all fields, contains in request
     *
     * @param CategoryRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CategoryRequest $request, int $id)
    {
        return response()->json(Category::query()->whereId($id)->update($request->except('api_token')));
    }

    /**
     * Find category by id and delete (soft delete)
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id)
    {
        return response()->json(Category::query()->whereId($id)->delete());
    }


    /**
     * Show create modal for add this entity
     */

    public function create()
    {

    }

    /**
     * Show edit modal for update this entity where id = param
     *
     * @param $id
     */
    public function edit($id)
    {

    }

    /**
     * Only upload file to storage/category folder
     *
     * @param ImageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function upload(ImageRequest $request)
    {
        return response()->json(ImageController::upload($request, 'public/category'));
    }
}
