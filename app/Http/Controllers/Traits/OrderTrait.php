<?php

namespace App\Http\Controllers\Traits;

use App\Agent;
use App\Http\Controllers\AutorizeNetController;
use App\Notifications\OrderSuccessBuy;
use App\Order;
use App\Product;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

trait OrderTrait
{
    use Notifiable;

    protected function buy(
        $user,
        array $request = [
//            'user_card_data' => [
//                'card_number' => '4111111111111111',
//                'card_year' => '2038',
//                'card_month' => '12',
//                'card_code' => '123'
//            ],
//            'card' => [
//                0 => [
//                    'product_id' => 1,
//                    'quantity1' => 10,
//                    'quantity2' => 0,
//                    'quantity3' => 1,
//                    'arrive_date' => '2017-11-20',
//                    'shipping_method' => 'electronic_ticket',
//                ],
//                1 => [
//                    'product_id' => 2,
//                    'quantity1' => 2,
//                    'quantity2' => 0,
//                    'quantity3' => 11,
//                    'arrive_date' => '2017-11-20',
//                    'shipping_method' => 'electronic_ticket',
//                ],
//                2 => [
//                    'product_id' => 8,
//                    'quantity1' => 14,
//                    'quantity2' => 0,
//                    'quantity3' => 2,
//                    'arrive_date' => '2017-11-20',
//                    'shipping_method' => 'electronic_ticket',
//                ],
//            ],
//            'address' => [
//                //                'country' => 'USA',
//                //                'state' => 'TX',
//                //                'city' => 'Pecan Springs',
//                //                'zip' => '44628',
//                //                'address' => '14 Main Street',
//            ],
        ]
    ) {
        if (!empty($request['user_card_data'])) {
            // create new profile or update card info and set new profile for auth user
            $user = $this->createOrUpdateProfileUserData($request['user_card_data'], $request['address'], $user);
        }
        $payData = $this->getAmount($request['card'], $request);
        $amount = $payData['amount'];
        $md5 = $this->getMd5($payData);
        DB::beginTransaction();
        $pay = AutorizeNetController::chargeCustomerProfile($user->profile_id, $user->payment_profile_id, $amount);
        if ($pay == false) {
            DB::rollback();
            return ['error' => 'transaction is not created'];
        }
        $order = Order::create([
            'user_id' => $user->id,
            'auth_code' => $pay['auth_code'],
            'trans_id' => $pay['trans_id'],
            'amount' => $amount,
            'hash' => $md5,
            'status' => 'Pending Settlement',
            'profile_id' => $user->profile_id,
            'payment_profile_id' => $user->payment_profile_id,
            'agent_id' => isset($request->agent_key) ? Agent::query()->whereKey($request->agent_key)->get()->id : null,
        ]);
        foreach ($request['card'] as $i => $item) {
            $order->items()->attach([$item['product_id'] => array_merge(
                $payData[$item['product_id']],
                [
                    'shipping_method' => $request['card'][$i]['shipping_method'],
                    'arrive_date' => $request['card'][$i]['arrive_date'],
                ]
            )]);
        }
        DB::commit();
        if ($order) {
            $user->notify(new OrderSuccessBuy());
        }
        return $order;
    }

    private function createOrUpdateProfileUserData(array $card_data, array $address, $user)
    {
        if (empty($user->profile_id) || empty($user->payment_profile_id)) {
            return $this->createCustumer($card_data, $address, $user);
        }
        return $this->updateCustumer($card_data, $user);
    }

    private function createCustumer($card_data, $address, &$user)
    {
        $profileData = AutorizeNetController::createCustomerProfile($card_data, $address, $user);
        if ($profileData == false) {
            return ['error' => 'custumer profile not created'];
        }
        $user->update($profileData);
        return $user;
    }

    private function updateCustumer($card_data, &$user)
    {
        $profileData = AutorizeNetController::updateCustomerPaymentProfile(
            $user->profile_id,
            $user->payment_profile_id,
            $card_data
        );
        if ($profileData == false) {
            return ['error' => 'custumer payment profile not updated'];
        }
        $user->update(['payment_profile_id' => $profileData]);
        return $user;
    }

    private function getAmount(array $card, $request)
    {
        $amount = 0;
        $productAmountData = [];
        foreach ($card as $item) {
            $product = Product::query()->find($item['product_id']);

            $q1 = $item['quantity1'];
            $q2 = $item['quantity2'];
            $q3 = $item['quantity3'];
            $p1 = $product->price1;
            $p2 = $product->price2;
            $p3 = $product->price3;
            $productAmountData[$product->id]['quantity1'] = $q1;
            $productAmountData[$product->id]['quantity2'] = $q2;
            $productAmountData[$product->id]['quantity3'] = $q3;
            $productAmountData[$product->id]['p1'] = $p1;
            $productAmountData[$product->id]['p2'] = $p2;
            $productAmountData[$product->id]['p3'] = $p3;
            $p1_c = $this->execute($q1, $q2, $q3, $p1, $p2, $p3, $product->commission1);
            $p1_d = $this->execute($q1, $q2, $q3, $p1_c, $p2, $p3, $product->discount1);

            $productAmountData[$product->id]['p1_c'] = $p1_c;
            $productAmountData[$product->id]['p1_d'] = $p1_d;

            $p2_c = $this->execute($q1, $q2, $q3, $p1, $p2, $p3, $product->commission2);
            $p2_d = $this->execute($q1, $q2, $q3, $p2_c, $p2, $p3, $product->discount2);

            $productAmountData[$product->id]['p2_c'] = $p2_c;
            $productAmountData[$product->id]['p2_d'] = $p2_d;

            $p3_c = $this->execute($q1, $q2, $q3, $p1, $p2, $p3, $product->commission3);
            $p3_d = $this->execute($q1, $q2, $q3, $p3_c, $p2, $p3, $product->discount3);

            $productAmountData[$product->id]['p3_c'] = $p3_c;
            $productAmountData[$product->id]['p3_d'] = $p3_d;

            $subTotal = $this->execute($q1, $q2, $q3, $p1_d, $p2_d, $p3_d, $product->sub_total);
            $total = $this->execute($q1, $q2, $q3, $p1_d, $p2_d, $p3_d, $product->total, $subTotal);

            $productAmountData[$product->id]['subtotal'] = $subTotal;
            $productAmountData[$product->id]['total'] = $total;

            $amount += $total;
        }
        if (isset($request->agent_key)) {
            $formula = Agent::query()->whereKey($request->agent_key)->get()->formula;
            $amount = $this->execute(0, 0, 0, 0, 0, 0, $formula, $amount);
        }
        $productAmountData['amount'] = $amount;
        return $productAmountData;
    }


    private function getMd5(array $productAmountData)
    {
        $string = '';
        foreach ($productAmountData as $item) {
            $string .= $item['quantity1'];
            $string .= $item['quantity2'];
            $string .= $item['quantity3'];
            $string .= $item['p1_d'];
            $string .= $item['p2_d'];
            $string .= $item['p3_d'];
            $string .= $item['subtotal'];
            $string .= $item['total'];
        }
        $string .= Carbon::now()->toDayDateTimeString();
        return md5($string);
    }

    private function execute($q1, $q2, $q3, $p1, $p2, $p3, $formula, $price = 0)
    {
        $exec = "(function () {
                  var p1 = $p1;
                  var p2 = $p2;
                  var p3 = $p3;
                  var q1 = $q1;
                  var q2 = $q2;
                  var q3 = $q3;
                  var price = $price;
                  var formula = '$formula';
                  var result = 0;
                  result = (function () { return eval(formula) })();
                  return result;
                })()";
        exec('node -p "'.$exec.'"', $arr);
        return $arr[0];
    }
}
