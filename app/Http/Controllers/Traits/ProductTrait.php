<?php

namespace App\Http\Controllers\Traits;

use App\Product;
use Illuminate\Http\Request;

trait ProductTrait
{
    public function getProduct(Product $product)
    {
        return $product;
    }

    public function getProductsIn(Request $request)
    {
        return Product::whereIn('id', $request->ids)->get();
    }

    /**
     * Find products in category idCategory
     *
     * @param int $id
     * @param int $limit
     * @return mixed
     */
    protected function searchCategory(int $id, $limit = 12)
    {
        return Product::onList()
            ->whereHas('categories', function ($query) use ($id) {
                $query->whereId($id);
            })->paginate($limit);
    }

    protected function searchProduct(Request $request, $count = 12)
    {
        return Product::query()->onList()->where('title', 'like', "%{$request->text}%")
            ->orWhere('item_description', 'like', "%{$request->text}%")
            ->orWhere('item_term', 'like', "%{$request->text}%")
            ->orWhere('address', 'like', "%{$request->text}%")
            ->orWhereHas('categories', function ($query) use ($request) {
                $query->where('title', 'like', "%{$request->text}%")
                    ->orWhere('description', 'like', "%{$request->text}%");
            })->orWhereHas('keywords', function ($query) use ($request) {
                $query->where('title', 'like', "%{$request->text}%");
            })->orderBy('title')->paginate($count);
    }

    protected function bestSellers($count = 12)
    {
        return Product::query()->onList()->withCount('orders')->orderBy('orders_count', 'desc')->paginate($count);
    }
}
