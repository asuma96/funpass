<?php

namespace App\Http\Controllers\Traits;

trait UserTrait
{
    protected function wishlist($user)
    {
        return $user->wishlist;
    }

    protected function updateSelfInformation(array $request, $user)
    {
        return $user->update($request);
    }
}
