<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\OrderTrait;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    use OrderTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $order = $this->buy(Auth::user(), $request->all());
        if (!isset($order['error'])) {
            return redirect('/shopping')->with('message', 'Order has been created');
        }
        return redirect()->back()->with('message', $order['error']);
    }

    public function history()
    {
        $orders = Order::selfHistory(Auth::user()->id);
        return view('pages.orders', compact('orders'));
    }

    public function orderHash(string $hash)
    {
        return response()->json(Order::searchByHash($hash));
    }
}
