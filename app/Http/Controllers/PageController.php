<?php

namespace App\Http\Controllers;

use App\Block;
use App\Page;

class PageController extends Controller
{
    public function sitemap()
    {
        $sitemap = App::make("sitemap");
        $products = \App\Product::query()->get()->all();
        foreach ($products as $product) {
            $sitemap->add(getenv('APP_URL') . '/single-product/' . $product->id, $product->updated_at, 1);
        }
        $sitemap->store('xml', 'sitemap');
        return $sitemap->render('xml');
    }

    public function page($url)
    {
        $page = Page::query()->where('url', $url)->get()->first();
        $view = view('pages.page', compact('page'))->render();
        preg_match_all('/\[\[\w*\]\]/', $page->body, $matches, PREG_OFFSET_CAPTURE);
        foreach ($matches[0] as $item) {
            $block = '';
            if (Block::query()->whereCode($item[0])->count() != 0) {
                $block = Block::query()->whereCode($item[0])->get()->first()->body;
            }
            $view = str_replace($item[0], $block, $view);
        }
        return $view;
    }
}
