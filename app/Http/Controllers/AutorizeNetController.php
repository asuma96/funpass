<?php

namespace App\Http\Controllers;

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class AutorizeNetController extends Controller
{
    public static function createCustomerProfile($card_data, $address, $user)
    {
        // Common setup for API credentials
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(getenv('MERCHANT_LOGIN_ID'));
        $merchantAuthentication->setTransactionKey(getenv('MERCHANT_TRANSACTION_KEY'));
        $refId = 'ref' . time();

        // Create the payment data for a credit card
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($card_data['card_number']);
        $creditCard->setExpirationDate($card_data['card_year'] . '-' . $card_data['card_month']);
        $creditCard->setCardCode($card_data['card_code']);
        $paymentCreditCard = new AnetAPI\PaymentType();
        $paymentCreditCard->setCreditCard($creditCard);

        // Create the Bill To info
        $billto = new AnetAPI\CustomerAddressType();
        $billto->setFirstName($user->first_name);
        $billto->setLastName($user->last_name);
//        $billto->setCompany("Souveniropolis");
//        $billto->setAddress($address['address']);
//        $billto->setCity($address['city']);
//        $billto->setState($address['state']);
//        $billto->setZip($address['zip']);
//        $billto->setCountry($address['country']);

        // Create a Customer Profile Request
        //  1. create a Payment Profile
        //  2. create a Customer Profile
        //  3. Submit a CreateCustomerProfile Request
        //  4. Validate Profiiel ID returned

        $paymentprofile = new AnetAPI\CustomerPaymentProfileType();

        $paymentprofile->setCustomerType('individual');
        $paymentprofile->setBillTo($billto);
        $paymentprofile->setPayment($paymentCreditCard);
        $paymentprofiles[] = $paymentprofile;
        $customerprofile = new AnetAPI\CustomerProfileType();
        $customerprofile->setDescription("Site custumer");

        $customerprofile->setMerchantCustomerId("M_" . $user->id);
        $customerprofile->setEmail($user->email);
        $customerprofile->setPaymentProfiles($paymentprofiles);

        $request = new AnetAPI\CreateCustomerProfileRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setProfile($customerprofile);
        $controller = new AnetController\CreateCustomerProfileController($request);
        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
            $result = [];
//            echo "Succesfully create customer profile : " . $response->getCustomerProfileId() . "\n";
            $result['profile_id'] = $response->getCustomerProfileId();
            $paymentProfiles = $response->getCustomerPaymentProfileIdList();
//            echo "SUCCESS: PAYMENT PROFILE ID : " . $paymentProfiles[0] . "\n";
            $result['payment_profile_id'] = $paymentProfiles[0];
        } else {
//            echo "ERROR :  Invalid response\n";
            $errorMessages = $response->getMessages()->getMessage();
//            echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
            $result = false;
        }
        return $result;
    }

    /**
     * Create transaction use AutorizeNet API
     * @param string $card_number
     * @param string $card_date
     * @param string $card_code
     * @param string $amount
     * @return array|bool
     */
    public static function chargeCreditCard(
        array $data = [
            'card_number' => '4111111111111111',
            'card_date' => '1226',
            'card_code' => '123',
            'amount' => 2,
        ]
    ) {
        // Common setup for API credentials
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(getenv('MERCHANT_LOGIN_ID'));
        $merchantAuthentication->setTransactionKey(getenv('MERCHANT_TRANSACTION_KEY'));
        $refId = 'ref' . time();

        // Create the payment data for a credit card
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($data['card_number']);
        $creditCard->setExpirationDate($data['card_date']);
        $creditCard->setCardCode($data['card_code']);
        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setCreditCard($creditCard);

        $order = new AnetAPI\OrderType();
        $order->setDescription("New Order");

        //create a transaction
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount(15);
        $transactionRequestType->setOrder($order);
        $transactionRequestType->setPayment($paymentOne);


        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequestType);
        $controller = new AnetController\CreateTransactionController($request);
        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);

        if ($response != null) {
            if ($response->getMessages()->getResultCode() == 'Ok') {
                $tresponse = $response->getTransactionResponse();
                if ($tresponse != null && $tresponse->getMessages() != null) {
//                    echo " Transaction Response code : " . $tresponse->getResponseCode() . "\n";
//                    echo "Charge Credit Card AUTH CODE : " . $tresponse->getAuthCode() . "\n";
//                    echo "Charge Credit Card TRANS ID  : " . $tresponse->getTransId() . "\n";
//                    echo " Code : " . $tresponse->getMessages()[0]->getCode() . "\n";
//                    echo " Description : " . $tresponse->getMessages()[0]->getDescription() . "\n";
                    $result = [];
                    $result['auth_code'] = $tresponse->getAuthCode();
                    $result['trans_id'] = $tresponse->getTransId();
                } else {
//                    echo "Transaction Failed \n";
//                    if ($tresponse->getErrors() != null) {
//                        echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
//                        echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
//                    }
                    $result = false;
                }
            } else {
//                echo "Transaction Failed \n";
//                $tresponse = $response->getTransactionResponse();
//                if ($tresponse != null && $tresponse->getErrors() != null) {
//                    echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
//                    echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
//                } else {
//                    echo " Error code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
//                    echo " Error message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
//                }
                $result = false;
            }
        } else {
//            echo "No response returned \n";
            $result = false;
        }

        return $result;
    }

    public static function updateCustomerPaymentProfile($customerProfileId, $customerPaymentProfileId, $card_data)
    {
        // Common setup for API credentials
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(getenv('MERCHANT_LOGIN_ID'));
        $merchantAuthentication->setTransactionKey(getenv('MERCHANT_TRANSACTION_KEY'));
        $refId = 'ref' . time();

        //Set profile ids of profile to be updated
        $request = new AnetAPI\UpdateCustomerPaymentProfileRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setCustomerProfileId($customerProfileId);
        $controller = new AnetController\GetCustomerProfileController($request);


        // We're updating the billing address but everything has to be passed in an update
        // For card information you can pass exactly what comes back from an GetCustomerPaymentProfile
        // if you don't need to update that info
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($card_data['card_number']);
        $creditCard->setExpirationDate($card_data['card_year'] . '-' . $card_data['card_month']);
        $creditCard->setCardCode($card_data['card_code']);
        $paymentCreditCard = new AnetAPI\PaymentType();
        $paymentCreditCard->setCreditCard($creditCard);

//        // Create the Bill To info for new payment type
//        $billto = new AnetAPI\CustomerAddressType();
//        $billto->setFirstName("Mrs Mary");
//        $billto->setLastName("Doe");
//        $billto->setAddress("1 New St.");
//        $billto->setCity("Brand New City");
//        $billto->setState("WA");
//        $billto->setZip("98004");
//        $billto->setPhoneNumber("000-000-0000");
//        $billto->setfaxNumber("999-999-9999");
//        $billto->setCountry("USA");


        // Create the Customer Payment Profile object
        $paymentprofile = new AnetAPI\CustomerPaymentProfileExType();
        $paymentprofile->setCustomerPaymentProfileId($customerPaymentProfileId);
//        $paymentprofile->setBillTo($billto);
        $paymentprofile->setPayment($paymentCreditCard);

        // Submit a UpdatePaymentProfileRequest
        $request->setPaymentProfile($paymentprofile);

        $controller = new AnetController\UpdateCustomerPaymentProfileController($request);
        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
            // echo "Update Customer Payment Profile SUCCESS: " . "\n";
            // Update only returns success or fail, if success
            // confirm the update by doing a GetCustomerPaymentProfile
            $getRequest = new AnetAPI\GetCustomerPaymentProfileRequest();
            $getRequest->setMerchantAuthentication($merchantAuthentication);
            $getRequest->setRefId($refId);
            $getRequest->setCustomerProfileId($customerProfileId);
            $getRequest->setCustomerPaymentProfileId($customerPaymentProfileId);

            $controller = new AnetController\GetCustomerPaymentProfileController($getRequest);
            $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
            if (($response != null)) {
                if ($response->getMessages()->getResultCode() == "Ok") {
//                    echo "GetCustomerPaymentProfile SUCCESS: " . "\n";
//                    echo "Customer Payment Profile Id: " . $response->getPaymentProfile()->getCustomerPaymentProfileId() . "\n";
//                    echo "Customer Payment Profile Billing Address: " . $response->getPaymentProfile()->getbillTo()->getAddress() . "\n";
                    return $response->getPaymentProfile()->getCustomerPaymentProfileId();
                } else {
//                    echo "GetCustomerPaymentProfile ERROR :  Invalid response\n";
//                    $errorMessages = $response->getMessages()->getMessage();
//                    echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
                }
            } else {
//                echo "NULL Response Error";
            }

        } else {
//            echo "Update Customer Payment Profile: ERROR Invalid response\n";
//            $errorMessages = $response->getMessages()->getMessage();
//            echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
        }
        return false;
    }

    public static function chargeCustomerProfile($profileid, $paymentprofileid, $amount)
    {
        // Common setup for API credentials
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(getenv('MERCHANT_LOGIN_ID'));
        $merchantAuthentication->setTransactionKey(getenv('MERCHANT_TRANSACTION_KEY'));
        $refId = 'ref' . time();

        $profileToCharge = new AnetAPI\CustomerProfilePaymentType();
        $profileToCharge->setCustomerProfileId($profileid);
        $paymentProfile = new AnetAPI\PaymentProfileType();
        $paymentProfile->setPaymentProfileId($paymentprofileid);
        $profileToCharge->setPaymentProfile($paymentProfile);

        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($amount);
        $transactionRequestType->setProfile($profileToCharge);

        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequestType);
        $controller = new AnetController\CreateTransactionController($request);
        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);

        if ($response != null) {
            if ($response->getMessages()->getResultCode() == 'Ok') {
                $tresponse = $response->getTransactionResponse();
                if ($tresponse != null && $tresponse->getMessages() != null) {
//                    echo " Transaction Response code : " . $tresponse->getResponseCode() . "\n";
//                    echo "Charge Customer Profile APPROVED  :" . "\n";
//                    echo " Charge Customer Profile AUTH CODE : " . $tresponse->getAuthCode() . "\n";
//                    echo " Charge Customer Profile TRANS ID  : " . $tresponse->getTransId() . "\n";
//                    echo " Code : " . $tresponse->getMessages()[0]->getCode() . "\n";
//                    echo " Description : " . $tresponse->getMessages()[0]->getDescription() . "\n";
                    return ['auth_code' => $tresponse->getAuthCode(), 'trans_id' => $tresponse->getTransId()];
                } else {
//                    echo "Transaction Failed \n";
//                    if ($tresponse->getErrors() != null) {
//                        echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
//                        echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
//                    }
                    return false;
                }
            } else {
//                echo "Transaction Failed \n";
//                $tresponse = $response->getTransactionResponse();
//                if ($tresponse != null && $tresponse->getErrors() != null) {
//                    echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
//                    echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
//                } else {
//                    echo " Error code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
//                    echo " Error message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
//                }
                return false;
            }
        } else {
            return false;
        }
    }

    public static function refundTransaction($amount, $profile_id, $payment_profile_id)
    {
        // Common setup for API credentials
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(getenv('MERCHANT_LOGIN_ID'));
        $merchantAuthentication->setTransactionKey(getenv('MERCHANT_TRANSACTION_KEY'));
        $refId = 'ref' . time();

        $profileToCharge = new AnetAPI\CustomerProfilePaymentType();
        $profileToCharge->setCustomerProfileId($profile_id);
        $paymentProfile = new AnetAPI\PaymentProfileType();
        $paymentProfile->setPaymentProfileId($payment_profile_id);
        $profileToCharge->setPaymentProfile($paymentProfile);

        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("refundTransaction");
        $transactionRequestType->setAmount($amount);
        $transactionRequestType->setProfile($profileToCharge);

        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequestType);
        $controller = new AnetController\CreateTransactionController($request);
        $response = $controller->executeWithApiResponse(
            \net\authorize\api\constants\ANetEnvironment::SANDBOX
        );

        if ($response != null) {
            if ($response->getMessages()->getResultCode() == 'Ok') {
                $tresponse = $response->getTransactionResponse();
                if ($tresponse != null && $tresponse->getMessages() != null) {
//                    echo " Transaction Response code : " . $tresponse->getResponseCode() . "\n";
//                    echo "Refund SUCCESS: " . $tresponse->getTransId() . "\n";
//                    echo " Code : " . $tresponse->getMessages()[0]->getCode() . "\n";
//                    echo " Description : " . $tresponse->getMessages()[0]->getDescription() . "\n";
                    return ['message' => "Refund SUCCESS: " . $tresponse->getTransId()];
                } else {
//                    echo "Transaction Failed \n";
//                    if ($tresponse->getErrors() != null) {
//                        echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
//                        echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
//                    }
                }
            } else {
//                echo "Transaction Failed \n";
//                $tresponse = $response->getTransactionResponse();
//                if ($tresponse != null && $tresponse->getErrors() != null) {
//                    echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
//                    echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
//                } else {
//                    echo " Error code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
//                    echo " Error message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
//                }
            }
        } else {
//            echo "No response returned \n";
        }
        return false;
    }
}
