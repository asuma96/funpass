<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\ProductTrait;
use App\Product;

class ProductController extends Controller
{
    use ProductTrait;

    public function listByCategory(int $id)
    {
        $products = $this->searchCategory($id);
        return view('pages.products', compact('products'));
    }

    public function show(Product $product)
    {
        return view('pages.single-product', compact('product'));
    }

    public function specialOffers()
    {
        $products = Product::query()->where('special_offer', 1)->paginate(10);
        return view('pages.specialOffers', compact('products'));
    }

    protected function best()
    {
        $products = $this->bestSellers(10);
        return view('pages.bestSellers', compact('products'));
    }
}
