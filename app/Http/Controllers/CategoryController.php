<?php

namespace App\Http\Controllers;

use App\Category;

class CategoryController extends Controller
{
    public static function menus(int $limit = 5)
    {
        return Category::query()->selectRaw('id,title,cover')->menus()->limit($limit)->get();
    }

    public static function listCategory(int $count = 20)
    {
        return Category::query()->selectRaw('id,title')->limit($count)->get();
    }
}
