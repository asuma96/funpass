<?php

namespace App\Http\Controllers;

use App\Http\Controllers\API\ImageController;
use App\Http\Controllers\Traits\UserTrait;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\UserSelfPasswordUpdateRequest;
use App\Http\Requests\UserSelfUpdateRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    use UserTrait;

    public function __construct()
    {
        $this->middleware('auth')->except(['loginAsUser']);
    }

    protected function myAccount()
    {
        return view('pages.myaccount');
    }

    protected function update(UserSelfUpdateRequest $request)
    {
        if ($this->updateSelfInformation($request->all(), Auth::user())) {
            return redirect('/myaccount')->with('message', 'Update success');
        }
        return redirect()->back()->with('message', 'Error update your info');
    }

    protected function changePassword(UserSelfPasswordUpdateRequest $request)
    {
        return $this->updateSelfInformation($request->all(), Auth::user());
    }

    protected function uploadAvatar(ImageRequest $request)
    {
        $user = Auth::user();
        $avatar = ImageController::upload($request, 'public/avatar/'. $user->id);
        $user->update(['avatar' => $avatar]);
        return redirect('/myaccount')->with('message', 'Update success');
    }

    protected function getWishlists()
    {
        $wishlist = $this->wishlist(Auth::user());
        return view('pages.wishList', compact('wishlist'));
    }

    public function loginAsUser(Request $request, int $id)
    {
        $user = User::query()->where('api_token', $request->api_token)->get()->first();
        if ($user->role == 'admin') {
            Auth::loginUsingId($id);
        }
        return redirect('/');
    }
}
